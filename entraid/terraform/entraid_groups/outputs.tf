output "group_ids" {
  value = { for name, group in azuread_group.adgroups : name => group.id }
}
