# Define the required versions
terraform {
  # Terraform Version
  required_version = ">= 1.2"
  # AzureRM Version
  required_providers {
    azuread = {
      source  = "hashicorp/azuread"
      version = ">= 2.51.0" # Ensure you use the latest version suitable for your needs
    }
  }
} 