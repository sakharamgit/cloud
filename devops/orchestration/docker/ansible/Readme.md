# 1. Building Docker image:
docker build . -t ansible

# 2. Spinning the containers:
docker run -it ansible

# 3. Identifying IP's to connect:
docker inspect ansiblemaster | grep IPAddress
   
# 4. Connecting to docker instance:
docker exec -it ansiblemaster /bin/bash
