#### Dockerfile

- Contains the code to install Terraform, Ansible on the base image of python.

#### Steps to build an image and push it to Docker Hub

1. Login into Dokcer Hub
```bash
docker login --username <username>
```

2. Build the Docker Image
```bash
docker build -t py-tf-ansible:latest . 
```

3. Login into DockerHub using PAT
```bash
docker login --username <user_name>
```

4. Push the Docker Image
```bash
docker tag py-tf-ansible:latest:latest <user_name>/<repository_name>:latest
docker push <user_name>/<repository_name>:latest
```