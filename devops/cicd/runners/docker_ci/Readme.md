# CI/CD Runner

## Introduction

This Dockerfile sets up a CI/CD runner environment based on Ubuntu 23.10. It includes various tools and utilities required for continuous integration and deployment tasks, such as Docker, Terraform, and other essential packages.

## Dockerfile Overview

The Dockerfile performs the following tasks:

1. **Base Image**: Uses `ubuntu:23.10` as the base image.
2. **Maintainer**: Sets the maintainer label.
3. **Arguments**: Defines build arguments for the username and Terraform version.
4. **User Creation**: Creates a new user and group for running the CI/CD tasks.
5. **Package Installation**: Installs essential packages such as `unzip`, `curl`, `wget`, `make`, and `jq`.
6. **Docker Installation**: Installs Docker and its dependencies.
7. **Working Directory**: Sets the working directory to the home directory of the created user.
8. **User Switch**: Switches to the created user for running subsequent commands.

### Usage
1. **Building the Docker Image**
- To build the Docker image, run the following command in the directory containing the Dockerfile:

```sh
docker build -t ci-cd-runner .
```

2. **Running the Docker Container**
- To run the Docker container, use the following command:

```sh
docker run -d --name ci-cd-runner ci-cd-runner
```

3. **Accessing the Container**
- To access the running container, use the following command:

```sh
docker exec -it ci-cd-runner /bin/bash
```

