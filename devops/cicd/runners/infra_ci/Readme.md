# Infra CI/CD Runner

## Introduction

This Dockerfile sets up an infrastructure CI/CD runner environment based on Alpine Linux. It includes various tools and utilities required for continuous integration and deployment tasks, such as Terraform, Python, and other essential packages.

## Dockerfile Overview

The Dockerfile performs the following tasks:

1. **Base Image**: Uses `alpine:3.18.0` as the base image.
2. **Maintainer**: Sets the maintainer label.
3. **Package Installation**: Updates and upgrades the Alpine packages and installs a list of necessary packages.
4. **Python and Pip**: Upgrades `pip` and installs Python packages.
5. **Terraform Installation**: Downloads and installs Terraform.
6. **Terraform Providers**: Copies Terraform providers into the container and creates necessary directories for them.


### Terraform Installation
- Downloads and installs Terraform.

```
RUN wget https://releases.hashicorp.com/terraform/1.3.6/terraform_1.3.6_linux_amd64.zip
RUN unzip terraform_1.3.6_linux_amd64.zip -d /usr/local/bin/
```

- Terraform Providers
    + Copies Terraform providers into the container and creates necessary directories for them.

```sh
COPY ./terraform_providers/ /tmp/

RUN mkdir -p /root/tf_cache/registry.terraform.io/hashicorp/azurerm/3.66.0/linux_amd64/
RUN mkdir -p /root/tf_cache/registry.terraform.io/hashicorp/random/3.4.3/linux_amd64/
RUN mkdir -p /root/tf_cache/registry.terraform.io/hashicorp/azuread/2.31.0/linux_amd64/
RUN mkdir -p /root/tf_cache/registry.terraform.io/hashicorp/tls/4.0.4/linux_amd64/
```

### Usage

1. Building the Docker Image
- To build the Docker image, run the following command in the directory containing the Dockerfile:

```sh
docker build -t infra-ci-cd-runner .
```

2. Running the Docker Container
- To run the Docker container, use the following command:

```sh
docker run -d --name infra-ci-cd-runner infra-ci-cd-runner
```

3. Accessing the Container
- To access the running container, use the following command:

```sh
docker exec -it infra-ci-cd-runner /bin/bash
```
