# CI/CD Runner

## Introduction

This Dockerfile sets up a CI/CD runner environment based on Alpine Linux. It includes various tools and utilities required for continuous integration and deployment tasks, such as AWS CLI, Azure CLI, Ansible, Kubernetes CLI, Helm, and more.

## Dockerfile Overview

The Dockerfile performs the following tasks:

1. **Base Image**: Uses `alpine:3.18.0` as the base image.
2. **Maintainer**: Sets the maintainer label.
3. **Package Installation**: Updates and upgrades the Alpine packages and installs a list of necessary packages.
4. **Python and Pip**: Upgrades `pip` and installs Python packages.
5. **Azure CLI**: Installs Azure CLI and necessary extensions.
6. **Ansible**: Installs Ansible and related dependencies.
7. **Kubernetes CLI**: Downloads and installs `kubectl`.
8. **Helm**: Downloads and installs Helm.
9. **Ansible Galaxy Collections**: Installs specific Ansible Galaxy collections.
10. **SOPS**: Downloads and installs SOPS for secret management.

## Installed Packages

The Dockerfile installs the following packages and tools:

- **System Packages**: `openssl`, `openssh`, `curl`, `jq`, `bash`, `wget`, `tar`, `unzip`, `py3-pip`, `gcc`, `musl-dev`, `python3-dev`, `libffi-dev`, `openssl-dev`, `cargo`, `busybox-extras`, `make`, `aws-cli`
- **Python Packages**: `pip`, `azure-cli`, `ansible`, `pywinrm`, `yq`
- **Kubernetes CLI**: `kubectl`
- **Helm**: Helm 3
- **Ansible Galaxy Collections**: `azure.azcollection`, `newrelic.newrelic-infra`
- **SOPS**: SOPS for secret management

## Usage

### Building the Docker Image
- To build the Docker image, run the following command in the directory containing the Dockerfile:

```sh
docker build -t ci-cd-runner .
```

### Running the Docker Container
- To run the Docker container, use the following command:

```sh
docker run -d --name ci-cd-runner ci-cd-runner
```

### Accessing the Container
- To access the running container, use the following command:

```sh
docker exec -it ci-cd-runner /bin/bash
```

### Additional Information
- **Azure CLI**
The Azure CLI is installed along with the bastion extension and the AKS CLI. You can use it to manage Azure resources from within the container.

- **Ansible**
Ansible is installed along with the pywinrm package for Windows remote management and the yq package for YAML processing. Additionally, specific Ansible Galaxy collections are installed for Azure and New Relic integrations.

- **Kubernetes CLI**
The Kubernetes CLI (kubectl) is installed for managing Kubernetes clusters.

- **Helm**
Helm 3 is installed for managing Kubernetes applications.

- **SOPS**
SOPS is installed for managing secrets in your infrastructure as code.