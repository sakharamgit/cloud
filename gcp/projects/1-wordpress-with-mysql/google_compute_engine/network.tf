resource "google_compute_network" "this" {
  auto_create_subnetworks = false
  name                    = "${var.prefix}-${var.environment}-${var.project}-gcn-${random_string.suffix.result}"
  routing_mode            = "REGIONAL"
}

resource "google_compute_subnetwork" "this" {
  name          = "${var.prefix}-${var.environment}-${var.project}-gcs-${random_string.suffix.result}"
  ip_cidr_range = "192.168.24.0/24"
  region        = var.gcp_region
  network       = google_compute_network.this.id
}