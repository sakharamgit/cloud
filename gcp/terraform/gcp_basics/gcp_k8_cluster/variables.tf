##################################################################################
# VARIABLES
##################################################################################

// GCP Variables
variable "gcp_project_name" {}
variable "gcp_region" {}
variable "gcp_zone" {}

variable "prefix" {
  type    = string
  default = "ss"
}

variable "project" {
  type    = string
  default = "demo"
}

variable "gke_username" {
  default     = ""
  description = "gke username"
}

variable "gke_password" {
  default     = ""
  description = "gke password"
}

variable "gke_num_nodes" {
  default     = 2
  description = "number of gke nodes"
}