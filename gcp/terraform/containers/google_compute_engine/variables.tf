##################################################################################
# VARIABLES
##################################################################################

// GCP Variables
variable "gcp_project_name" {}
variable "gcp_region" {}
variable "gcp_zone" {}

// Generic Variables
variable "prefix" {
  type    = string
  default = "ss"
}
variable "env" {
  type    = string
  default = "dev"
}

variable "project" {
  type    = string
  default = "gce"  
}

resource "random_string" "suffix" {
  length  = 2
  upper   = false
  lower   = true
  special = false
}