resource "azurerm_kubernetes_cluster_node_pool" "application" {
    lifecycle {
    ignore_changes = [
      node_count
    ]
  }
  kubernetes_cluster_id = azurerm_kubernetes_cluster.aks_cluster.id
  name                  = var.application_node_pool.name
  vm_size               = var.application_node_pool.vm_size
  availability_zones    = var.application_node_pool.availability_zones
  enable_auto_scaling   = var.application_node_pool_scaling.enable_auto_scaling
  enable_node_public_ip = var.application_node_pool.enable_node_public_ip
  max_pods              = var.application_node_pool.max_pods
  node_labels           = var.application_node_pool.node_labels
  node_taints           = var.application_node_pool.node_taints
  os_disk_size_gb       = var.application_node_pool.os_disk_size_gb
  os_disk_type          = var.application_node_pool.os_disk_type
  vnet_subnet_id        = var.application_node_pool_subnet_id
  node_count            = var.application_node_pool.node_count
  orchestrator_version  = var.application_node_pool.orchestrator_version
  tags                  = var.application_node_pool.tags
  
#   min_count = var.application_node_pool_scaling.enable_auto_scaling ? var.application_node_pool_scaling.min_count : null
#   max_count = var.application_node_pool_scaling.enable_auto_scaling ? var.application_node_pool_scaling.max_count : null
# }

  # # Use merge maps & Locals to reduce inputs
  # role_based_access_control {
  #   enabled = var.rbac_enabled
  #   dynamic "azure_active_directory" {
  #     for_each = var.azure_active_directory
  #     content {
  #       managed                = azure_active_directory.value.managed
  #       admin_group_object_ids = azure_active_directory.value.admin_group_object_ids
  #       client_app_id          = azure_active_directory.value.client_app_id
  #       server_app_id          = azure_active_directory.value.server_app_id
  #       server_app_secret      = azure_active_directory.value.server_app_secret
  #       tenant_id              = azure_active_directory.value.tenant_id
  #     }
  #   }
  # }
  /*  dynamic "role_based_access_control" {
    for_each = var.role_based_access_control
    content {
      enabled = role_based_access_control.value.enabled
      azure_active_directory {
        managed                = role_based_access_control.value.azure_active_directory.managed
        admin_group_object_ids = role_based_access_control.value.azure_active_directory.admin_group_object_ids
        client_app_id          = role_based_access_control.value.azure_active_directory.client_app_id
        server_app_id          = role_based_access_control.value.azure_active_directory.server_app_id
        server_app_secret      = role_based_access_control.value.azure_active_directory.server_app_secret
        tenant_id              = role_based_access_control.value.azure_active_directory.tenant_id
      }
    }
  } */

  # dynamic "network_profile" {
  #   for_each = var.aks_network_profile != null ? [1] : []
  #   content {
  #     network_plugin     = var.aks_network_profile.network_plugin
  #     network_policy     = var.aks_network_profile.network_policy
  #     dns_service_ip     = var.aks_network_profile.dns_service_ip
  #     service_cidr       = var.aks_network_profile.service_cidr
  #     load_balancer_sku  = var.aks_network_profile.load_balancer_sku
  #   }
  # }

  # dynamic "linux_profile" {
  #   for_each = var.linux_profile
  #   content {
  #     admin_username = linux_profile.value.admin_username
  #     ssh_key {
  #       key_data = linux_profile.value.ssh_key.key_data
  #     }
  #   }
  # }
  # private_cluster_enabled = var.private_cluster_enabled

  # dynamic "service_principal" {
  #   for_each = var.service_principal
  #   content {
  #     client_id     = service_principal.value.client_id
  #     client_secret = service_principal.value.client_secret
  #   }
  # }
}