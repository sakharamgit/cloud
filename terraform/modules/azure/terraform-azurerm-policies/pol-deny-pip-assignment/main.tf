resource "azurerm_policy_definition" "pol-deny-pip-assignment" {
  name         = var.name
  policy_type  = var.policy_type
  mode         = var.mode
  display_name = var.display_name
  description  = var.description

  policy_rule = <<POLICY_RULE
	{
      "if": {
        "anyOf": [
          {
            "source": "action",
            "like": "Microsoft.Network/publicIPAddresses/*"
          }
        ]
      },
      "then": {
        "effect": "deny"
      }
    }
	POLICY_RULE
}

resource "azurerm_policy_assignment" "pol-deny-pip-assignment" {
  name                 = var.name
  scope                = var.scope
  policy_definition_id = azurerm_policy_definition.pol-deny-pip-assignment.id
  description          = var.description
  display_name         = var.display_name
  not_scopes           = var.not_scopes
  depends_on           = [azurerm_policy_definition.pol-deny-pip-assignment]
}
