resource "azurerm_policy_definition" "pol-deny-resourcesnotallowed" {
  name         = var.name
  policy_type  = var.policy_type
  mode         = var.mode
  display_name = var.display_name
  description  = var.description
  metadata     = <<METADATA
    {
    "category": "Resources not allowed"
    }
 
METADATA

  policy_rule = <<POLICY_RULE
    {
    "if": {
          "field": "type",
          "in": "[parameters('listOfResourceTypesNotAllowed')]"
          },
          "then": {
          "effect": "Deny"
        }
    }
POLICY_RULE

  parameters = <<PARAMETERS
    {
    "listOfResourceTypesNotAllowed": {
          "type": "Array",
          "metadata": {
            "description": "The list of resource types that cannot be deployed.",
            "displayName": "Not allowed resource types",
            "strongType": "resourceTypes"
            }
        }
    }
PARAMETERS

}

resource "azurerm_policy_assignment" "pol-deny-resourcesnotallowed" {
  name                 = var.name
  scope                = var.scope
  policy_definition_id = azurerm_policy_definition.pol-deny-resourcesnotallowed.id
  description          = var.description
  display_name         = var.display_name
  not_scopes           = var.not_scopes
  depends_on           = [azurerm_policy_definition.pol-deny-resourcesnotallowed]
  parameters = jsonencode({
    listOfResourceTypesNotAllowed = {
      value = var.listresourcesnotallowed
    }
  })
}