resource "azurerm_policy_definition" "pol-allowed-locations" {
  name         = var.name
  policy_type  = var.policy_type
  mode         = var.mode
  display_name = var.display_name
  description  = var.description

  parameters = <<PARAMETERS
	{
      "listOfAllowedLocations": {
        "type": "Array",
        "metadata": {
          "displayName": "${var.display_name}",
          "description": "${var.description}",
          "strongType": "${var.strongType}"
        }
      }
  }
  	PARAMETERS

  policy_rule = <<POLICY_RULE
	{
    "if": {
      "allOf": [
        {
            "field": "location",
            "notIn": "[parameters('listOfAllowedLocations')]"
          }
        ]
      },
      "then": {
        "effect": "deny"
      }
  }
	POLICY_RULE
}

resource "azurerm_policy_assignment" "pol-allowed-locations" {
  name                 = var.name
  scope                = var.scope
  policy_definition_id = azurerm_policy_definition.pol-allowed-locations.id
  description          = var.description
  display_name         = var.display_name
  not_scopes           = var.not_scopes
  depends_on           = [azurerm_policy_definition.pol-allowed-locations]

  parameters = jsonencode({
    listOfAllowedLocations = {
      value = var.listOfAllowedLocations
    }
  })
}
