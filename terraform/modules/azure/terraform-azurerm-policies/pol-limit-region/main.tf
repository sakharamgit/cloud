resource "azurerm_policy_definition" "pol-limit-region" {
  name         = var.name
  policy_type  = var.policy_type
  mode         = var.mode
  display_name = var.display_name
  description  = var.description

  policy_rule = <<POLICY_RULE
	{
      "if": {
        "not": {
          "field": "location",
          "in": [
            "eastus2",
            "eastus",
            "westus",
            "westus2",
            "southcentralus",
            "brazilsouth"
          ]
        }
      },
      "then": {
        "effect": "deny"
      }
    }
	POLICY_RULE
}

resource "azurerm_policy_assignment" "pol-limit-region" {
  name                 = var.name
  scope                = var.scope
  policy_definition_id = azurerm_policy_definition.pol-limit-region.id
  description          = var.description
  display_name         = var.display_name
  not_scopes           = var.not_scopes
  depends_on           = [azurerm_policy_definition.pol-limit-region]
}
