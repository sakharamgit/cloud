resource "azurerm_policy_definition" "pol-require-rgtag-application" {
  name         = var.name
  policy_type  = var.policy_type
  mode         = var.mode
  display_name = var.display_name
  description  = var.description

  policy_rule = <<POLICY_RULE
	{
      "if": {
        "allOf": [
          {
            "field": "type",
            "equals": "Microsoft.Resources/saz_solutionscriptions/resourceGroup"
          },
          {
            "field": "tags['Application']",
            "exists": "false"
          }
        ]
      },
      "then": {
        "effect": "deny"
      }
    }
	POLICY_RULE
}

resource "azurerm_policy_assignment" "pol-require-rgtag-application" {
  name                 = var.name
  scope                = var.scope
  policy_definition_id = azurerm_policy_definition.pol-require-rgtag-application.id
  description          = var.description
  display_name         = var.display_name
  not_scopes           = var.not_scopes
  depends_on           = [azurerm_policy_definition.pol-require-rgtag-application]
}
