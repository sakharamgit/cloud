resource "azurerm_policy_definition" "pol-inherit-tag-owner" {
  name         = var.name
  policy_type  = var.policy_type
  mode         = var.mode
  display_name = var.display_name
  description  = var.description

  policy_rule = <<POLICY_RULE
	{
      "if": {
        "field": "tags['Owner']",
        "exists": "false"
      },
      "then": {
        "effect": "append",
        "details": [
          {
            "field": "tags['Owner']",
            "value": "[resourceGroup().tags['Owner']]"
          }
        ]
      }
    }
	POLICY_RULE
}

resource "azurerm_policy_assignment" "pol-inherit-tag-owner" {
  name                 = var.name
  scope                = var.scope
  policy_definition_id = azurerm_policy_definition.pol-inherit-tag-owner.id
  description          = var.description
  display_name         = var.display_name
  not_scopes           = var.not_scopes
  depends_on           = [azurerm_policy_definition.pol-inherit-tag-owner]
}
