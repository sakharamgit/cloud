resource "azurerm_policy_definition" "pol-deny-peerings-assignment" {
  name         = var.name
  policy_type  = var.policy_type
  mode         = var.mode
  display_name = var.display_name
  description  = var.description
  metadata     = <<METADATA
    {
    "category": "Virtual Networks"
    }
  METADATA

  parameters = <<PARAMETERS
    {
    "remote-vnets-allowed": {
          "type": "array",
          "metadata": {
            "displayName": "Allowed remote peerings",
            "description": "The list of allowed remote vnets to peering."
          }
        }
    }
PARAMETERS

  policy_rule = <<POLICY_RULE
    {
        "if": {
          "allof": [
            {
              "field": "Microsoft.Network/virtualNetworks/virtualNetworkPeerings/remoteVirtualNetwork.id",
              "notcontains": "[concat('saz_solutionscriptions/',saz_solutionscription().saz_solutionscriptionId)]"
            },
            {
              "field": "Microsoft.Network/virtualNetworks/virtualNetworkPeerings/remoteVirtualNetwork.id",
              "notIn": "[parameters('remote-vnets-allowed')]"
            }
          ]
        },
        "then": {
          "effect": "Deny"
        }
      }
POLICY_RULE
}

resource "azurerm_policy_assignment" "pol-deny-peerings-assignment" {
  name                 = var.name
  scope                = var.scope
  policy_definition_id = azurerm_policy_definition.pol-deny-peerings-assignment.id
  description          = var.description
  display_name         = var.display_name
  not_scopes           = var.not_scopes
  depends_on           = [azurerm_policy_definition.pol-deny-peerings-assignment]
  parameters = jsonencode({
    remote-vnets-allowed = {
      value = var.list-remote-vnets-allowed
    }
  })
}