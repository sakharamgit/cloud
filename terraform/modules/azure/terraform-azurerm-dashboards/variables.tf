
// Required Variables
//**********************************************************************************************
variable "resource_group_name" {
  type        = string
  description = "(Required) Specifies the Resource Group where the Managed Kubernetes Cluster should exist"
}

variable "location" {
  type        = string
  description = "(Required) Location of the resource group" 
}

variable "dashboard_name" {
  type        = string
  description = "(Required) Name of the dashboard" 
}

variable "dashboard_properties" {
  type        = string
  description = "(Required) Dashboard Properties" 
}





// Tags
//**********************************************************************************************
variable "resource_tags" {
  type        = map(string)
  description = "(Optional) Tags for resources"
  default     = {}
}
variable "deployment_tags" {
  type        = map(string)
  description = "(Optional) Tags for deployment"
  default     = {}
}
//**********************************************************************************************