// Action Group
//**********************************************************************************************
variable "resource_group_monitoring" {
  type        = string
  description = "(Required) Resource group to deploy Monitoring Action Group and Alerts"
}
variable "action_group_name" {
  type        = string
  description = "(Required) The name of the Action Group"
}
variable "short_name" {
  type        = string
  description = "(Required) The short name of the action group. This will be used in SMS messages"
}
variable "action_group_enabled" {
  type        = bool
  description = "(Optional) Whether this action group is enabled. If an action group is not enabled, then none of its receivers will receive communications"
  default     = true
}
variable "email_receivers" {
  type = map(object({
    name                    = string
    email_address           = string
    use_common_alert_schema = bool
  }))
  description = "(Optional) One or more email_receiver blocks"
  default     = {}
}
//**********************************************************************************************


// Alerts
//**********************************************************************************************
variable "monitoring_enabled" {
  type        = bool
  description = "Enable or Disable resource monitoring and alerting"
  default     = true
}

### Metric Alerts ###
variable "metric_alert_enabled" {
  type        = bool
  description = "(Optional) Should this Metric Alert be enabled?"
  default     = true
}
variable "auto_mitigate" {
  type        = bool
  description = "(Optional) Should the alerts in this Metric Alert be auto resolved?"
  default     = true
}

# Inputs for creation Metric Alert along with conditions(critetias)
variable "metric_alerts" {
  type = map(object({
    name        = string
    description = string
    frequency   = string
    window_size = string
    severity    = number
    metric_criterias = map(object({
      metric_namespace = string
      metric_name      = string
      aggregation      = string
      operator         = string
      threshold        = number
      dimension = map(object({
        name     = string
        operator = string
        values   = list(string)
      }))
    }))
  }))
  description = "(Required) One or more criteria blocks"
  default = {
    # Total amount of available memory(bytes) in a managed cluster
    kube_node_status_allocatable_memory_bytes = {
      name        = "aks_allocatable_memory_bytes"
      description = "AKS memory not available"
      frequency   = "PT1M"
      window_size = "PT5M"
      severity    = 0
      metric_criterias = {
        criteria1 = {
          metric_namespace = "Microsoft.ContainerService/managedClusters"
          metric_name      = "kube_node_status_allocatable_memory_bytes"
          aggregation      = "Average"
          operator         = "LessThanOrEqual"
          threshold        = 1024000000
          dimension        = {}
        }
      }
    },
    # Total number of available cpu cores in a managed cluster
    kube_node_status_allocatable_cpu_cores = {
      name        = "aks_allocatable_cpu_cores"
      description = "AKS CPU Core not available"
      frequency   = "PT1M"
      window_size = "PT5M"
      severity    = 0
      metric_criterias = {
        criteria1 = {
          metric_namespace = "Microsoft.ContainerService/managedClusters"
          metric_name      = "kube_node_status_allocatable_cpu_cores"
          aggregation      = "Average"
          operator         = "LessThanOrEqual"
          threshold        = 1
          dimension        = {}
        }
      }
    },
    # Number of pods in Ready state
    kube_pod_status_ready = {
      name        = "aks_pod_status_ready"
      description = "AKS has 0 pods in ready state"
      frequency   = "PT1M"
      window_size = "PT5M"
      severity    = 0
      metric_criterias = {
        criteria1 = {
          metric_namespace = "Microsoft.ContainerService/managedClusters"
          metric_name      = "kube_pod_status_ready"
          aggregation      = "Total"
          operator         = "LessThan"
          threshold        = 1
          dimension = {
            namespace = {
              name     = "namespace"
              operator = "Include"
              values   = ["*"]
            },
            pod = {
              name     = "pod"
              operator = "Include"
              values   = ["*"]
            }
          }
        }
      }
    }
  }
}

### Activity Log Alerts ###
variable "log_alerts" {
  type = map(object({
    name        = string
    description = string
    log_criterias = object({
      category       = string
      operation_name = string
      level          = string
      status         = string
      sub_status     = string
    })
  }))
  description = "(Required) A criteria block"
  default = {
    aks_delete = {
      name        = "aks_delete"
      description = "AKS cluster deleted"
      log_criterias = {
        category       = "Administrative"
        operation_name = "Microsoft.ContainerService/managedClusters/delete"
        level          = null
        status         = "Started"
        sub_status     = null
      }
    }
  }
}
//**********************************************************************************************

variable "resource_group_name" {}

// Tags
//**********************************************************************************************
variable "resource_tags" {
  type        = map(string)
  description = "(Optional) Tags for resources"
  default     = {}
}

variable "deployment_tags" {
  type        = map(string)
  description = "(Optional) Tags for deployment"
  default     = {}
}

variable "timeout" {
  type        = string
  description = "(Optional) Timeout"
  default     = "90m"
}

variable "aks_name" {
  type        = string
  description = "AKS Name"
}
