locals {
  timeout_duration = var.timeout
}

data "azurerm_kubernetes_cluster" "aks" {
  name                = var.aks_name
  resource_group_name = var.resource_group_name
} 

// Set up Azure Monitor Action Group
//**********************************************************************************************
resource "azurerm_monitor_action_group" "action_group" {
  count               = var.monitoring_enabled ? 1 : 0
  name                = var.action_group_name
  resource_group_name = var.resource_group_name
  short_name          = var.short_name
  enabled             = var.action_group_enabled

  dynamic "email_receiver" {
    for_each = var.email_receivers
    content {
      name                    = email_receiver.value.name
      email_address           = email_receiver.value.email_address
      use_common_alert_schema = email_receiver.value.use_common_alert_schema
    }
  }

  tags = merge(var.resource_tags, var.deployment_tags)

  timeouts {
    create = local.timeout_duration
    delete = local.timeout_duration
  }
}
//**********************************************************************************************



// Set up Azure Monitor Alert For Metrics
//**********************************************************************************************
resource "azurerm_monitor_metric_alert" "metric_alert" {
  for_each            = var.monitoring_enabled ? var.metric_alerts : {}
  name                = each.value.name
  resource_group_name = var.resource_group_name
  scopes              = [data.azurerm_kubernetes_cluster.aks.id]
  description         = each.value.description

  enabled       = var.metric_alert_enabled
  auto_mitigate = var.auto_mitigate
  frequency     = each.value.frequency
  severity      = each.value.severity
  window_size   = each.value.window_size

  dynamic "criteria" {
    for_each = each.value.metric_criterias
    content {
      metric_namespace = criteria.value.metric_namespace
      metric_name      = criteria.value.metric_name
      aggregation      = criteria.value.aggregation
      operator         = criteria.value.operator
      threshold        = criteria.value.threshold

      dynamic "dimension" {
        for_each = criteria.value.dimension
        content {
          name     = dimension.value.name
          operator = dimension.value.operator
          values   = dimension.value.values
        }
      }
    }
  }

  action {
    action_group_id = azurerm_monitor_action_group.action_group[0].id
  }

  tags = merge(var.resource_tags, var.deployment_tags)

  timeouts {
    create = local.timeout_duration
    delete = local.timeout_duration
  }
}
//****************************************************************************************


// Set up Azure Monitor Alert For Activity Logs
//****************************************************************************************
resource "azurerm_monitor_activity_log_alert" "log_alert" {
  for_each            = var.monitoring_enabled ? var.log_alerts : {}
  name                = each.value.name
  resource_group_name = var.resource_group_name
  scopes              = [data.azurerm_kubernetes_cluster.aks.id]
  description         = each.value.description

  criteria {
    resource_id    = data.azurerm_kubernetes_cluster.aks.id
    category       = each.value.log_criterias.category
    operation_name = each.value.log_criterias.operation_name
    level          = each.value.log_criterias.level
    status         = each.value.log_criterias.status
    sub_status     = each.value.log_criterias.sub_status
  }
  action {
    action_group_id = azurerm_monitor_action_group.action_group[0].id
  }
  tags = merge(var.resource_tags, var.deployment_tags)
  timeouts {
    create = local.timeout_duration
    delete = local.timeout_duration
  }
}
//****************************************************************************************