// Resource Group
resource "azurerm_resource_group" "resource_group" {
  name     = format("%s%s%s",
                    var.resource_group_prefix != "" ? "${var.resource_group_prefix}-" : "",
                    var.resource_group_name,
                    var.resource_group_suffix != "" ? "-${var.resource_group_suffix}" : ""
           )
  location = var.resource_group_location

  tags       = merge(var.resource_tags, var.deployment_tags)
  depends_on = [var.it_depends_on]

  lifecycle {
    ignore_changes = []
  }
  timeouts {
    create = local.timeout_duration
    delete = local.timeout_duration
  }
}
