// Virtual Network
resource "azurerm_virtual_network" "vnet" {
  name                = var.virtual_network_name
  resource_group_name = var.resource_group_name
  location            = var.vnet_location
  address_space       = var.vnet_address_space
  # dns_servers         = var.dns_servers

  # dynamic "ddos_protection_plan" {
  #   for_each = var.ddos_protection_plan

  #   content {
  #     id     = ddos_protection_plan.value.id
  #     enable = ddos_protection_plan.value.enable
  #   }
  # }

  tags       = merge(var.resource_tags, var.deployment_tags)
  # depends_on = [var.it_depends_on]

  # lifecycle {
  #   ignore_changes = [
  #     tags,
  #   ]
  # }

  # timeouts {
  #   create = local.timeout_duration
  #   delete = local.timeout_duration
  # }
}