# List of Objects Declaration
variable "listofobjects" {
        type = list(object({
        name = string
        size = string
        quantity = number
    }))
}

# List of Maps Declaration
variable "listofmaps" {
        type = list(map(string))
}

# List of Maps within a List of Object
variable "listofobjectswithinlistofmaps" {
  type        = list(object(
  {
    name = string
    cidr = string
    subnets = list(map(string))
  }))
}


# Map of Objects Declaration
variable "mapofobjects" {
  type = map(object({
    name = string
    age = number
    city = string
    vaccinated = bool
  }))
  
}

# Map of Lists Declaration
variable "mapoflists" {
  type = map(list(string))
}