locals  {
# List of Maps within List of Objects
listofmapswithinlistofojbects_secondlayer = flatten({
    [ for vnet in var.listofobjectswithinlistofmaps: 
        [ for subnet in var.vnet : 
        {
            name = "${vnet.name}-${subnet.name}"
            vpc = vnet.name
            subnet = subnet.name
        }
        ]
    ]
})
}