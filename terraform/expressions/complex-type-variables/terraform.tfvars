# List of Objects Example
listofobjects = [
    {
        name = "vm1",
        size = "Standard_DS2",
        quantity = 1
    },
    {
        name = "vm2",
        size = "Standard_DS4",
        quantity = 2
    },
    {
        name = "vm3",
        size = "Standard_DS6",
        quantity = 3
    }
]

# List of Maps Example
listofmaps = [
  {
    name = "subnet1"
    cidr = "10.0.1.0/24"
  },
  {
    name = "subnet2"
    cidr = "10.0.2.0/24"
  }
]

# List of Maps within List of Maps Example
listofobjectswithinlistofmaps = [
  {
    name    = "vpc-1"
    cidr    = "10.0.0.0/16"
    subnets = [
      {
        name = "subnet-1"
        cidr = "10.0.1.0/24"
      },
      {
        name = "subnet-2"
        cidr = "10.0.2.0/24"
      }
    ]
  },
  {
    name    = "vpc-2"
    cidr    = "10.0.0.0/16"
    subnets = [
      {
        name = "subnet-3"
        cidr = "10.0.3.0/24"
      },
      {
        name = "subnet-4"
        cidr = "10.0.4.0/24"
      }
    ]
  }
]

# Map of Objects Example
mapofobjects = {
    user1 = {
      name = "John"
      age = 32
      city = "New York"
      vaccinated = true
    }
    user2 = {
      name = "Jane"
      age = 31
      city = "San Jose"
      vaccinated = false
    }
    user3 = {
      name = "Mary"
      age = 30
      city = "San Francisco"
      vaccinated = true
    }
  }

# Map of Lists Example
mapoflists = {
    hall = ["orange", "pink"]
    kitchen = ["green"]
    masterbedroom = ["red", "blue"]
    kidsbedroom = ["blue", "white", "yellow"]
  }