output "a_string" {
    value = var.city
}

output "b_number" {
    value = var.age
}

output "c_bool" {
    value = var.flag
}

output "d_list" {
    value = [for user in var.userlist: user]
}

output "e_map" {
    value = [for user in var.userdetails: user]
}

output "e_map_filtered" {
    value = [var.userdetails["john"]]
}

output "f_set" {
    value = [for user in var.usernames: user]
}

output "g_object" {
    value = "${var.objectuserdetails}"
}

output "h_tuple" {
    value = "${var.tupleuserdetails}"
}