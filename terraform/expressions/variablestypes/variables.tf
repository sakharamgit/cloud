# String Declaration
variable "city" {
  type = string
}

# Number Declaration
variable "age" {
  type = number
}

# Boolean Declaration
variable "flag" {
  type = bool
}

# List Declaration
variable userlist {
    type = list(string)
}


# Map Declaration
variable userdetails {
    type = map(string)
}

# Set Declaration
variable "usernames" {
  type = set(string)
}

# Object Declaration
variable objectuserdetails{
    type = object({
        name= string, 
        age= number,
        city= string        
 })
}

# Tuple Declaration
variable "tupleuserdetails" {
  type = tuple([string, number, bool])
}

