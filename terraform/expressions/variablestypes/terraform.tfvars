# String Example
city = "california"

# Number Example
age = 32

# Boolean Example
flag = true

# List Example
userlist = ["john", "jane", "mary", "john"]

# Map Example
userdetails = {
    john = 32
    jane = 31
    mary = 30
}

# Set Example
usernames = [
    "john",
    "jane",
  ]

# Object Example
objectuserdetails = {
    name = "John",
    age = 32,
    city = "New York"
}

# Tuple Example
tupleuserdetails = [ "john", 32 , true ]