
# List
listoffirstnames = ["john", "jane", "james"]

# List
listofmiddlenames = ["a", "b", "c"]

# List
listoflastnames = ["smith", "mary", "jonas"]

# String
city = "california"

# List of objects
routine = [
  {
    id   = "1"
    name = "john"
    city = "newyork"
  },
  {
    id   = "2"
    name = "jane"
    city = "california"
  },
]

storage = [
      "eastusstgact1": {
          "rg": "rg1",
          "location": "eastus",
          "account_tier": "Standard",
          "account_replication_type":"GRS",
          "vnets_allowed": ["eastus-vnet1","eastus2-vnet2"],
          "subnets_allowed": ["subnet1"],
          "file_share_name": "fileshare1"
      },
      "eastus2stgact2": {
          "rg": "rg1",
          "location": "eastus2",
          "account_tier": "Standard",
          "account_replication_type": "GRS",
          "vnets_allowed": ["eastus2-vnet2","eastus-vnet1"],
          "subnets_allowed": ["subnet1"],
          "file_share_name": "fileshare2"
      }
  ]