# For Each Loop for a List
resource "null_resource" "users" {
  for_each = toset(var.listoffirstnames)

  triggers = {
    firstName = each.value
  }
}


# For Each Loop - One List Input and List Output with First Names
#output "listinputlistoutput" {
#  value = [for user in null_resource.users : user.triggers.firstName]
#}


# For Each Loop - One List Input and List Output with First Names
#output "tripleloopoutput" {
#  value = [for user in local.tripleloop : user ]
#}

