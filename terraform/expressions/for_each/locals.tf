# Defining a list of lists
locals {
  list_of_lists = [
    [
      { key = "luke", value = "jedi" },
      { key = "yoda", value = "jedi" }
    ],
    [
      { key = "darth", value = "sith" },
      { key = "palpatine", value = "sith" }
    ]
  ]

  merged_list = flatten(local.list_of_lists)

  tripleloop = flatten([for firstname, firstnames in var.listoffirstnames :
    [ for middlename, middlenames in var.listofmiddlenames :
      [ for lastname, lastnames in var.listoflastnames :
      {
        firstname     = firstnames
        middlename    = middlenames
        lastname      = lastnames
        name          = "${firstnames}-${middlenames}-${lastnames}"
  }]]])
}

