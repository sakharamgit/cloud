# Declaring a list of string
variable "listoffirstnames" {
  type = list(string)
}

# Declaring a list of string
variable "listofmiddlenames" {
  type = list(string)
}

# Decarling a list of string
variable "listoflastnames" {
  type = list(string)
}

# Declaring a string variable
variable "city" {
  type = string
}

# Declaring a list of objects
variable "routine" {
  type = list(object({
    id   = string
    name = string
    city = string
  }))
}

# Declaring a list of objects
variable "storage" {
  type = list(object({
    rg = string
    location = string
    account_tier = string
    account_replication_type = string
    vnets_allowed = list(string)
    subnets_allowed = list(string)
    file_share_name = string
  }))
}