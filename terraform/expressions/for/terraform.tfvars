
# List
listoffirstnames = ["john", "jane", "james"]

# List
listoflastnames = ["smith", "mary", "jonas"]

# String
city = "california"

# List of objects
routine = [
    {
        id = "1"
        name = "john"
        city = "newyork"
    },
    {
        id = "2"
        name = "jane"
        city = "california"
    },
]