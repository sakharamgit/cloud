# Declaring a list of string
variable "listoffirstnames" {
  type = list(string)
}

# Decarling a list of string
variable "listoflastnames" {
  type = list(string)
}

# Declaring a string variable
variable "city" {
  type = string
}

# Declaring a list of objects
variable "routine" {
  type = list(object({
    id = string
    name = string
    city = string
  }))
}