# Locals for NLB
locals {
  nlb_info          = yamldecode(file("../../aws_projects/og_ec2/vpcintegration.yaml"))
  nlb_name          = local.nlb_info.nlb_name
  target_group_name = local.nlb_info.target_group_name
  #vpc_id            = local.nlb_info.vpc_id
  vpc_name          = local.nlb_info.vpc_name
  nlb_subnet_id     = local.nlb_info.nlb_subnet_id
  ec2_private_ips   = local.nlb_info.ec2_private_ips
  ports             = local.nlb_info.ports
}

#To get the vpc
data "aws_vpc" "vpc1" {
  #id = local.vpc_id
  filter {
    name   = "tag:Name"
    values = [local.vpc_name]
  }
}


# To get the subnet 
data "aws_subnet" "awssubnet1" {
  id = local.nlb_subnet_id
}

# To create a network load balancer
resource "aws_lb" "example" {
  name               = local.nlb_name
  internal           = false
  load_balancer_type = "network"
  subnets            = [data.aws_subnet.awssubnet1.id]

  enable_deletion_protection       = false
  enable_cross_zone_load_balancing = true

  tags = {
    Environment = "integration"
  }
}

// To create the target group and attach it to the NLB
resource "aws_lb_target_group" "example" {
  count       = length(local.ports)
  name        = "${local.target_group_name}-${local.ports[count.index]}"
  port        = local.ports[count.index]
  protocol    = "TCP"
  target_type = "ip"
  vpc_id      = data.aws_vpc.vpc1.id
}

# To add the targets to the target group
resource "aws_lb_target_group_attachment" "tga" {
  count            = length(local.ports)
  target_group_arn = aws_lb_target_group.example[count.index].arn
  port             = local.ports[count.index]
  target_id        = local.ec2_private_ips
}


// To create a NLB listener
resource "aws_lb_listener" "lblist" {
  count             = length(local.ports)
  load_balancer_arn = aws_lb.example.arn

  port     = local.ports[count.index]
  protocol = "TCP"
  default_action {
    target_group_arn = aws_lb_target_group.example[count.index].arn
    type             = "forward"
  }
}


# Outputs
output "my_nlb" {
  value = aws_lb.example.name
}
