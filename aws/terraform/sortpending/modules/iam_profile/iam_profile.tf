# Locals for iam
locals {
  iam_info          = yamldecode(file("../../aws_projects/og_ec2/vpcintegration.yaml"))
  iam_name          = local.iam_info.iam_name
  iam_profile_name  = local.iam_info.iam_profile_name
}

// To create AWS IAM Role
resource "aws_iam_role" "sql_ec2_iam_role" {
  name = local.iam_name
  path = "/"

  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
               "Service": "ec2.amazonaws.com"
            },
            "Effect": "Allow",
            "Sid": ""
        }
    ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "sql_ec2_iam_pa" {
  role       = aws_iam_role.sql_ec2_iam_role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ReadOnlyAccess"
}

resource "aws_iam_instance_profile" "sql_ec2_iam_inst_profile" {
  name = local.iam_profile_name
  role = aws_iam_role.sql_ec2_iam_role.name
}