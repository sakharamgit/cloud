# Locals for NLB
locals {
  bp_info          = yamldecode(file("../../aws_projects/og_ec2/vpcintegration.yaml"))
  bv_name          = local.bp_info.bv_name
  bp_name          = local.bp_info.bp_name
  bp_rule_name1    = local.bp_info.bp_rule_name1
  bp_rule_name2    = local.bp_info.bp_rule_name2
}

data "aws_backup_vault" "vpcintsrvr_backup_vault" {
  name = local.bv_name
}

resource "aws_backup_plan" "vpcintsrvr_backup_plan" {
  name = local.bp_name

  rule {
    rule_name         = local.bp_rule_name1
    target_vault_name = data.aws_backup_vault.vpcintsrvr_backup_vault.name
    schedule          = "cron(0 2 * * ? *)"
  }

  rule {
    rule_name         = local.bp_rule_name2
    target_vault_name = data.aws_backup_vault.vpcintsrvr_backup_vault.name
    schedule          = "cron(0 14 * * ? *)"
  }

  advanced_backup_setting {
    backup_options = {
      WindowsVSS = "disabled"
    }
    resource_type = "EC2"
  }
}