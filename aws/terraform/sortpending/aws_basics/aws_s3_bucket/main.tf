# To create IAM role
resource "aws_iam_role" "example" {
  name = "${var.prefix}-${var.project}-${var.env}-iam-role"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "s3.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
POLICY
}

# To create an IAM policy
resource "aws_iam_policy" "example" {
  name = "${var.prefix}-${var.project}-${var.env}-iam-policy"

  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "s3:*",
            "Resource": "*"
        }
    ]
}
POLICY
}

# To attac the IAM policy with the role
resource "aws_iam_role_policy_attachment" "example" {
  role       = aws_iam_role.example.name
  policy_arn = aws_iam_policy.example.arn
}

# To create an S3 bucket
resource "aws_s3_bucket" "example" {
  bucket = "${var.prefix}-${var.project}-${var.env}-s3-bucket"

  versioning {
    enabled = true
  }
}