# To create IAM role
resource "aws_iam_role" "example" {
  name = "${var.prefix}-${var.project}-${var.env}-iam-role"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "s3.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
POLICY
}

# To create an IAM policy
resource "aws_iam_policy" "example" {
  name = "${var.prefix}-${var.project}-${var.env}-iam-policy"

  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "s3:*",
            "Resource": "*"
        }
    ]
}
POLICY
}

# To attac the IAM policy with the role
resource "aws_iam_role_policy_attachment" "example" {
  role       = aws_iam_role.example.name
  policy_arn = aws_iam_policy.example.arn
}

# To create an S3 bucket
resource "aws_s3_bucket" "example" {
  bucket = "${var.prefix}-${var.project}-${var.env}-s3-bucket"

  tags = {
    Snapshot = "True"
  }

  versioning {
    enabled = true
  }
}


# To create an IAM role for DLM 
resource "aws_iam_role" "dlm_lifecycle_role" {
  name = "dlm-lifecycle-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "dlm.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

# To create a policy for DLM
resource "aws_iam_role_policy" "dlm_lifecycle" {
  name = "dlm-lifecycle-policy"
  role = aws_iam_role.dlm_lifecycle_role.id

  policy = <<EOF
{
   "Version": "2012-10-17",
   "Statement": [
      {
         "Effect": "Allow",
         "Action": [
            "ec2:CreateSnapshot",
            "ec2:CreateSnapshots",
            "ec2:DeleteSnapshot",
            "ec2:DescribeInstances",
            "ec2:DescribeVolumes",
            "ec2:DescribeSnapshots"
         ],
         "Resource": "*"
      },
      {
         "Effect": "Allow",
         "Action": [
            "ec2:CreateTags"
         ],
         "Resource": "arn:aws:ec2:*::snapshot/*"
      }
   ]
}
EOF
}

# To create a DLM policy
resource "aws_dlm_lifecycle_policy" "example" {
  description        = "S3 Backup DLM lifecycle policy"
  execution_role_arn = aws_iam_role.dlm_lifecycle_role.arn
  state              = "ENABLED"

  policy_details {
    resource_types = ["VOLUME"]

    schedule {
      name = "Daily Snapshots"

      create_rule {
        interval      = 2
        interval_unit = "HOURS"
        times         = ["00:00"]
      }

      retain_rule {
        count = 420
      }

      tags_to_add = {
        Type = "Daily"
        Name = "SQLServerDBBackups-Daily"
      }

      copy_tags = true
    }

    schedule {
      name = "Weekly Snapshots"

      create_rule {
        interval      = 1
        interval_unit = "HOURS"
        times         = ["00:00"]
      }

      retain_rule {
        count = 840
      }

      tags_to_add = {
        Type = "Weekly"
        Name = "SQLServerDBBackups-Weekly"
      }

      copy_tags = true
    }

    schedule {
      name = "Monthly Snapshots"

      create_rule {
        interval      = 1
        interval_unit = "HOURS"
        times         = ["00:00"]
      }

      retain_rule {
        count = 1000
      }

      tags_to_add = {
        Type = "Monthly"
        Name = "SQLServerDBBackups-Monthly"
      }

      copy_tags = true
    }

    schedule {
      name = "Yearly Snapshots"

      create_rule {
        interval      = 1
        interval_unit = "HOURS"
        times         = ["00:00"]
      }

      retain_rule {
        count = 1000
      }

      tags_to_add = {
        Type = "Yearly"
        Name = "SQLServerDBBackups-Yearly"
      }

      copy_tags = true
    }

    target_tags = {
      Snapshot = "true"
    }
  }
}