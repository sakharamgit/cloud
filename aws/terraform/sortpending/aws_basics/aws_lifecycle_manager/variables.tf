##################################################################################
# VARIABLES
##################################################################################

//AWS Variables
variable "aws_region" {
  default = "ap-southeast-1"
}

variable "aws_availability_zone1" {
  default = "ap-southeast-1a"
}

variable "aws_availability_zone2" {
  default = "ap-southeast-1b"
}

variable "prefix" {
  type    = string
  default = "ss"
}

variable "project" {
  type    = string
  default = "awslm"
}

variable "env" {
  type    = string
  default = "dev"
}
