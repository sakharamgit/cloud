# # To retrieve the default VPC in AWS
# data "aws_vpcs" "example" {
#   filter {
#     name   = "vpc-id"
#     values = [var.default_vpc_id]
#   }
# }

# To retrieve the sql server engine version
data "aws_rds_engine_version" "example" {
  engine             = "sqlserver-ee"
  preferred_versions = ["*"]
}
