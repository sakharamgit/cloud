#############################################################################################################
# OUTPUT
#############################################################################################################

// Address of the mssql DB instance.
output "mssql_engine_version" {
  value = data.aws_rds_engine_version.example
}