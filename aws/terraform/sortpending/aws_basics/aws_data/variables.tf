##################################################################################
# VARIABLES
##################################################################################

//AWS Variables


variable "aws_region" {
  default = "us-east-2"
}

variable "aws_availability_zone1" {
  default = "us-east-2a"
}

variable "aws_availability_zone2" {
  default = "us-east-2b"
}

variable "prefix" {
  type    = string
  default = "ss"
}

variable "environment" {
  type    = string
  default = "dev"
}
