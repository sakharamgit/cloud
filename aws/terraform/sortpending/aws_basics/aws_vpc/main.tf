// To create a VPC
resource "aws_vpc" "example" {
  cidr_block           = var.vpc_cidr
  enable_dns_hostnames = true

  tags = {
    Name        = "${var.prefix}-${var.project}-${var.env}-vpc"
    environment = var.env
  }
}

// To create an internet gateway
resource "aws_internet_gateway" "example" {
  vpc_id = aws_vpc.example.id
  tags = {
    Name        = "${var.prefix}-${var.project}-${var.env}-ig"
    environment = var.env
  }
}

// To create an elasic ip for the NAT gateway
resource "aws_eip" "example" {
  vpc        = true
  depends_on = [aws_internet_gateway.example]
  tags = {
    Name        = "${var.prefix}-${var.project}-${var.env}-eip"
    environment = var.env
  }
}

// To create a NAT gateway
resource "aws_nat_gateway" "example" {
  allocation_id = aws_eip.example.id
  subnet_id     = element(aws_subnet.public_subnet.*.id, 0)
  depends_on    = [aws_internet_gateway.example]
  tags = {
    Name        = "${var.prefix}-${var.project}-${var.env}-nat"
    Environment = var.env
  }
}

// To create an security group
resource "aws_security_group" "nat" {
  name        = "${var.prefix}-${var.project}-${var.env}-sg"
  description = "Allow traffic to pass from the private subnet to the internet"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = [var.private_subnet_cidr]
  }
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = [var.private_subnet_cidr]
  }
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [var.vpc_cidr]
  }
  egress {
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = aws_vpc.example.id

  tags = {
    Name = "${var.prefix}-${var.project}-${var.env}-sg"
  }
}


// To create a Public subnet in the VPC
resource "aws_subnet" "public_subnet" {
  vpc_id            = aws_vpc.example.id
  cidr_block        = var.public_subnet_cidr
  availability_zone = var.aws_availability_zone

  tags = {
    Name        = "${var.prefix}-${var.project}-${var.env}-pubsubnet"
    environment = var.env
  }
}

# To create a route table for the public subnet
resource "aws_route_table" "public-rt-table" {
  vpc_id = aws_vpc.example.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.example.id
  }

  tags = {
    Name        = "${var.prefix}-${var.project}-${var.env}-pubrt"
    environment = var.env
  }
}

#  To create an AWS route for the public IG
resource "aws_route" "public_internet_gateway" {
  route_table_id         = aws_route_table.public-rt-table.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.example.id
}

# To associate the route table with the public subnet
resource "aws_route_table_association" "public-rta" {
  subnet_id      = aws_subnet.public_subnet.id
  route_table_id = aws_route_table.public-rt-table.id
}


// To create a Private subnet in the VPC
resource "aws_subnet" "private_subnet" {
  vpc_id            = aws_vpc.example.id
  cidr_block        = var.private_subnet_cidr
  availability_zone = var.aws_availability_zone

  tags = {
    Name        = "${var.prefix}-${var.project}-${var.env}-prvsubnet"
    environment = var.env
  }
}

# To create a route table for the private subnet
resource "aws_route_table" "private-rt-table" {
  vpc_id = aws_vpc.example.id

  tags = {
    Name        = "${var.prefix}-${var.project}-${var.env}-prvrt"
    environment = var.env
  }
}

# To create an AWS route for the private NAT
resource "aws_route" "private_nat_gateway" {
  route_table_id         = aws_route_table.private-rt-table.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_nat_gateway.example.id
}

# To associate the route table with the private subnet
resource "aws_route_table_association" "private-rta" {
  subnet_id      = aws_subnet.private_subnet.id
  route_table_id = aws_route_table.private-rt-table.id
}
