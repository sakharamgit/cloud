##################################################################################
# VARIABLES
##################################################################################

//Common Variables
variable "prefix" {
  type    = string
  default = "ss"
}

variable "env" {
  type    = string
  default = "dev"
}

variable "project" {
  type    = string
  default = "vpc"
}

variable "aws_region" {
  type    = string
  default = "ap-southeast-1"
}
variable "aws_availability_zone" {
  default = "ap-southeast-1a"
}

variable "aws_key_name" {
  type = string
}

variable "aws_key_path" {
  type = string
}

variable "aws_username" {
  type = string
}

//AWS Variables
variable "vpc_cidr" {
  description = "CIDR for the whole VPC"
  default     = "10.0.0.0/16"
}

variable "public_subnet_cidr" {
  description = "CIDR for the Public Subnet"
  default     = "10.0.1.0/24"
}

variable "private_subnet_cidr" {
  description = "CIDR for the Private Subnet"
  default     = "10.0.2.0/24"
}