##################################################################################
# VARIABLES
##################################################################################

//AWS Variables
variable "aws_region" {
  default = "us-east-1"
}

variable "aws_availability_zone1" {
  default = "us-east-1a"
}

variable "aws_availability_zone2" {
  default = "us-east-1b"
}

variable "prefix" {
  type    = string
  default = "ss"
}

variable "project" {
  type    = string
  default = "nlb"
}

variable "env" {
  type    = string
  default = "dev"
}

variable "vpc_cidr_range" {
  type    = string
  default = "10.0.0.0/16"
}

variable "subnet1_cidr_block" {
  type    = string
  default = "10.0.1.0/24"
}

variable "subnet2_cidr_block" {
  type    = string
  default = "10.0.2.0/24"
}