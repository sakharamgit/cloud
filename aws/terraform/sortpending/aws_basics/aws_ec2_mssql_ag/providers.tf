
// Configure the AWS Provider
provider "aws" {
  version = "~> 3.18.0"
  region  = var.aws_region
}
