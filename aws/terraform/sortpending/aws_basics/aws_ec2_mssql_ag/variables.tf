##################################################################################
# VARIABLES
##################################################################################

//AWS Variables
variable "aws_key_name" {}
variable "private_key_path" {}
variable "aws_username" {}
variable "aws_region" {
  default = "ap-southeast-1"
}

variable "aws_availability_zone1" {
  default = "ap-southeast-1a"
}

variable "aws_availability_zone2" {
  default = "ap-southeast-1b"
}

variable "prefix" {
  type    = string
  default = "ss"
}

variable "project" {
  type    = string
  default = "ec2sql"
}

variable "env" {
  type    = string
  default = "dev"
}

variable "vpc_cidr_range" {
  type    = string
  default = "10.0.0.0/16"
}

variable "subnet1_cidr_block" {
  type    = string
  default = "10.0.1.0/24"
}

variable "subnet2_cidr_block" {
  type    = string
  default = "10.0.2.0/24"
}

variable "nic1_prv_ip_addresses" {
  type = list
}

variable "nic2_prv_ip_addresses" {
  type = list
}

variable winusername {
  type    = string
  default = "winadmin"
}

variable winpassword {
  type    = string
  default = "Password1!"
}

variable "trusted_domain" {
  type    = string
  default = "*.compute.amazonaws.com"
}

variable "sqlusername" {
  type    = string
  default = "sqladmin"
}

variable "sqlpassword" {
  type    = string
  default = "Password1!"
}

variable "sql_config_script_path" {
  type = string
}

variable "winclustersetup_script_path" {
  type = string
}

variable "alwaysonsetup_script_path" {
  type = string
}