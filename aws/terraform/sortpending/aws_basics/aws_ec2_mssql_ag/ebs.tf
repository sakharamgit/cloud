# To create an EBS for the sql data and log files
resource "aws_ebs_volume" "az1" {
  availability_zone = var.aws_availability_zone1
  size              = 40
  type              = "io1"
  iops              = 200

  tags = {
    Name        = "${var.prefix}-${var.project}-${var.env}-ebs1"
    Environment = var.env
  }
}

resource "aws_ebs_volume" "az2" {
  availability_zone = var.aws_availability_zone2
  size              = 40
  type              = "io1"
  iops              = 200

  tags = {
    Name        = "${var.prefix}-${var.project}-${var.env}-ebs2"
    Environment = var.env
  }
}

// To attach the ebs volume to the virtual machine ec2az1
resource "aws_volume_attachment" "ebs_att1" {
  depends_on  = [aws_instance.ec2az1]
  device_name = "xvdh"
  volume_id   = aws_ebs_volume.az1.id
  instance_id = aws_instance.ec2az1.id
}

resource "aws_volume_attachment" "ebs_att2" {
  depends_on  = [aws_instance.ec2az2]
  device_name = "xvdh"
  volume_id   = aws_ebs_volume.az2.id
  instance_id = aws_instance.ec2az2.id
}
