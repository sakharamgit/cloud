#############################################################################################################
# OUTPUT
#############################################################################################################

output "aws_instance_public_dns1" {
  value = aws_instance.ec2az1.public_dns
}

output "aws_instance_public_dns2" {
  value = aws_instance.ec2az2.public_dns
}