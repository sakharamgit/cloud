
# To create a sql login for CloudBasic Migration

$sqlusername = ${var.sqlusername}
$sqlpassword = ${var.$sqlpassword}

$sqlserver = $env:COMPUTERNAME

$sqlLoginQuery = @"
IF(NOT EXISTS (SELECT name FROM sys.syslogins WHERE name = 'sqladmin'))
BEGIN
CREATE LOGIN sqladmin WITH PASSWORD = 'Password1!'

CREATE USER sqladmin FOR LOGIN sqladmin WITH DEFAULT_SCHEMA = dbo; 

EXEC master..sp_addsrvrolemember @loginame = N'sqladmin', @rolename = N'sysadmin'
END
"@

try
{
    Invoke-Sqlcmd -ServerInstance $sqlserver -Query $sqlLoginQuery -ErrorAction STOP
}
catch
{
    continue
}


# To change the authentication mode
$sqlAuthModeQuery = @"
IF OBJECT_ID('tempdb..#authmode') is not null DROP TABLE #authmode
CREATE TABLE #authmode (value VARCHAR(20), data VARCHAR(10))
DECLARE @AuthenticationMode INT 

INSERT INTO #authmode
EXEC master.dbo.xp_instance_regread N'HKEY_LOCAL_MACHINE',N'Software\Microsoft\MSSQLServer\MSSQLServer',N'LoginMode'

set @AuthenticationMode = (select data from #authmode)

If(@AuthenticationMode = 1)
begin
	EXEC xp_instance_regwrite N'HKEY_LOCAL_MACHINE',N'Software\Microsoft\MSSQLServer\MSSQLServer', N'LoginMode', REG_DWORD, 2
end
"@

try
{
    Invoke-Sqlcmd -ServerInstance $sqlserver -Query $sqlAuthModeQuery -ErrorAction STOP
}
catch
{
    continue
}


# To restart the sql service
try {
    Get-Service -Name 'MSSQLServer' | Restart-Service -Force
}
catch {
    continue
}

try {
    Get-Service -Name 'SQLServerAgent' | Restart-Service -Force
}
catch {
    continue
}
