#############################################################################################################
# DATA
#############################################################################################################

// To retrieve the latest AMI for Windows Server 2019
data "aws_ami" "windows" {
  most_recent = true

  filter {
    name   = "image-id"
    values = ["ami-0b8aa9827ffff6e39"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["801119661308"] # Canonical
}

# To run the below script on the ec2 instance creation
data "template_file" "winrm_config" {
  template = file("./files/winrm_config.ps1")
  vars = {
    windows_username = var.winusername
    windows_password = var.winpassword
  }
}

#############################################################################################################
# RESOURCES
#############################################################################################################

// To create a security group
resource "aws_security_group" "example" {
  name        = "${var.prefix}-${var.project}-${var.env}-sg"
  description = "AWS Security Group for EC2"
  vpc_id      = aws_vpc.example.id

  ingress {
    description = "RDP"
    from_port   = 3389
    to_port     = 3389
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "SQL"
    from_port   = 1433
    to_port     = 1433
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "WINRM-HTTP"
    from_port   = 5985
    to_port     = 5985
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "WINRM-HTTPS"
    from_port   = 5986
    to_port     = 5986
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name        = "${var.prefix}-${var.project}-${var.env}-sg"
    environment = var.env
  }
}

// To create a network interface for EC2 in AZ1
resource "aws_network_interface" "nic1" {
  subnet_id         = aws_subnet.az1.id
  security_groups   = [aws_security_group.example.id]
  private_ips_count = "2"
  #private_ips       = var.nic1_prv_ip_addresses

  tags = {
    Name        = "${var.prefix}-${var.project}-${var.env}-ec2-nic1"
    environment = var.env
  }
}

// To create a network interface for EC2 in AZ2
resource "aws_network_interface" "nic2" {
  subnet_id         = aws_subnet.az2.id
  security_groups = [aws_security_group.example.id]
  private_ips_count = "2"
  #private_ips       = var.nic2_prv_ip_addresses

  tags = {
    Name        = "${var.prefix}-${var.project}-${var.env}-ec2-nic2"
    environment = var.env
  }
}

// To create elastic ip to associate with the network interfaces
resource "aws_eip" "eip1" {
  depends_on = [aws_network_interface.nic1]
  vpc                       = true
  network_interface         = aws_network_interface.nic1.id

  tags = {
    Name        = "${var.prefix}-${var.project}-${var.env}-eip1"
    environment = var.env
  }
}

resource "aws_eip" "eip2" {
  depends_on = [aws_network_interface.nic2]
  vpc                       = true
  network_interface         = aws_network_interface.nic2.id

  tags = {
    Name        = "${var.prefix}-${var.project}-${var.env}-eip2"
    environment = var.env
  }
}

// Using AWS Instance Profiles and IAM Roles instead of Client Keys and Secret Keys for the Prometheus Instance
// To create AWS IAM Role
resource "aws_iam_role" "example" {
  name = "${var.prefix}-${var.project}-${var.env}-ec2-iam"
  path = "/"

  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
               "Service": "ec2.amazonaws.com"
            },
            "Effect": "Allow",
            "Sid": ""
        }
    ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "example" {
  role       = aws_iam_role.example.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ReadOnlyAccess"
}

resource "aws_iam_instance_profile" "example" {
  name = "${var.prefix}-${var.project}-${var.env}-ec2-iam-profile"
  role = aws_iam_role.example.name
}

// To create Amazon EC2 Windows instance in AZ1
resource "aws_instance" "ec2az1" {
  ami                         = data.aws_ami.windows.id
  instance_type               = "t2.xlarge"
  user_data                   = data.template_file.winrm_config.rendered
  key_name                    = var.aws_key_name
  iam_instance_profile        = aws_iam_instance_profile.example.name

  network_interface {
    network_interface_id = aws_network_interface.nic1.id
    device_index         = 0
  }

  tags = {
    Name        = "${var.prefix}-${var.project}-${var.env}-ec2-mssql1"
    environment = var.env
  }
}


// To create Amazon EC2 Windows instance in AZ2
resource "aws_instance" "ec2az2" {
  ami                         = data.aws_ami.windows.id
  instance_type               = "t2.xlarge"
  user_data                   = data.template_file.winrm_config.rendered
  key_name                    = var.aws_key_name

  network_interface {
    network_interface_id = aws_network_interface.nic2.id
    device_index         = 0
  }

  tags = {
    Name        = "${var.prefix}-${var.project}-${var.env}-ec2-mssql2"
    environment = var.env
  }
}

# To add some delay for the EC2 instance to start
resource "time_sleep" "wait_4_minutes" {
  depends_on = [aws_instance.ec2az1]
  create_duration = "4m"
}


# To execute scripts on the remote EC2 instance ec2az1
resource "null_resource" "ps_script_exec1" {
  depends_on = [time_sleep.wait_4_minutes]

  connection {
    type     = "winrm"
    host     = aws_eip.eip1.public_ip
    user     = var.winusername
    password = var.winpassword
  }
  provisioner "file" {
    source      = var.sql_config_script_path
    destination = "C:\\bootstrap\\sql_config.ps1"
  }
  provisioner "file" {
    source      = var.winclustersetup_script_path
    destination = "C:\\bootstrap\\winclustersetup.ps1"
  }
  provisioner "file" {
    source      = var.alwaysonsetup_script_path
    destination = "C:\\bootstrap\\alwaysonsetup.ps1"
  }

  provisioner "local-exec" {
    command     = "Set-Item WSMan:\\localhost\\client\\Trustedhosts -Value '${var.trusted_domain}' -Force"
    interpreter = ["PowerShell", "-Command"]
  }

  provisioner "remote-exec" {
    inline = [
      "Powershell C:\\bootstrap\\sql_config.ps1 -sqlusername ${var.sqlusername} -sqlpassword ${var.sqlpassword}",
      "Powershell C:\\bootstrap\\winclustersetup.ps1"
      #"Powershell C:\\WinRM\\alwaysonsetup.ps1"
    ]
  }

}

# To execute scripts on the remote EC2 instance ec2az2
resource "null_resource" "ps_script_exec2" {
  depends_on = [time_sleep.wait_4_minutes]

  connection {
    type     = "winrm"
    host     = aws_eip.eip2.public_ip
    user     = var.winusername
    password = var.winpassword
  }
  provisioner "file" {
    source      = var.sql_config_script_path
    destination = "C:\\bootstrap\\sql_config.ps1"
  }
  provisioner "file" {
    source      = var.winclustersetup_script_path
    destination = "C:\\bootstrap\\winclustersetup.ps1"
  }  

  provisioner "local-exec" {
    command     = "Set-Item WSMan:\\localhost\\client\\Trustedhosts -Value '${var.trusted_domain}' -Force"
    interpreter = ["PowerShell", "-Command"]
  }

  provisioner "remote-exec" {
    inline = [
      "Powershell C:\\bootstrap\\sql_config.ps1 -sqlusername ${var.sqlusername} -sqlpassword ${var.sqlpassword}",
      "Powershell C:\\bootstrap\\winclustersetup.ps1"
    ]
  }

}

