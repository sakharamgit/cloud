// AWS Credentials
aws_key_name                = "aws-cloudbasic"
private_key_path            = "..\\..\\..\\common\\aws-cloudbasic.pem"
aws_username                = "ec2-user"
sql_config_script_path      = "./files/sql_config.ps1"
winclustersetup_script_path = "./files/winclustersetup.ps1"
alwaysonsetup_script_path   = "./files/alwaysonsetup.ps1"



vpc_cidr_range        = "10.0.0.0/16"
subnet1_cidr_block    = "10.0.1.0/24"
subnet2_cidr_block    = "10.0.2.0/24"
nic1_prv_ip_addresses = ["10.0.1.20", "10.0.1.21"]
nic2_prv_ip_addresses = ["10.0.2.20", "10.0.2.21"]

