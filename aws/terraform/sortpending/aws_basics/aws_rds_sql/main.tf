# To retrieve the default VPC in AWS
data "aws_vpcs" "example" {
  filter {
    name   = "vpc-id"
    values = [var.default_vpc_id]
  }
}

# To retrieve the default security group
data "aws_security_group" "example" {
  filter {
    name   = "group-id"
    values = [var.default_security_group_id]
  }
}

# To retrieve the default subnets
data "aws_subnet" "subnet1" {
  id = var.default_subnet_id1
}

data "aws_subnet" "subnet2" {
  id = var.default_subnet_id2
}


# To create a subnet Group
resource "aws_db_subnet_group" "example" {
  name        = "${var.prefix}-${var.environment}-rds-mssql-subnet-group"
  description = "The ${var.environment} rds-mssql private subnet group."
  subnet_ids  = [data.aws_subnet.subnet1.id, data.aws_subnet.subnet2.id]

  tags = {
    Name = "${var.environment}-rds-mssql-subnet-group"
    Env  = var.environment
  }
}

# To create an ingress rule for MSSQL port in the default security group
resource "aws_security_group_rule" "example" {
  type              = "ingress"
  from_port         = 1433
  to_port           = 1433
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = data.aws_security_group.example.id
}

# To create the MSSQL RDS instance
resource "aws_db_instance" "example" {
  depends_on                = [aws_db_subnet_group.example]
  identifier                = "${var.prefix}-${var.environment}-mssql"
  allocated_storage         = var.rds_allocated_storage
  license_model             = "license-included"
  storage_type              = "gp2"
  engine                    = "sqlserver-se"
  engine_version            = "12.00.4422.0.v1"
  instance_class            = "db.m4.large"
  multi_az                  = "false"
  username                  = var.sqlUsername
  password                  = var.sqlPassword
  publicly_accessible       = "true"
  vpc_security_group_ids    = [data.aws_security_group.example.id]
  db_subnet_group_name      = aws_db_subnet_group.example.id
  backup_retention_period   = 0
  skip_final_snapshot       = "true"
  final_snapshot_identifier = "${var.environment}-mssql-final-snapshot"
}