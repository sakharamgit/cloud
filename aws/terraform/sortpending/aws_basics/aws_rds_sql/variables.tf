##################################################################################
# VARIABLES
##################################################################################

//AWS Variables
variable "aws_key_name" {}
variable "private_key_path" {}
variable "aws_username" {}
variable "sqlUsername" {}
variable "sqlPassword" {}

variable "aws_region" {
  default = "us-east-2"
}

variable "aws_availability_zone1" {
  default = "us-east-2a"
}

variable "aws_availability_zone2" {
  default = "us-east-2b"
}

variable "prefix" {
  type    = string
  default = "ss"
}

variable "environment" {
  type    = string
  default = "dev"
}

variable "rds_allocated_storage" {
  type    = string
  default = 20
}

variable "default_vpc_id" {
  type = string
}

variable "default_subnet_id1" {
  type = string
}

variable "default_subnet_id2" {
  type = string
}

variable "default_security_group_id" {
  type = string
}
  