// To retrieve the latest AMI for Ubuntu
data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

// To create a VPC
resource "aws_vpc" "example" {
  cidr_block = "172.16.0.0/16"

  tags = {
    Name        = "${var.aws_prefix}-aws-vpc"
    environment = var.aws_environment
  }
}

// To create a subnet in the VPC
resource "aws_subnet" "example" {
  vpc_id            = aws_vpc.example.id
  cidr_block        = "172.16.0.0/24"
  availability_zone = var.aws_availability_zone

  tags = {
    Name        = "${var.aws_prefix}-aws-subnet"
    environment = var.aws_environment
  }
}

// To create a network interface
resource "aws_network_interface" "example" {
  subnet_id   = aws_subnet.example.id
  private_ips = ["172.16.0.100"]

  tags = {
    Name        = "${var.aws_prefix}-aws-ec2-nic"
    environment = var.aws_environment
  }
}

// To create a security group
resource "aws_security_group" "example" {
  name        = "aws-sg"
  description = "AWS Security Group for EC2"
  vpc_id      = aws_vpc.example.id

  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [aws_vpc.example.cidr_block]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [aws_vpc.example.cidr_block]
  }

  tags = {
    Name        = "${var.aws_prefix}-aws-sg"
    environment = var.aws_environment
  }
}

// To create Amazon EC2 instance
resource "aws_instance" "example" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = "t2.micro"
  //user_data              = data.template_file.ubuntu.template
  key_name                    = var.aws_key_name
  subnet_id                   = aws_subnet.example.id
  vpc_security_group_ids      = [aws_security_group.example.id]
  associate_public_ip_address = true


  tags = {
    Name        = "${var.aws_prefix}-aws-ec2"
    environment = var.aws_environment
  }
}
