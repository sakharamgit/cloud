# Locals
locals {
  role_policy_arns = [
    "arn:aws:iam::aws:policy/service-role/AmazonEC2RoleforSSM",
    "arn:aws:iam::aws:policy/CloudWatchAgentServerPolicy"
  ]
}


#############################################################################################################
# DATA
#############################################################################################################

// To retrieve the latest AMI for Windows
data "aws_ami" "windows" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["Windows_Server-2019-English-Full-ContainersLatest*"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

# To run the below script on the ec2 instance creation
data "template_file" "winrm_config" {
  template = file("./files/winrm_config.ps1")
  vars = {
    windows_username = var.winusername
    windows_password = var.winpassword
  }
}

#############################################################################################################
# RESOURCES
#############################################################################################################

# This uses the default VPC. It will not delete it on destroy
resource "aws_default_vpc" "default" {

}

# This will create the AWS security group to allow WinRM to the AWS default VPC
resource "aws_security_group" "example" {
  name        = "${var.prefix}-${var.project}-${var.env}-sg"
  description = "AWS Security Group for EC2"
  vpc_id      = aws_default_vpc.default.id

  ingress {
    from_port   = 5985
    to_port     = 5985
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 5986
    to_port     = 5986
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "RDP"
    from_port   = 3389
    to_port     = 3389
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name        = "${var.prefix}-${var.project}-${var.env}-aws-sg"
    environment = var.env
  }
}


// Using AWS Instance Profiles and IAM Roles instead of Client Keys and Secret Keys for the Prometheus Instance
// To create AWS IAM Role
resource "aws_iam_role" "ec2_role" {
  name = "${var.prefix}-${var.project}-${var.env}-ec2-iam"
  path = "/"

  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
               "Service": "ec2.amazonaws.com"
            },
            "Effect": "Allow",
            "Sid": ""
        }
    ]
}
EOF
}

# To create an S3 bucket
resource "aws_s3_bucket" "sql-db-backup-s3" {
  bucket        = "${var.prefix}-${var.project}-${var.env}-s3-bucket"
  force_destroy = true
  versioning {
    enabled = true
  }
}

# To create a role policy for CloudWatch SSM agent
resource "aws_iam_role_policy" "cw-role-policy" {
  name = "${var.prefix}-${var.project}-${var.env}-cw-policy"
  role = aws_iam_role.ec2_role.id
  policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Effect" : "Allow",
          "Action" : [
            "ssm:GetParameter"
          ],
          "Resource" : "*"
        }
      ]
    }
  )
}

# To create a role policy for S3 bucket
resource "aws_iam_role_policy" "s3_iam_role_policy" {
  name = "s3_iam_role_policy"
  role = aws_iam_role.ec2_role.id
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "s3:GetBucketLocation",
        "s3:ListAllMyBuckets"
      ],
      "Resource": "*"
    },
    {
      "Effect": "Allow",
      "Action": ["s3:ListBucket"],
      "Resource": ["arn:aws:s3:::${aws_s3_bucket.sql-db-backup-s3.bucket}"]
    },
    {
      "Effect": "Allow",
      "Action": [
        "s3:PutObject",
        "s3:GetObject",
        "s3:DeleteObject"
      ],
      "Resource": ["arn:aws:s3:::${aws_s3_bucket.sql-db-backup-s3.bucket}/*"]
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "cw-policy-attachment" {
  count = length(local.role_policy_arns)

  role       = aws_iam_role.ec2_role.name
  policy_arn = element(local.role_policy_arns, count.index)
}

resource "aws_iam_instance_profile" "ec2_cw" {
  name = "${var.prefix}-${var.project}-${var.env}-ec2-cw-profile"
  role = aws_iam_role.ec2_role.name
}

// To create AWS EC2 instance
resource "aws_instance" "example" {
  ami                    = data.aws_ami.windows.id
  instance_type          = "t3.medium"
  key_name               = var.aws_key_name
  vpc_security_group_ids = [aws_security_group.example.id]
  user_data              = data.template_file.winrm_config.rendered
  iam_instance_profile   = aws_iam_instance_profile.ec2_cw.name

  tags = {
    Name        = "${var.prefix}-${var.project}-${var.env}-aws-ec2"
    environment = var.env
  }
}


resource "null_resource" "ps_script_exec" {
  depends_on = [aws_instance.example]

  connection {
    type     = "winrm"
    host     = aws_instance.example.public_ip
    user     = var.winusername
    password = var.winpassword
  }
  provisioner "file" {
    source      = var.cw_agent_config_path
    destination = "C:\\Program Files\\Amazon\\AmazonCloudWatchAgent\\config.json"
  }
  provisioner "file" {
    source      = var.start_cw_agent_path
    destination = "C:\\files\\start_cw_agent.ps1"
  }

  provisioner "local-exec" {
    command     = "Set-Item WSMan:\\localhost\\client\\Trustedhosts -Value '${var.trusted_domain}' -Force"
    interpreter = ["PowerShell", "-Command"]
  }

  provisioner "remote-exec" {
    inline = [
      "Powershell C:\\files\\start_cw_agent.ps1",
    ]
  }

}

