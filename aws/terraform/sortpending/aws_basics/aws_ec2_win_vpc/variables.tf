##################################################################################
# VARIABLES
##################################################################################

//AWS Variables
variable "aws_key_name" {}
variable "private_key_path" {}
variable "aws_username" {}
variable "aws_region" {
  default = "ap-southeast-1"
}
variable "aws_availability_zone" {
  default = "ap-southeast-1a"
}

//Azure Variables
variable "prefix" {
  type    = string
  default = "ss"
}

variable "project" {
  type    = string
  default = "ec2vpc"
}

variable "env" {
  type    = string
  default = "dev"
}

variable winusername {
  type    = string
  default = "winadmin"
}

variable winpassword {
  type    = string
  default = "Qwerty@123"
}

variable trusted_domain {
  type    = string
  default = "*.compute.amazonaws.com"
}

variable "sample_ps_script_path1" {
  type = string
}

variable "sample_ps_script_path2" {
  type = string
}

variable "vpc_cidr_range" {
  type    = string
  default = "10.0.0.0/16"
}

variable "subnet_cidr_block" {
  type    = string
  default = "10.0.1.0/24"
}

variable "nic_private_ip" {
  type = list
  default = ["10.0.1.15"]
}