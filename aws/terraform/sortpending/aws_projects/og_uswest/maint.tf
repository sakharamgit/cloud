# To create AWS IAM Role for  elasticache instances
resource "aws_iam_role" "elasticache_iam_role" {
  name = "elasticache_iam_role"
  path = "/"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "elasticache.amazonaws.com"
        }
      },
    ]
  })

 
  inline_policy {
    name = "elasticache_iam_policy" 

    policy = jsonencode({
    "Version" = "2012-10-17",
    "Statement" = [
      {
        "Sid" = "VisualEditor0",
        "Effect" = "Allow",
        "Action" = [
          "elasticache:RemoveTagsFromResource",
          "elasticache:DescribeCacheParameters",
          "elasticache:ModifyCacheParameterGroup",
          "elasticache:DescribeCacheParameterGroups",
          "elasticache:CreateCacheParameterGroup",
          "elasticache:AddTagsToResource",
          "elasticache:DeleteCacheParameterGroup",
          "elasticache:DescribeCacheClusters",
          "elasticache:ListTagsForResource"
        ],
        "Resource" = "*"
      }
    ]
  })
  }
}
