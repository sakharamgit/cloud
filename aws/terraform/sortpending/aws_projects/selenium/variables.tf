variable aws_region {
  type    = string
  default = "ap-southeast-1"
}

variable "aws_key_name" {
  default = "xor_aws_terraform_user_key"
}

variable winusername {
  type    = string
  default = "winadmin"
}

variable winpassword {
  type    = string
  default = "P@ssword1!"
}

variable "prefix" {
  type    = string
  default = "ss"
}

variable "project" {
  type    = string
  default = "selnm"
}

variable "env" {
  type    = string
  default = "dev"
}

variable "sample_ps_script_path1" {
  type = string
  default = "./files/ec2_configuration.ps1"
}

variable trusted_domain {
  type    = string
  default = "*.compute.amazonaws.com"
}