# To add a local user
$PlainPassword = "Testing@2021"
$password = $PlainPassword | ConvertTo-SecureString -AsPlainText -Force
New-LocalUser “tester” -Password $password
Add-LocalGroupMember -Group "Remote Desktop Users" -Member "Tester"

# To install Chrome
$Path = $env:TEMP;
$Installer = "chrome_installer.exe";
Invoke-WebRequest "https://dl.google.com/tag/s/appguid%3D%7B8A69D345-D564-463C-AFF1-A69D9E530F96%7D%26iid%3D%7BFC3517F7-7E6A-6353-1146-413A2935A066%7D%26lang%3Den%26browser%3D4%26usagestats%3D0%26appname%3DGoogle%2520Chrome%26needsadmin%3Dprefers%26ap%3Dx64-stable-statsdef_1%26installdataindex%3Dempty/update2/installers/ChromeSetup.exe" -OutFile $Path\$Installer;
Start-Process -FilePath $Path\$Installer -Args "/silent /install" -Verb RunAs -Wait;
Remove-Item $Path\$Installer;

# To install Chocolatey
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))

Invoke-Item C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe

# To install JDK
choco install jdk8 -y
netsh advfirewall firewall add rule name="java" dir=in action=allow program="C:\Program Files\Java\jdk1.8.0_211\bin\java.exe" enable=yes

# To download Selenium
mkdir "C:\Selenium"
Invoke-WebRequest -Uri https://selenium-release.storage.googleapis.com/3.141/selenium-server-standalone-3.141.59.jar -OutFile "C:\files\selenium.jar"
Invoke-WebRequest -Uri https://chromedriver.storage.googleapis.com/90.0.4430.24/chromedriver_win32.zip -OutFile "C:\files\chromedriver_win32.zip"
