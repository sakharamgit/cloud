
locals {
  sel_info               = yamldecode(file("./config.yaml"))
  ami_name               = local.sel_info.ami_name
  instance_count         = local.sel_info.instance_count
  instance_type          = local.sel_info.instance_type
  security_group_name    = local.sel_info.security_group_name
}

// To retrieve the latest AMI for Windows
data "aws_ami" "windows" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = [local.ami_name]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

# To get the security group id
data "aws_security_group" "selenium-sg" {
  name = local.security_group_name
}

# To run the below script on the ec2 instance creation
data "template_file" "winrm_config" {
  template = file("./files/winrm_config.ps1")
  vars = {
    windows_username = var.winusername
    windows_password = var.winpassword
  }
}

// To create AWS EC2 instances
resource "aws_instance" "example" {
  ami                    = data.aws_ami.windows.id
  instance_type          = "t3.medium"
  key_name               = var.aws_key_name
  vpc_security_group_ids = [data.aws_security_group.selenium-sg.id]
  user_data              = data.template_file.winrm_config.rendered

  tags = {
    Name        = "${var.prefix}-${var.project}-${var.env}-aws-ec2"
    environment = var.env
  }
}

resource "null_resource" "ps_script_exec" {
  depends_on = [aws_instance.example]

  connection {
    type     = "winrm"
    host     = aws_instance.example.public_ip
    user     = var.winusername
    password = var.winpassword
  }
  provisioner "file" {
    source      = var.sample_ps_script_path1
    destination = "C:\\files\\ec2_configuration.ps1"
  }

  provisioner "local-exec" {
    command     = "Set-Item WSMan:\\localhost\\client\\Trustedhosts -Value '${var.trusted_domain}' -Force"
    interpreter = ["PowerShell", "-Command"]
  }

  provisioner "remote-exec" {
    inline = [
      "Powershell C:\\files\\ec2_configuration.ps1"
    ]
  }

}
