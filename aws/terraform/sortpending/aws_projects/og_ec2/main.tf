
locals {
  es_info                             = yamldecode(file("${path.module}/elasticsearch.yaml"))
  elasticsearch_version               = local.es_info.elasticsearch_version
  es_domain_name                      = local.es_info.es_domain_name
  es_instance_type                    = local.es_info.es_instance_type
  instance_count                      = local.es_info.instance_count
  zone_awareness_enabled              = local.es_info.zone_awareness_enabled
  availability_zone_count             = local.es_info.availability_zone_count
  ebs_volume_size_gb                  = local.es_info.ebs_volume_size_gb
  ebs_volume_type                     = local.es_info.ebs_volume_type
  encrypt_at_rest_enabled             = local.es_info.encrypt_at_rest_enabled
  automated_snapshot_start_hour_in_PT = local.es_info.automated_snapshot_start_hour_in_PT
  dedicated_master_enabled            = local.es_info.dedicated_master_enabled
  dedicated_master_count              = local.es_info.dedicated_master_count
  dedicated_master_type               = local.es_info.dedicated_master_type
  log_publishing_index_enabled        = local.es_info.log_publishing_index_enabled
  log_publishing_search_enabled       = local.es_info.log_publishing_search_enabled
  log_publishing_application_enabled  = local.es_info.log_publishing_application_enabled
  node_to_node_encryption_enabled     = local.es_info.node_to_node_encryption_enabled
  ebs_enabled                         = local.es_info.ebs_enabled
}

# Declare the data source
data "aws_vpc" "es_vpc" {
  id = "vpc-08ab3b7c966e22704"
}

data "aws_subnet_ids" "es_subnet_ids" {
  vpc_id = data.aws_vpc.es_vpc.id
}

data "aws_subnet" "data_es_subnet" {
  count = length(data.aws_subnet_ids.es_subnet_ids.ids)
  id    = (split(",",join(",", data.aws_subnet_ids.es_subnet_ids.ids)))[count.index]
}

locals {
  subnet_azs = [ for azs in data.aws_subnet.data_es_subnet[*].availability_zone : azs ]
  distinct_subnet_azs = distinct(local.subnet_azs)

  es_subnets = { for subnetdetails, value in [ for subnets in data.aws_subnet.data_es_subnet :
      {
        az = subnets.availability_zone
        subnetid = subnets.id
      }]
    : subnetdetails => value
  }

  es_subnets_0 = try(compact([for subnetid, value in local.es_subnets : value.az == local.distinct_subnet_azs[0] ? value.subnetid : ""])[0],null)
  es_subnets_1 = try(compact([for subnetid, value in local.es_subnets : value.az == local.distinct_subnet_azs[1] ? value.subnetid : ""])[0],null)
  es_subnets_2 = try(compact([for subnetid, value in local.es_subnets : value.az == local.distinct_subnet_azs[2] ? value.subnetid : ""])[0],null)
  es_subnets_3 = try(compact([for subnetid, value in local.es_subnets : value.az == local.distinct_subnet_azs[3] ? value.subnetid : ""])[0],null)
}




# output "subnet_id" {
#   value = data.aws_subnet.data_es_subnet
# }

# output "vpc" {
#   value = data.aws_vpc.es_vpc.tags
# }

# output "subnet_azs" {
#   value = local.subnet_azs
# }

output "distinct_subnet_azs" {
  value = local.distinct_subnet_azs
}

# output "es_subnets" {
#   value = local.es_subnets
# }

output "es_subnets_0" {
  value = local.es_subnets_0
}

output "es_subnets_1" {
  value = local.es_subnets_1
}

output "es_subnets_2" {
  value = local.es_subnets_2
}

output "es_subnets_3" {
  value = local.es_subnets_3
}

# output "data_es_subnets" {
#   value = data.aws_subnet.data_es_subnet
# }


resource "aws_security_group" "select_vpc" {
  name        = "allow_tls"
  description = "Placed in storage vpc"
  vpc_id      = data.aws_vpc.es_vpc.id
}


# Creating log group
resource "aws_cloudwatch_log_group" "eslogroup" {
  name = "es_loggroup_name"
}

# Creating Elasticsearch Domain

resource "aws_elasticsearch_domain" "default" {
  domain_name           = local.es_domain_name
  elasticsearch_version = local.elasticsearch_version

  ebs_options {
    ebs_enabled = local.ebs_enabled
    volume_size = local.ebs_volume_size_gb
    volume_type = local.ebs_volume_type
    # iops        = local.ebs_iops
  }

  vpc_options {
    subnet_ids = compact([
      local.availability_zone_count - 1 >= 0 ? local.es_subnets_0 : null ,
      local.availability_zone_count - 2 >= 0 ? local.es_subnets_1 : null ,
      local.availability_zone_count - 3 >= 0 ? local.es_subnets_2 : null ,
      local.availability_zone_count - 4 >= 0 ? local.es_subnets_3 : null ,
    ])
  }

  encrypt_at_rest {
    enabled = local.encrypt_at_rest_enabled
  }

  cluster_config {
    instance_count           = local.instance_count
    instance_type            = local.es_instance_type
    dedicated_master_enabled = local.dedicated_master_enabled
    dedicated_master_count   = local.dedicated_master_count
    dedicated_master_type    = local.dedicated_master_type
    zone_awareness_enabled   = local.zone_awareness_enabled

    zone_awareness_config {
      availability_zone_count = local.availability_zone_count
    }
  }

  node_to_node_encryption {
    enabled = local.node_to_node_encryption_enabled
  }

  snapshot_options {
    automated_snapshot_start_hour = local.automated_snapshot_start_hour_in_PT
  }

  log_publishing_options {
    enabled                  = local.log_publishing_index_enabled
    log_type                 = "INDEX_SLOW_LOGS"
    cloudwatch_log_group_arn = aws_cloudwatch_log_group.eslogroup.arn
  }

  log_publishing_options {
    enabled                  = local.log_publishing_search_enabled
    log_type                 = "SEARCH_SLOW_LOGS"
    cloudwatch_log_group_arn = aws_cloudwatch_log_group.eslogroup.arn
  }

  log_publishing_options {
    enabled                  = local.log_publishing_application_enabled
    log_type                 = "ES_APPLICATION_LOGS"
    cloudwatch_log_group_arn = aws_cloudwatch_log_group.eslogroup.arn
  }

}
