# To retrieve the values for EC2
locals {
  ec2_info      = yamldecode(file("./sample.yaml"))
  owner         = local.ec2_info.owner
  tag_purpose   = local.ec2_info.tag_purpose
  instance_type = local.ec2_info.instance_type
  aws_key_name  = local.ec2_info.aws_key_name
  sg_id         = local.ec2_info.sg_id
  vpc_name      = local.ec2_info.vpc_name
  ec2_restore   = try(local.ec2_info.ec2_restore,false)
}

# To retrieve the  AMI for vpcintegration server
data "aws_ami" "vpcintg_ami" {
  most_recent = true

  filter {
    name   = "state"
    values = ["available"]
  }

  filter {
    name   = "tag:purpose"
    values = [local.tag_purpose]
  }

  owners = [local.owner] # Canonical
}

# To retrieve the lastest EBS snapshots
data "aws_ebs_snapshot" "ebs_root" {
  most_recent = true
  owners      = [local.owner]

  filter {
    name   = "volume-size"
    values = ["300"]
  }

  filter {
    name   = "tag:purpose"
    values = [local.tag_purpose]
  }
}

data "aws_ebs_snapshot" "ebs_extra" {
  most_recent = true
  owners      = [local.owner]

  filter {
    name   = "volume-size"
    values = ["512"]
  }

  filter {
    name   = "tag:purpose"
    values = [local.tag_purpose]
  }
}

data "aws_ebs_snapshot" "ebs_ftp" {
  most_recent = true
  owners      = [local.owner]

  filter {
    name   = "volume-size"
    values = ["1024"]
  }

  filter {
    name   = "tag:purpose"
    values = [local.tag_purpose]
  }
}

data "aws_ebs_snapshot" "ebs_temp" {
  most_recent = true
  owners      = [local.owner]

  filter {
    name   = "volume-size"
    values = ["128"]
  }

  filter {
    name   = "tag:purpose"
    values = [local.tag_purpose]
  }
}

# To retrieve the subnet
data "aws_subnet" "vpcintg_subnet" {
  filter {
    name   = "tag:Name"
    values = [local.vpc_name]
  }
}

# To create a network interface
resource "aws_network_interface" "vpcintg_nic" {
  count             = local.ec2_restore ? 1 : 0
  subnet_id         = data.aws_subnet.vpcintg_subnet.id
  security_groups   = [local.sg_id]
}

# To create an public ip
resource "aws_eip" "vpcintg_eip" {
  count                     = local.ec2_restore ? 1 : 0
  vpc                       = true
  network_interface         = aws_network_interface.vpcintg_nic[count.index].id
}

# To create the vpcintegration server
resource "aws_instance" "vpcintg" {
  count                = local.ec2_restore ? 1 : 0
  ami                  = data.aws_ami.vpcintg_ami.id
  instance_type        = local.instance_type
  key_name             = local.aws_key_name

  network_interface {
    network_interface_id = aws_network_interface.vpcintg_nic[count.index].id
    device_index         = 0
  }
}


# Output 
output "vpc_integration_server_ip" {
  value = aws_instance.vpcintg.*.public_ip
}
