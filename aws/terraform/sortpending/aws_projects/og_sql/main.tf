locals {
  sg_info            = yamldecode(file("./sg.yaml"))

}


output "sg" {
    value = [local.sg_info]
}