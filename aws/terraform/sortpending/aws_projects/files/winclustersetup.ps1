################################################################
# Script to Install Failover Clustering Role on the server
################################################################

# Variables
$serverFeature = 'Failover-Clustering'
$scriptLogPath = "C:\bootstrap\initial_config.txt"


# Script

If((Get-WindowsFeature -Name $serverFeature).Installed -ne 'True')
{
    try 
    {
        "Installing the Failover Clustering Feature on the server" | Out-File -FilePath $scriptLogPath -Append
         Install-WindowsFeature $serverFeature -IncludeManagementTools -Restart
    }
    catch {
        "Error while installing the feature $serverFeature. Error : $_ " | Out-File -FilePath $scriptLogPath -Append
    }
}
else
{
    "The feature $serverFeature is already installed on the server." | Out-File -FilePath $scriptLogPath -Append
    If((Get-WindowsFeature -Name $serverFeature).InstallState -eq 'InstallPending')
    {
        "Restarting the computer as the installation state is InstallPending." | Out-File -FilePath $scriptLogPath -Append
        Restart-Computer -Force
    }
}
