
// Configure the AWS Provider
provider "aws" {
  region     = var.aws_region
  access_key = var.OG_AWS_ACCESS_KEY_ID
  secret_key = var.OG_AWS_SECRET_ACCESS_KEY
}