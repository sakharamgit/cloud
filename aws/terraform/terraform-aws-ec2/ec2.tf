locals {

  ebs_iops         = var.ebs_volume_type == "io1" ? var.ebs_iops : "0"
  ami              = var.ami != "" ? var.ami : join("", data.aws_ami.default.*.image_id)
  ami_owner        = var.ami != "" ? var.ami_owner : join("", data.aws_ami.default.*.owner_id)
  root_volume_type = var.root_volume_type != "" ? var.root_volume_type : data.aws_ami.info.root_device_type
}

module "security_group" {
  source = "../terraform-aws-security-group"


}

data "aws_caller_identity" "default" {
}

data "aws_region" "default" {
}

data "aws_partition" "default" {
}

data "aws_subnet" "default" {
  id = var.subnet
}

data "aws_iam_policy_document" "default" {
  statement {
    sid = ""

    actions = [
      "sts:AssumeRole",
    ]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }

    effect = "Allow"
  }
}
data "aws_ami" "default" {
  count       = var.ami == "" ? 1 : 0
  most_recent = "true"

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"]
}

data "aws_ami" "info" {
  filter {
    name   = "image-id"
    values = [local.ami]
  }

  owners = [local.ami_owner]
}

# https://github.com/hashicorp/terraform-guides/tree/master/infrastructure-as-code/terraform-0.13-examples/module-depends-on

resource "aws_instance" "default" {
  #bridgecrew:skip=BC_AWS_GENERAL_31: Skipping `Ensure Instance Metadata Service Version 1 is not enabled` check until BridgeCrew supports conditional evaluation. See https://github.com/bridgecrewio/checkov/issues/793
  #bridgecrew:skip=BC_AWS_NETWORKING_47: Skiping `Ensure AWS EC2 instance is configured with VPC` because it is incorrectly flagging that this instance does not belong to a VPC even though subnet_id is configured.
  ami                                  = local.ami
  instance_type                        = var.instance_type
  key_name                             = var.key_name
  ebs_optimized                        = var.ebs_optimized
  disable_api_termination              = var.disable_api_termination
  user_data                            = var.user_data
  user_data_base64                     = var.user_data_base64
  instance_initiated_shutdown_behavior = var.instance_initiated_shutdown_behavior
  associate_public_ip_address          = var.associate_public_ip_address
  monitoring                           = var.monitoring
  private_ip                           = var.private_ip
  source_dest_check                    = var.source_dest_check
  ipv6_address_count                   = var.ipv6_address_count < 0 ? null : var.ipv6_address_count
  ipv6_addresses                       = length(var.ipv6_addresses) == 0 ? null : var.ipv6_addresses


  root_block_device {
    volume_type           = local.root_volume_type
    volume_size           = var.root_volume_size
    delete_on_termination = var.delete_on_termination
    encrypted             = var.root_block_device_encrypted
  }

  metadata_options {
    http_endpoint               = var.metadata_http_endpoint_enabled ? "enabled" : "disabled"
    instance_metadata_tags      = var.metadata_tags_enabled ? "enabled" : "disabled"
    http_put_response_hop_limit = var.metadata_http_put_response_hop_limit
    http_tokens                 = var.metadata_http_tokens_required ? "required" : "optional"
  }

  credit_specification {
    cpu_credits = var.burstable_mode
  }


}

resource "aws_eip" "default" {
  instance = join("", aws_instance.default.*.id)
  vpc      = true
}

resource "aws_ebs_volume" "default" {

  availability_zone = var.availability_zone
  size              = var.ebs_volume_size
  iops              = local.ebs_iops
  type              = var.ebs_volume_type

  encrypted  = var.ebs_volume_encrypted
  kms_key_id = var.kms_key_id
}

resource "aws_volume_attachment" "default" {

  device_name = var.ebs_device_name
  volume_id   = aws_ebs_volume.default.id
  instance_id = join("", aws_instance.default.*.id)
}