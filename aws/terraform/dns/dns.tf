locals {
  dns               = yamldecode(file("${path.module}/dns.yaml"))
  domain_name       = local.dns.domain-name
  subject_alt_names = local.dns.subject-alternative-names
}

#TLS Certificate
resource "aws_acm_certificate" "this" {
  lifecycle {
    create_before_destroy = true
  }
  domain_name       = local.domain_name
  validation_method = "DNS"

  subject_alternative_names = local.subject_alt_names

  tags = {
    environment = local.environment
    terraform   = true
  }
}

# The zone to be used for the kubernetes cluster
resource "aws_route53_zone" "this" {
  name    = local.domain_name
  comment = format("Domain for the %s instances", local.env_name)

  tags = merge(
    {
      "environment" = local.environment
    },
    module.aws_global.tags,
  )
}

resource "aws_route53_record" "this" {
  zone_id = aws_route53_zone.this.zone_id
  name    = "*"
  type    = "CNAME"
  ttl     = "60"
  records = [local.domain_name]
}
