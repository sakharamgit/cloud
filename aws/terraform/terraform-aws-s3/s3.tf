# To create an S3 bucket 
resource "aws_s3_bucket" "s3_bucket" {
  bucket = var.s3_bucket_name

  tags = merge(var.resource_tags, var.deployment_tags)
}
