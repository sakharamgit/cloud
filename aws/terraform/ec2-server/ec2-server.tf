# vpcintegration server - Restore ONLY
# Below code is used to restore the vpcintegration server from the configured backups in case of a disaster
# server was migrated from Azure using CloudEndure, so we are restoring the ec2 from existing 
# AMI and ebs snapshots

locals {
  restore_info    = yamldecode(file("${path.module}/cit-etl-server.yaml"))
  owner           = local.restore_info.owner
  tag_purpose     = local.restore_info.tag_purpose
  instance_type   = local.restore_info.instance_type
  aws_key_name    = local.restore_info.aws_key_name
  ec2_restore     = try(local.restore_info.ec2_restore, false)
  ec2_az          = local.restore_info.ec2_az
  vpcintg_ami_sid = local.restore_info.vpcintg_ami_sid
  ebs_root_sid    = local.restore_info.ebs_root_sid
  ebs_extra_sid   = local.restore_info.ebs_extra_sid
  ebs_ftp_sid     = local.restore_info.ebs_ftp_sid
  ebs_temp_sid    = local.restore_info.ebs_temp_sid

  ec2_tags = merge(
    { "environment" = local.environment },
    module.aws_global.tags,
    local.vpc_config.tags,
  )
}

# To retrieve the VPC and Subnet
data "aws_vpc" "vpcintg_vpc" {
  filter {
    name   = "tag:Name"
    values = [module.custom_vpc.name]
  }
}

data "aws_subnet" "vpcintg_subnet" {
  availability_zone = local.ec2_az
  filter {
    name   = "tag:Name"
    values = [format("%s-%s-*", module.custom_vpc.name, local.subnet_name)]
  }
}

# To retrieve the lastest EBS snapshots
data "aws_ebs_snapshot" "ebs_root" {
  snapshot_ids = [local.ebs_root_sid]
}

data "aws_ebs_snapshot" "ebs_extra" {
  snapshot_ids = [local.ebs_extra_sid]
}

data "aws_ebs_snapshot" "ebs_ftp" {
  snapshot_ids = [local.ebs_ftp_sid]
}

data "aws_ebs_snapshot" "ebs_temp" {
  snapshot_ids = [local.ebs_temp_sid]
}

# Create a name for the vpcintegration ec2 instance
module "vpcintg_ec2_name" {
  source  = "app.terraform.io/cloudutsuk/name/aws"
  version = "12.0.0"

  environment = local.environment
  resource    = "vpcintg"
}

# To create a network interface
resource "aws_network_interface" "vpcintg_nic" {
  count           = local.ec2_restore ? 1 : 0
  subnet_id       = data.aws_subnet.vpcintg_subnet.id
  security_groups = [aws_security_group.vpcintegration-sg.id]
  tags            = local.ec2_tags
}

# To create an public ip for vpcintegration server
resource "aws_eip" "vpcintg_eip" {
  count             = local.ec2_restore ? 1 : 0
  vpc               = true
  network_interface = aws_network_interface.vpcintg_nic[count.index].id
  tags = {
    "Name" = "vpcIntegration-eip"
  }
}

# To create the vpcintegration server
resource "aws_instance" "vpcintg" {
  count         = local.ec2_restore ? 1 : 0
  ami           = local.vpcintg_ami_sid
  instance_type = local.instance_type
  key_name      = local.aws_key_name

  network_interface {
    network_interface_id = aws_network_interface.vpcintg_nic[count.index].id
    device_index         = 0
  }

  tags = merge(
    { "Name" = "vpcintegration" },
    local.ec2_tags
  )
}

# Output the vpcintegration server public ip
output "vpc_integration_server_ip" {
  value = aws_instance.vpcintg.*.public_ip
}
