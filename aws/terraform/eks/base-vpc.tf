###
### Locals for VPC settings
###

locals {
  vpc_info                                = yamldecode(file("${path.module}/vpc.yaml"))
  env_override                            = local.vpc_info.env_override
  account_type                            = local.vpc_info.account_type
  data_store_vpc                          = try(local.vpc_info.data_store_vpc, true)
  vpc_peer                                = try(local.vpc_info.vpc_peer, true)
  vpn_vpc_peer                            = try(local.vpc_info.vpn_vpc_peer, true)
  datastore_vpn_vpc_peer                  = try(local.vpc_info.datastore_vpn_vpc_peer, false)
  mongodb_atlas_vpc_peer_create           = try(local.vpc_info.mongodb_atlas_vpc_peer.create, false)
  mongodb_atlas_vpc_peer_peering_id       = try(local.vpc_info.mongodb_atlas_vpc_peer.peering_id, false)
  mongodb_atlas_vpc_peer_atlas_cidr_block = try(local.vpc_info.mongodb_atlas_vpc_peer.atlas_cidr_block, false)
}

module "eks_name" {
  source  = "app.terraform.io/cloudutsuk/name/aws"
  version = "0.2.0"

  providers = {
    random = random
  }

  environment = local.environment
  resource    = "eks"
}

module "kubernetes_vpc" {
  source  = "app.terraform.io/cloudutsuk/vpc/aws//modules/kubernetes"
  version = "1.0.3-tf13"

  providers = {
    aws      = aws
    random   = random
    template = template
  }

  environment             = local.environment
  kubernetes_cluster_name = module.eks_name.name
  starting_address        = module.aws_global.vpcs[format("%s-kubernetes-%s-%s", local.env_override != "" ? local.env_override : local.environment, local.account_type, local.region)]
}

module "data_store_vpc" {
  count   = local.data_store_vpc ? 1 : 0
  source  = "app.terraform.io/cloudutsuk/vpc/aws//modules/data-store"
  version = "1.0.3-tf13"

  providers = {
    aws      = aws
    random   = random
    template = template
  }

  environment      = local.environment
  starting_address = module.aws_global.vpcs[format("%s-data-store-%s-%s", local.env_override != "" ? local.env_override : local.environment, local.account_type, local.region)]
}

module "vpc_peer" {
  count      = local.vpc_peer ? 1 : 0
  depends_on = [module.data_store_vpc]
  source     = "app.terraform.io/cloudutsuk/vpc/aws//modules/peer-regional"
  version    = "1.0.3-tf13"

  providers = {
    random        = random
    aws.accepter  = aws
    aws.requester = aws
  }

  environment = local.environment

  requester_vpc_id              = module.kubernetes_vpc.vpc_id
  requester_vpc_name            = module.kubernetes_vpc.vpc_name
  requester_vpc_route_table_ids = module.kubernetes_vpc.kubernetes_route_table_ids
  requester_vpc_cidr_block      = module.kubernetes_vpc.vpc_cidr_block

  accepter_vpc_id              = module.data_store_vpc[count.index].vpc_id
  accepter_vpc_name            = module.data_store_vpc[count.index].vpc_name
  accepter_vpc_route_table_ids = module.data_store_vpc[count.index].private_route_table_ids
  accepter_vpc_cidr_block      = module.data_store_vpc[count.index].vpc_cidr_block
}

# VPC Peering with EngOps VPN VPC
module "vpn_vpc_peer" {
  count   = local.vpn_vpc_peer ? 1 : 0
  source  = "app.terraform.io/cloudutsuk/vpc/aws//modules/peer-requester"
  version = "1.0.0-tf13"

  providers = {
    random        = random
    aws.accepter  = aws
    aws.requester = aws
  }

  environment = local.environment
  region      = module.aws_global.account_region_default["engops"]

  requester_vpc_id              = module.kubernetes_vpc.vpc_id
  requester_vpc_cidr_block      = module.kubernetes_vpc.vpc_cidr_block
  requester_vpc_route_table_ids = module.kubernetes_vpc.kubernetes_route_table_ids
  requester_vpc_name            = module.kubernetes_vpc.vpc_name

  accepter_owner_id       = module.aws_global.account_id["engops"]
  accepter_vpc_id         = module.aws_global.vpc_ids["production-vpn-engops-us-west-2"]
  accepter_vpc_cidr_block = "${module.aws_global.vpn_vpcs["production-vpn-engops-us-west-2"]}/${module.aws_global.vpn_vpcs["vpn_vpc_cidr_block_size"]}"
  accepter_vpc_name       = "EngOps VPN VPC Peering"
}

# VPC Peering of Datastore VPC with EngOps VPN VPC
module "datastore_vpn_vpc_peer" {
  count   = local.datastore_vpn_vpc_peer ? 1 : 0
  source  = "app.terraform.io/cloudutsuk/vpc/aws//modules/peer-requester"
  version = "1.0.0-tf13"

  providers = {
    random        = random
    aws.accepter  = aws
    aws.requester = aws
  }

  environment = local.environment
  region      = module.aws_global.account_region_default["engops"]

  requester_vpc_id              = module.data_store_vpc[0].vpc_id
  requester_vpc_cidr_block      = module.data_store_vpc[0].vpc_cidr_block
  requester_vpc_route_table_ids = module.data_store_vpc[0].private_route_table_ids
  requester_vpc_name            = module.data_store_vpc[0].vpc_name

  accepter_owner_id       = module.aws_global.account_id["engops"]
  accepter_vpc_id         = module.aws_global.vpc_ids["production-vpn-engops-us-west-2"]
  accepter_vpc_cidr_block = "${module.aws_global.vpn_vpcs["production-vpn-engops-us-west-2"]}/${module.aws_global.vpn_vpcs["vpn_vpc_cidr_block_size"]}"
  accepter_vpc_name       = "EngOps VPN VPC Peering"
}

module "mongodb_atlas_vpc_peer" {
  count   = local.mongodb_atlas_vpc_peer_create ? 1 : 0
  source  = "app.terraform.io/cloudutsuk/vpc/aws//modules/peer-accepter"
  version = "1.0.3-tf13"

  providers = {
    aws.accepter = aws
  }

  environment               = local.environment
  vpc_peering_connection_id = local.mongodb_atlas_vpc_peer_peering_id

  requester_vpc_name       = "MongoDB Atlas ${local.environment} ${local.account_type}-${local.region}"
  requester_vpc_cidr_block = local.mongodb_atlas_vpc_peer_atlas_cidr_block

  accepter_vpc_name            = module.kubernetes_vpc.vpc_name
  accepter_vpc_route_table_ids = module.kubernetes_vpc.kubernetes_route_table_ids
}

