###
### Locals for EKS Cluster settings
###

locals {
  workers_info             = yamldecode(file("${path.module}/eks.yaml"))
  cluster_version          = local.workers_info.cluster_version
  map_roles                = try(local.workers_info.map_roles, [])
  map_users                = try(local.workers_info.map_users, [])
  redshift_audit_logging   = try(local.workers_info.redshift_audit_logging, false)
  root_volume_type         = try(local.workers_info.root_volume_type, "gp2")
  service                  = try(local.workers_info.service, false)
  sg_group                 = try(local.workers_info.sg_group, true)
  tags                     = { terraform = true }
  upgrade_ami              = try(local.workers_info.upgrade_ami, "")
  worker_subnets_single_az = try(local.workers_info.worker_subnets_single_az, false)
  # Single-AZ. We don't care which, but both workers groups need to be the same. This is done for EE
  worker_subnets   = local.worker_subnets_single_az ? [module.kubernetes_vpc.kubernetes_subnets[0]] : module.kubernetes_vpc.kubernetes_subnets
  workers          = local.workers_info.workers
  write_kubeconfig = try(local.workers_info.write_kubeconfig, false)

  enable_iam_roles_for_service_accounts = try(local.workers_info.enable_iam_roles_for_service_accounts, false)
}

## Import users created from TFC.
# This is not working as all the environments does not have this user created
# Either make this local variable conditional and then remove the users details from yaml file
# locals {
#   map_service_users = (local.iam_users == true ? [
#     for key, value in aws_iam_user.jenkins_spinnaker_deploy :
#     {
#       userarn  = value.arn
#       username = value.name
#       groups   = ["system:masters"]
#     }
#   ] : [])
# }

###
### Locals for EKS Workers
###

locals {
  base_worker_node_settings = {
    enable_monitoring     = local.environment == "production" ? true : false
    root_volume_size      = local.environment == "production" ? 512 : 256
    root_volume_type      = local.root_volume_type
    instance_type         = "t3.large"
    protect_from_scale_in = true
    enabled_metrics = [
      "GroupDesiredCapacity",
      "GroupInServiceInstances",
      "GroupMaxSize",
      "GroupMinSize",
      "GroupPendingInstances",
      "GroupStandbyInstances",
      "GroupTerminatingInstances",
      "GroupTotalInstances",
    ]
  }

  default_eks_roles = [
    {
      rolearn  = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/Okta-Read-Only-Access"
      username = "Okta-Read-Only-Access"
      groups   = ["system:masters"]
    },
    {
      rolearn  = local.tf_cloud_iam_role
      username = "Terraform-Cloud-Role"
      groups   = ["system:masters"]
    }
  ]
}

###
### Data
###

## For getting the account number
data "aws_caller_identity" "current" {}

# EKS AMIs are looked up by name, e.g.
#   aws_ami.eks_worker["og-amazon-eks-node-1.16-v20210427124939"]
data "aws_ami" "eks_worker" {
  for_each = toset([for config in local.workers : config.ami_name])

  most_recent = true
  owners      = ["self"]

  filter {
    name   = "name"
    values = [each.value]
  }
}

data "aws_eks_cluster" "cluster" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks.cluster_id
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.cluster.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
  token                  = data.aws_eks_cluster_auth.cluster.token
}

module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "15.1.0"

  cluster_name    = module.eks_name.name
  cluster_version = local.cluster_version
  enable_irsa     = local.enable_iam_roles_for_service_accounts
  manage_aws_auth = true
  map_roles       = concat(local.default_eks_roles, local.map_roles)
  map_users       = local.map_users

  vpc_id  = module.kubernetes_vpc.vpc_id
  subnets = concat(module.kubernetes_vpc.public_subnets, module.kubernetes_vpc.kubernetes_subnets)

  tags = merge(
    {
      "environment"        = local.environment
      "kubernetes-cluster" = module.eks_name.name
      # These three tags are needed for the cluster-autoscaler to work correctly.
      # Verified from AWS Documentation https://docs.aws.amazon.com/eks/latest/userguide/cluster-autoscaler.html
      "k8s.io/cluster-autoscaler/${module.eks_name.name}"                                = "owned"
      "k8s.io/cluster-autoscaler/enabled"                                                = "true"
      "k8s.io/cluster-autoscaler/node-template/label-autoscaling-to-zero-nodes-disabled" = ""
      "service"                                                                          = local.service
    },
    local.tags
  )

  workers_group_defaults = local.base_worker_node_settings
  worker_groups_launch_template = flatten(
    [for config in local.workers :
      {
        name          = config.name
        ami_id        = data.aws_ami.eks_worker[config.ami_name].image_id
        instance_type = try(config.spot, false) ? "" : config.type
        subnets       = local.worker_subnets

        root_encrypted = true

        asg_min_size = config.min
        asg_max_size = config.max
        # We don't want to specify a desired capacity explicitly since the ASG will drive it.
        asg_desired_capacity = null

        # Enable docker_in_docker when running Continuous Integration in K8s and building docker images by
        # either mounting the docker sock as a volume or using docker in docker.
        # Without the bridge enabled, internal routing from the inner container can't reach the outside world.
        bootstrap_extra_args = try(config.docker_in_docker, false) ? "--enable-docker-bridge true" : ""

        # Spot instance support.
        override_instance_types = try(config.spot, false) ? [config.type] : null
        spot_price              = try(config.spot, false) ? lookup(config, "spot_price", null) : null

        #   Nice to have: Auto-add the correct spot lifecycle args if the user forgot.
        kubelet_extra_args = join(" ", [for key, value in config.kubelet_extra_args :
          #   When https://github.com/terraform-aws-modules/terraform-aws-eks/pull/1046 is merged we need to remove the \"
          # NOTE! Due to a regression in EKS, double-double-quoted strings with single quoted strings are required.
          # DO NOT MODIFY WITHOUT CARE/CAUTION:
          "\"${key}='${join(",", value)}'\""
        ])
      }
    ]
  )

  write_kubeconfig = local.write_kubeconfig
}

resource "aws_config_aggregate_authorization" "config" {
  account_id = module.aws_global.account_id["engops"]
  region     = local.region
}

module "config" {
  source         = "app.terraform.io/cloudutsuk/security/aws//modules/config"
  version        = "1.0.0-tf13"
  aws_account_id = module.aws_global.account_id[local.account]

  providers = {
    aws = aws
  }
}

module "securityhub" {
  source  = "app.terraform.io/cloudutsuk/security/aws//modules/securityhub"
  version = "1.0.0-tf13"
}

module "inspector" {
  source  = "app.terraform.io/cloudutsuk/security/aws//modules/inspector"
  version = "1.0.0-tf13"

  name = "${module.aws_global.account_id[local.environment]}-${local.environment}"
}

# We need to update the default security group of the
# data store vpc to allow inbound connections from
# the kubernetes VPC
resource "aws_security_group_rule" "data_store_vpc_ingress" {
  count      = local.sg_group ? 1 : 0
  depends_on = [module.data_store_vpc]

  security_group_id = module.data_store_vpc[count.index].default_security_group_id

  type                     = "ingress"
  from_port                = 1025
  to_port                  = 65535
  protocol                 = "tcp"
  source_security_group_id = module.eks.worker_security_group_id
}

##Audits
# S3 bucket with appropriate permissions so that Redshift can send audit trail logs to it
module "redshift_audit_logging" {
  count   = local.redshift_audit_logging ? 1 : 0
  source  = "app.terraform.io/cloudutsuk/redshift-cloudutsuk/aws//modules/audit_logging"
  version = "0.7.0"

  providers = {
    aws = aws
  }

  environment = local.environment
}

###
### Outputs for EKS Cluster
###

output "region" {
  description = "The region of the VPCs"
  value       = local.region
}

#Kubernetes VPC
output "kubernetes_vpc_cidr" {
  description = "CIDR of Kubernetes VPC"
  value       = module.kubernetes_vpc.vpc_cidr_block
}

output "kubernetes_vpc_id" {
  description = "Kubernetes VPC ID"
  value       = module.kubernetes_vpc.vpc_id
}

output "requester_vpc_cidr_block" {
  description = "The VPC CIDR block"
  value       = module.kubernetes_vpc.vpc_cidr_block
}

output "requester_vpc_name" {
  description = "The VPC Name"
  value       = module.kubernetes_vpc.vpc_name
}

output "public_subnets" {
  description = "The VPC Public Subnets"
  value       = module.kubernetes_vpc.public_subnets
}

output "private_subnets" {
  description = "The VPC Private Subnets"
  value       = module.kubernetes_vpc.kubernetes_subnets
}
