# The module version for the OG name module needs to be change to "~> 12.0.0"
# The DLM policy to backup the EBS drive has to be created manually considering the terraform limitation.

# Locals for ec2
locals {
  sql_info               = yamldecode(file("${path.module}/sqlec2.yaml"))
  sql_ami_name           = local.sql_info.sql_ami_name
  owners                 = local.sql_info.owners
  private_ip_count       = local.sql_info.private_ip_count
  aws_key_pair           = local.sql_info.aws_key_pair
  sql_subnet_name        = local.sql_info.sql_subnet_name
  sql_ec2_instance_type  = local.sql_info.sql_ec2_instance_type
  sql_availability_zones = local.sql_info.sql_availability_zones
  nlb_ports              = local.sql_info.nlb_ports
  sql_ingress_ports      = local.sql_info.sql_ingress_ports
  is_prod_env            = local.sql_info.is_prod_env
  dlm_policy_state       = local.sql_info.dlm_policy_state
  ebs_optimized_flag     = local.sql_info.ebs_optimized_flag
  vpn_vpc_cidr_block     = "${module.aws_global.vpn_vpcs["production-vpn-engops-us-west-2"]}/${module.aws_global.vpn_vpcs["vpn_vpc_cidr_block_size"]}"

  role_policy_arns = [
    "arn:aws:iam::aws:policy/service-role/AmazonEC2RoleforSSM",
    "arn:aws:iam::aws:policy/CloudWatchAgentServerPolicy"
  ]
}

###############
# Data
###############

# To retrieve the latest AMI for Windows Server 2019
data "aws_ami" "windows" {
  most_recent = true

  filter {
    name   = "name"
    values = [local.sql_ami_name]
  }

  owners = [local.owners] # Canonical
}

#To get the vpc details for sql ec2 instances
data "aws_vpc" "sql_ec2_vpc" {
  filter {
    name   = "tag:Name"
    values = [module.data_store_vpc[0].vpc_name]
  }
}

# To get the subnet details for sql ec2 instances
data "aws_subnet" "sqlec2_subnets" {
  count             = length(local.sql_availability_zones)
  availability_zone = local.sql_availability_zones[count.index]

  filter {
    name   = "tag:Name"
    values = [format("%s-%s-*", module.data_store_vpc[0].vpc_name, local.sql_subnet_name)]
  }
}

# Create a name for the iam role for sql ec2 instance
module "ec2_iam_role_name" {
  source  = "app.terraform.io/cloudutsuk/name/aws"
  version = "12.0.0"

  environment = local.environment
  resource    = "ec2_iam_role"
}

# Create a name for the iam profile for sql ec2 instance
module "ec2_iam_profile_name" {
  source  = "app.terraform.io/cloudutsuk/name/aws"
  version = "12.0.0"

  environment = local.environment
  resource    = "ec2_iam_profile"
}

# Create a name for the security group for sql ec2 server
module "sql_sg_name" {
  source  = "app.terraform.io/cloudutsuk/name/aws"
  version = "12.0.0"

  environment = local.environment
  resource    = "sql-ec2"
}

# Create a name for the NLB
module "sql_nlb_name" {
  source  = "app.terraform.io/cloudutsuk/name/aws"
  version = "12.0.0"

  environment = local.environment
  resource    = "sql-nlb"
}

# Create a name for the NLB
module "sql_nlb_sec_name" {
  source  = "app.terraform.io/cloudutsuk/name/aws"
  version = "12.0.0"

  environment = local.environment
  resource    = "sql-nlb-sec"
}

# Create a name for the NLB
module "sql_nlb_odbc_name" {
  source  = "app.terraform.io/cloudutsuk/name/aws"
  version = "12.0.0"

  environment = local.environment
  resource    = "sql-odbc"
}

# Create a name for the NLB target group name
module "sql_tg_name" {
  source  = "app.terraform.io/cloudutsuk/name/aws"
  version = "12.0.0"

  environment = local.environment
  resource    = "sqltg"
}

# Create a name for the secondary NLB ta*rget group name
module "sql_tg_sec_name" {
  source  = "app.terraform.io/cloudutsuk/name/aws"
  version = "12.0.0"

  environment = local.environment
  resource    = "sqltgsec"
}

# Create a name for the ODBC NLB target group name
module "sql_tg_odbc_name" {
  source  = "app.terraform.io/cloudutsuk/name/aws"
  version = "12.0.0"

  environment = local.environment
  resource    = "sqltgodbc"
}

# Create a name of IAM role policy for CloudWatch Agent
module "iam_cw_policy_name" {
  source  = "app.terraform.io/cloudutsuk/name/aws"
  version = "12.0.0"

  environment = local.environment
  resource    = "iamcw"
}

# Create a name of IAM role policy for S3 bucket
module "iam_s3_policy_name" {
  source  = "app.terraform.io/cloudutsuk/name/aws"
  version = "12.0.0"

  environment = local.environment
  resource    = "iams3"
}

# Create a name of IAM role for S3 bucket
module "s3_iam_policy_name" {
  source  = "app.terraform.io/cloudutsuk/name/aws"
  version = "12.0.0"

  environment = local.environment
  resource    = "s3iamread"
}

# Create a name for the lifecycle manager IAM role
module "dlm_lifecycle_iam_role" {
  source  = "app.terraform.io/cloudutsuk/name/aws"
  version = "12.0.0"

  environment = local.environment
  resource    = "dlmiam"
}

# Create a name for the lifecycle manager policy
module "dlm_lifecycle_iam_policy" {
  source  = "app.terraform.io/cloudutsuk/name/aws"
  version = "12.0.0"

  environment = local.environment
  resource    = "dlmiampolicy"
}

# Create a name for the DLM lifecycle manager policy
module "dlm_lifecycle_policy" {
  source  = "app.terraform.io/cloudutsuk/name/aws"
  version = "12.0.0"

  environment = local.environment
  resource    = "dlmpolicy"
}

# Create a name for the Tag management policy
module "tag_mgmt_policy_name" {
  source  = "app.terraform.io/cloudutsuk/name/aws"
  version = "12.0.0"

  environment = local.environment
  resource    = "tagmgmt"
}


# To create AWS IAM Role for sql ec2 instances
resource "aws_iam_role" "sql_ec2_iam_role" {
  name = "sql_ec2_dbcopy_iam_role"
  path = "/"

  assume_role_policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Action" : "sts:AssumeRole",
        "Principal" : {
          "Service" : "ec2.amazonaws.com"
        },
        "Effect" : "Allow",
        "Sid" : ""
      }
    ]
  })
}

# To create a role policy for CloudWatch SSM agent
resource "aws_iam_role_policy" "cw-role-policy" {
  name = module.iam_cw_policy_name.name
  role = aws_iam_role.sql_ec2_iam_role.id

  policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Effect" : "Allow",
          "Action" : [
            "ssm:GetParameter"
          ],
          "Resource" : "*"
        }
      ]
    }
  )
}

# To create a role policy for Tag management
resource "aws_iam_role_policy" "tag-mgmt-policy" {
  name = module.tag_mgmt_policy_name.name
  role = aws_iam_role.sql_ec2_iam_role.id

  policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Sid" : "tagsmanagementrole",
          "Effect" : "Allow",
          "Action" : [
            "ec2:DeleteTags",
            "ec2:CreateTags"
          ],
          "Resource" : "*"
        },
        {
          "Sid" : "tagsmanagementroleforec2",
          "Effect" : "Allow",
          "Action" : [
            "ec2:DescribeInstances",
            "ec2:DescribeTags"
          ],
          "Resource" : "*"
        }
      ]
    }
  )
}

resource "aws_iam_role_policy_attachment" "cw-policy-attachment" {
  count = length(local.role_policy_arns)

  role       = aws_iam_role.sql_ec2_iam_role.name
  policy_arn = element(local.role_policy_arns, count.index)
}

# To create an AWS IAM Instance Profile for SQL EC2
resource "aws_iam_instance_profile" "sql_ec2_iam_inst_profile" {
  name = module.ec2_iam_profile_name.name
  role = aws_iam_role.sql_ec2_iam_role.name
}

# To create a security group for SQL Ec2 instances
resource "aws_security_group" "sql-ec2-sg" {
  name        = module.sql_sg_name.name
  description = "AWS Security Group for EC2"
  vpc_id      = data.aws_vpc.sql_ec2_vpc.id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [data.aws_vpc.sql_ec2_vpc.cidr_block]
  }

  tags = merge(
    {
      "environment" = local.environment
      "Name"        = module.sql_sg_name.name
    },
    module.aws_global.tags,
    local.vpc_config.tags,
  )
}

resource "aws_security_group_rule" "sql-ingress" {
  for_each = local.sql_ingress_ports

  type              = "ingress"
  from_port         = lookup(each.value, "from_port")
  to_port           = lookup(each.value, "to_port")
  protocol          = lookup(each.value, "protocol")
  cidr_blocks       = [module.kubernetes_vpc.vpc_cidr_block, local.vpn_vpc_cidr_block, data.aws_vpc.sql_ec2_vpc.cidr_block, data.aws_subnet.this.cidr_block]
  security_group_id = aws_security_group.sql-ec2-sg.id
}

# To create a network interface for EC2 in AZ1
resource "aws_network_interface" "nic" {
  count             = length(data.aws_subnet.sqlec2_subnets)
  subnet_id         = data.aws_subnet.sqlec2_subnets[count.index].id
  security_groups   = [aws_security_group.sql-ec2-sg.id]
  private_ips_count = local.private_ip_count
}

# To create Amazon EC2 Windows instance in AZ1
resource "aws_instance" "sqlec2" {
  count                = length(aws_network_interface.nic)
  ami                  = data.aws_ami.windows.id
  instance_type        = local.sql_ec2_instance_type
  key_name             = local.aws_key_pair
  ebs_optimized        = local.ebs_optimized_flag
  iam_instance_profile = aws_iam_instance_profile.sql_ec2_iam_inst_profile.name

  network_interface {
    network_interface_id = aws_network_interface.nic[count.index].id
    device_index         = 0
  }

  tags = merge(
    {
      "environment" = local.environment
      "Name"        = "cit-sql-ec2"
      "type"        = "sqlec2"
    },
    module.aws_global.tags,
    local.vpc_config.tags,
  )
  lifecycle {
    ignore_changes = [
      ami
    ]
  }
}

# To create a network load balancer
resource "aws_lb" "sql-nlb" {
  name               = module.sql_nlb_name.name
  internal           = true
  load_balancer_type = "network"
  subnets            = data.aws_subnet.sqlec2_subnets.*.id

  enable_deletion_protection       = false
  enable_cross_zone_load_balancing = true

  tags = merge(
    {
      "environment" = local.environment
      "purpose"     = "sql-internal-lb"
    },
    module.aws_global.tags,
    local.vpc_config.tags,
  )
}

# To create the target group and attach it to the NLB
resource "aws_lb_target_group" "sqlnlbtargetgroup" {
  name        = module.sql_tg_name.name
  port        = local.nlb_ports
  protocol    = "TCP"
  target_type = "ip"
  vpc_id      = data.aws_vpc.sql_ec2_vpc.id
}

# To add the targets to the target group
resource "aws_lb_target_group_attachment" "sqltga" {
  count            = length(aws_network_interface.nic)
  target_group_arn = aws_lb_target_group.sqlnlbtargetgroup.arn
  port             = local.nlb_ports
  target_id        = element(split(",", join(",", aws_network_interface.nic[count.index].private_ips)), 3)
}

# To create a NLB listener
resource "aws_lb_listener" "sqllblist" {
  load_balancer_arn = aws_lb.sql-nlb.arn

  port     = local.nlb_ports
  protocol = "TCP"

  default_action {
    target_group_arn = aws_lb_target_group.sqlnlbtargetgroup.arn
    type             = "forward"
  }
}

# To create a secondary network load balancer
resource "aws_lb" "sql-sec-nlb" {
  name               = module.sql_nlb_sec_name.name
  internal           = true
  load_balancer_type = "network"
  subnets            = data.aws_subnet.sqlec2_subnets.*.id

  enable_deletion_protection       = false
  enable_cross_zone_load_balancing = true

  tags = merge(
    {
      "environment" = local.environment
      "purpose"     = "sql-internal-sec-lb"
    },
    module.aws_global.tags,
    local.vpc_config.tags,
  )
}

# To create the target group and attach it to the secondary NLB
resource "aws_lb_target_group" "sqlsecnlbtargetgroup" {
  name        = module.sql_tg_sec_name.name
  port        = local.nlb_ports
  protocol    = "TCP"
  target_type = "ip"
  vpc_id      = data.aws_vpc.sql_ec2_vpc.id
}

# To add the targets to the target group
resource "aws_lb_target_group_attachment" "sqltgasec" {
  count            = length(aws_network_interface.nic)
  target_group_arn = aws_lb_target_group.sqlsecnlbtargetgroup.arn
  port             = local.nlb_ports
  target_id        = element(split(",", join(",", aws_network_interface.nic[count.index].private_ips)), 4)
}

# To create a secondary NLB listener
resource "aws_lb_listener" "sqlseclblist" {
  load_balancer_arn = aws_lb.sql-sec-nlb.arn

  port     = local.nlb_ports
  protocol = "TCP"

  default_action {
    target_group_arn = aws_lb_target_group.sqlsecnlbtargetgroup.arn
    type             = "forward"
  }
}

# To create a ODBC network load balancer
resource "aws_lb" "sql-odbc-nlb" {
  name               = module.sql_nlb_odbc_name.name
  internal           = false
  load_balancer_type = "network"
  subnets            = data.aws_subnet.sqlec2_subnets.*.id

  enable_deletion_protection       = false
  enable_cross_zone_load_balancing = true

  tags = merge(
    {
      "environment" = local.environment
      "purpose"     = "sql-internal-odbc-lb"
    },
    module.aws_global.tags,
    local.vpc_config.tags,
  )
}

# To create the target group and attach it to the ODBC NLB
resource "aws_lb_target_group" "sqlodbcnlbtargetgroup" {
  name        = module.sql_tg_odbc_name.name
  port        = local.nlb_ports
  protocol    = "TCP"
  target_type = "ip"
  vpc_id      = data.aws_vpc.sql_ec2_vpc.id
}

# To add the targets to the target group
resource "aws_lb_target_group_attachment" "sqltgaodbc" {
  count            = length(aws_network_interface.nic)
  target_group_arn = aws_lb_target_group.sqlodbcnlbtargetgroup.arn
  port             = local.nlb_ports
  target_id        = aws_network_interface.nic[0].private_ip
}

# To create a ODBC NLB listener
resource "aws_lb_listener" "sqlodbclblist" {
  load_balancer_arn = aws_lb.sql-odbc-nlb.arn

  port     = local.nlb_ports
  protocol = "TCP"

  default_action {
    target_group_arn = aws_lb_target_group.sqlodbcnlbtargetgroup.arn
    type             = "forward"
  }
}

# To create an IAM role for DLM Lifecycle Manager
resource "aws_iam_role" "dlm_lifecycle_role" {
  name = module.dlm_lifecycle_iam_role.name

  assume_role_policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Action" : "sts:AssumeRole",
          "Principal" : {
            "Service" : "dlm.amazonaws.com"
          },
          "Effect" : "Allow",
          "Sid" : ""
        }
      ]
  })
}

# To create a policy for DLM
resource "aws_iam_role_policy" "dlm_lifecycle" {
  name = module.dlm_lifecycle_iam_policy.name
  role = aws_iam_role.dlm_lifecycle_role.id

  policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Effect" : "Allow",
          "Action" : [
            "ec2:CreateSnapshot",
            "ec2:CreateSnapshots",
            "ec2:DeleteSnapshot",
            "ec2:DescribeInstances",
            "ec2:DescribeVolumes",
            "ec2:DescribeSnapshots"
          ],
          "Resource" : "*"
        },
        {
          "Effect" : "Allow",
          "Action" : [
            "ec2:CreateTags"
          ],
          "Resource" : "arn:aws:ec2:*::snapshot/*"
        }
      ]
  })
}

# To create a DLM policy
resource "aws_dlm_lifecycle_policy" "dlm_policy_for_ebs" {
  description        = "EBS Backup DLM lifecycle policy"
  execution_role_arn = aws_iam_role.dlm_lifecycle_role.arn
  state              = local.dlm_policy_state

  policy_details {
    resource_types = ["VOLUME"]

    schedule {
      name = "Hourly Snapshots for SQL Backup ebs volume"

      create_rule {
        interval      = 1
        interval_unit = "HOURS"
        times         = ["00:00"]
      }

      retain_rule {
        count = 840
      }

      tags_to_add = {
        snapshot = "True"
      }

      copy_tags = false
    }

    target_tags = {
      snapshot = "true"
    }
  }
}



# Create a name of S3 bucket
module "s3_iam_read_name" {
  source  = "app.terraform.io/cloudutsuk/name/aws"
  version = "12.0.0"

  environment = local.environment
  resource    = "s3iamread"
}

# Create a name of S3 bucket
module "s3_iam_write_name" {
  source  = "app.terraform.io/cloudutsuk/name/aws"
  version = "12.0.0"

  environment = local.environment
  resource    = "s3iamwrite"
}