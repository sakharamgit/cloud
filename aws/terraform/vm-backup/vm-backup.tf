locals {
  bckp_info = yamldecode(file("${path.module}/vm-backup.yaml"))
  tag_Name  = local.bckp_info.tag_Name
  resource  = local.bckp_info.resource
  bckp_tags = merge(
    { "environment" = local.environment },
    module.aws_global.tags,
    local.vpc_config.tags,
  )
}

# Create a name for the backup vault
module "bckp_vault_name" {
  source  = "app.terraform.io/cloudutsuk/name/aws"
  version = "1.0.1"

  environment = local.environment
  resource    = local.resource
}

# Create a name for the backup plan
module "bckp_plan_name" {
  source  = "app.terraform.io/cloudutsuk/name/aws"
  version = "1.0.1"

  environment = local.environment
  resource    = "intg-bkcp-plan"
}

# Create a name for the backup plan rules
module "bckp_rule1_name" {
  source  = "app.terraform.io/cloudutsuk/name/aws"
  version = "1.0.1"

  environment = local.environment
  resource    = "rule-daily2AMUTC"
}

module "bckp_rule2_name" {
  source  = "app.terraform.io/cloudutsuk/name/aws"
  version = "1.0.1"

  environment = local.environment
  resource    = "rule-daily2PMUTC"
}

# Create a name for the IAM role
module "intg_iam_role_name" {
  source  = "app.terraform.io/cloudutsuk/name/aws"
  version = "1.0.1"

  environment = local.environment
  resource    = "intg_bckup_iam"
}

# Create a name for the AWS Backup selection
module "bckp_selection_name" {
  source  = "app.terraform.io/cloudutsuk/name/aws"
  version = "1.0.1"

  environment = local.environment
  resource    = "intg_server"
}


# To create an IAM role for AWS Backup
resource "aws_iam_role" "backup_iam_role" {
  name               = module.intg_iam_role_name.name
  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": ["sts:AssumeRole"],
      "Effect": "allow",
      "Principal": {
        "Service": ["backup.amazonaws.com"]
      }
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "example" {
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSBackupServiceRolePolicyForBackup"
  role       = aws_iam_role.backup_iam_role.name
}

# To create the backup vault
resource "aws_backup_vault" "intg_backup_vault" {
  name = module.bckp_vault_name.name
  #kms_key_arn = aws_kms_key.example.arn

  tags = merge(local.bckp_tags)

  lifecycle {
    ignore_changes = [recovery_points]
  }
}

# To create the backup plan
resource "aws_backup_plan" "intg_backup_plan" {
  name = module.bckp_plan_name.name

  rule {
    rule_name         = module.bckp_rule1_name.name
    target_vault_name = aws_backup_vault.intg_backup_vault.name
    schedule          = "cron(0 2 * * ? *)"
  }

  rule {
    rule_name         = module.bckp_rule2_name.name
    target_vault_name = aws_backup_vault.intg_backup_vault.name
    schedule          = "cron(0 14 * * ? *)"
  }

  advanced_backup_setting {
    backup_options = {
      WindowsVSS = "disabled"
    }
    resource_type = "EC2"
  }

  tags = merge(local.bckp_tags)
}

# To assign the backup plan to the ec2 instance
resource "aws_backup_selection" "intg_server" {
  iam_role_arn = aws_iam_role.backup_iam_role.arn
  name         = module.bckp_selection_name.name
  plan_id      = aws_backup_plan.intg_backup_plan.id

  selection_tag {
    type  = "STRINGEQUALS"
    key   = "Name"
    value = local.tag_Name
  }
}