## Azure App Registrations

### Setup Environment Variables

- To set up environment variables for Azure, you can use the following commands:

```bash
export ARM_TENANT_ID=<replace_this>
export ARM_CLIENT_ID=<replace_this>
export ARM_CLIENT_SECRET=<replace_this>
export ARM_SUBSCRIPTION_ID=<replace_this>
```

