# Import the list function from the list module
from .autocomplete import autocomplete_fuction

def call_openaiservice_function(tenant_id, client_id, client_secret):
    authority = f"https://login.microsoftonline.com/{tenant_id}"
    scope = ["https://graph.microsoft.com/.default"]
    
    try:
        # Call the list function with arguments
        autocomplete_function(tenant_id, client_id, client_secret, authority, scope)
    except Exception as e:
        print(f"An error occurred: {e}")
