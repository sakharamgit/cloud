import openai
import os

# Set environment variables
os.environ['OPENAI_API_TYPE'] = 'azure'
os.environ['OPENAI_API_BASE'] = 'https://<your-resource-name>.openai.azure.com/'
os.environ['OPENAI_API_KEY'] = '<your-azure-api-key>'

# Configure OpenAI client
openai.api_type = os.getenv('OPENAI_API_TYPE')
openai.api_base = os.getenv('OPENAI_API_BASE')
openai.api_key = os.getenv('OPENAI_API_KEY')

# Example API call to create a chat completion
def autocomplete_function():
    response = openai.ChatCompletion.create(
        model="<your-deployment-id>",  # Use your deployment ID here
        messages=[
            {"role": "system", "content": "You are a helpful assistant."},
            {"role": "user", "content": "Tell me a joke."}
        ]
    )

    # Print the response
    print(response['choices'][0]['message']['content'])
