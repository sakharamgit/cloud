# python/entraid/entraid.py
from aiml.openaiservice import openaiservice

def call_aiml_function(function_name, tenant_id, client_id, client_secret):
    if function_name == 'openaiservice':
        call_openaiservice_function(tenant_id, client_id, client_secret)
    else:
        print(f"Function '{function_name}' not found in appregistrations")
