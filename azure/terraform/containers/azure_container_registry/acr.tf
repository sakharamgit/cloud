

# To create a resource group
resource "azurerm_resource_group" "example" {
  name     = "${var.prefix}-${var.project}-${var.env}-rg"
  location = var.az_location
}

# To create a user assigned identity
resource "azurerm_user_assigned_identity" "example" {
  resource_group_name = azurerm_resource_group.example.name
  location            = azurerm_resource_group.example.location

  name = var.acr_pull_username
}

# To assign a role to the user
resource "azurerm_role_assignment" "example" {
  scope                = azurerm_resource_group.example.id
  role_definition_name = "AcrPull"
  principal_id         = azurerm_user_assigned_identity.example.principal_id

  depends_on = [azurerm_user_assigned_identity.example]
}

resource "azurerm_container_registry" "acr" {
  name                = "${var.prefix}${var.project}${var.env}acr"
  resource_group_name = azurerm_resource_group.example.name
  location            = azurerm_resource_group.example.location
  sku                 = "Premium"
  admin_enabled       = true
  #georeplication_locations = ["East US", "East US 2"]
}

resource "null_resource" "acr-push" {
  provisioner "local-exec" {
    command = <<EOT
        az login --service-principal -u $env:ARM_CLIENT_ID -p $env:ARM_CLIENT_SECRET --tenant $env:ARM_TENANT_ID
        az acr login --name ${azurerm_container_registry.acr.name}
        docker tag sakharamshinde/azgowebapp azbirdpediareg.azurecr.io/azgowebapp
        docker push azbirdpediareg.azurecr.io/azgowebapp        
    EOT
  }
  depends_on = [azurerm_container_registry.acr]
}