
module "virtual_network" {
  source = "../../../../terraform/modules/azure/terraform-azurerm-virtual-network"

  for_each = local.config.create_virtual_network ? { for vnet in local.config.virtual_networks : vnet.name => vnet } : {}

  virtual_network_name = each.value.name
  resource_group_name  = each.value.resource_group
  vnet_location        = each.value.location
  vnet_address_space   = each.value.address_space

  resource_tags   = local.config.resource_tags
  deployment_tags = local.config.deployment_tags
}

module "subnets" {
  source = "../../../../terraform/modules/azure/terraform-azurerm-subnet"

  for_each = local.config.create_virtual_network ? { for subnet in local.config.subnets : subnet.name => subnet } : {}

  subnet_name             = each.value.name
  resource_group_name     = each.value.resource_group
  virtual_network_name    = each.value.virtual_network_name
  subnet_address_prefixes = each.value.address_prefixes
  it_depends_on           = var.create_virtual_network ? [module.virtual_network] : []

  resource_tags   = local.config.resource_tags
  deployment_tags = local.config.deployment_tags
}