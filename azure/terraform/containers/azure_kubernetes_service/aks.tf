
module "aks_cluster" {
  source = "../../../../terraform/modules/azure/terraform-azurerm-aks"

  for_each = { for aks in local.config.aks_clusters : aks.name => aks }

  aks_name                = each.value.name
  aks_location            = each.value.location
  aks_resource_group_name = each.value.resource_group
  aks_dns_prefix          = lower(each.value.dns_prefix)
  aks_kubernetes_version  = each.value.kubernetes_version

  aks_default_node_pool                = each.value.default_node_pool
  aks_default_node_pool_vnet_subnet_id = var.create_virtual_network ? module.subnets[each.value.default_node_pool.subnet_name].subnet.id : null

  aks_network_profile = lookup(each.value, "network_profile", null)

  # resource_group_id                        = azurerm_resource_group.rg.id
  # private_cluster_enabled                  = true
  # automatic_channel_upgrade                = var.automatic_channel_upgrade
  # sku_tier                                 = var.sku_tier

  # dynamic "identity" {
  #   for_each = var.identity != null ? [var.identity] : []
  #   content {
  #     type         = each.value.identity.value.type
  #     identity_ids = lookup(each.value.identity.value, "identity_ids", null)
  #   }
  # }

  # dynamic "service_principal" {
  #   for_each = var.service_principal != null ? [var.service_principal] : []
  #   content {
  #     client_id     = each.value.service_principal.value.client_id
  #     client_secret = each.value.service_principal.value.client_secret
  #   }
  # }

  resource_tags   = local.config.resource_tags
  deployment_tags = local.config.deployment_tags

  timeout_duration = local.config.aks_timeout_duration
  it_depends_on = var.create_virtual_network ? [module.azurerm_resource_group, module.virtual_network, module.subnets] : [module.azurerm_resource_group, null, null]
}


module aks_node_pool {
  source = "../../../../terraform/modules/azure/terraform-azurerm-aks-node-pool"

  for_each = { for aks_node_pool in local.config.aks_clusters : aks_node_pool.name => aks_node_pool }

  kubernetes_cluster_id = each.value.aks_name
  name                  = var.application_node_pool.name
  vm_size               = var.application_node_pool.vm_size
  availability_zones    = var.application_node_pool.availability_zones
  enable_auto_scaling   = var.application_node_pool_scaling.enable_auto_scaling
  enable_node_public_ip = var.application_node_pool.enable_node_public_ip
  max_pods              = var.application_node_pool.max_pods
  node_labels           = var.application_node_pool.node_labels
  node_taints           = var.application_node_pool.node_taints
  os_disk_size_gb       = var.application_node_pool.os_disk_size_gb
  os_disk_type          = var.application_node_pool.os_disk_type
  vnet_subnet_id        = var.application_node_pool_subnet_id
  node_count            = var.application_node_pool.node_count
  orchestrator_version  = var.application_node_pool.orchestrator_version
  tags                  = var.application_node_pool.tags
}

# network_dns_service_ip                   = var.network_dns_service_ip
# network_plugin                           = var.network_plugin
# outbound_type                            = "userDefinedRouting"
# network_service_cidr                     = var.network_service_cidr
# log_analytics_workspace_id               = module.log_analytics_workspace.id
# role_based_access_control_enabled        = var.role_based_access_control_enabled
# tenant_id                                = data.azurerm_client_config.current.tenant_id
# admin_group_object_ids                   = var.admin_group_object_ids
# azure_rbac_enabled                       = var.azure_rbac_enabled
# admin_username                           = var.admin_username
# ssh_public_key                           = var.ssh_public_key
# keda_enabled                             = var.keda_enabled
# vertical_pod_autoscaler_enabled          = var.vertical_pod_autoscaler_enabled
# workload_identity_enabled                = var.workload_identity_enabled
# oidc_issuer_enabled                      = var.oidc_issuer_enabled
# open_service_mesh_enabled                = var.open_service_mesh_enabled
# image_cleaner_enabled                    = var.image_cleaner_enabled
# azure_policy_enabled                     = var.azure_policy_enabled
# http_application_routing_enabled         = var.http_application_routing_enabled

# depends_on = [module.routetable]


