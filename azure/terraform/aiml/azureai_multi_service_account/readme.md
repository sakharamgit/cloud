# Azure AI Multi Service Account with Terraform

- This repository contains Terraform code to deploy an Azure AI Multi Service Account. 

## Prerequisites

Before you begin, ensure you have the following:

1. [Terraform](https://www.terraform.io/downloads.html) installed on your local machine.
2. [Azure CLI](https://docs.microsoft.com/en-us/cli/azure/install-azure-cli) installed and authenticated.
3. An Azure subscription. If you don't have one, you can create a [free account](https://azure.microsoft.com/free/).

## Getting Started

### Clone the Repository

```sh
git clone https://gitlab.com/sakharamgit/cloud.git
cd azure/terraform/aiml/azureai_multi_service_account
```

### Configure Terraform Backend (Optional)
If you want to configure a remote backend for Terraform state, update the backend.tf file with your backend configuration.

### Initialize Terraform
- Initialize the Terraform configuration:

```sh
terraform init
```

### Review and Apply the Configuration
- Review the Terraform plan to see what resources will be created:

```sh
terraform plan
```

- Apply the Terraform plan to create the resources:

```sh
terraform apply
```
