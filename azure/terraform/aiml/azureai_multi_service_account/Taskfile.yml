# Computer Vision REST API
# https://learn.microsoft.com/en-us/rest/api/computer-vision/?view=rest-computervision-v3.2

version: '3'

includes:
  azure: ../../../../taskfile/azure
  terraform: ../../../../taskfile/terraform
  computervision: ./azureaiservices/computervision
  translator: ./azureaiservices/translator

vars:
  SUBSCRIPTION_ID: "{{.ARM_SUBSCRIPTION_ID}}"
  TENANT_ID: "{{.ARM_TENANT_ID}}"
  AZURE_REGION: "{{.ARM_LOCATION}}"
  FILE_PATH: Null
  FROM_LANG: '{{.FROM_LANG | default "en"}}'
  TO_LANG: '{{.TO_LANG | default "es"}}'
  TEXT: '{{.TEXT | default "Hello, what is your name?"}}'

tasks:
  one-click-deploy:
    desc: One Click Deployment
    cmds:
      - task azure:login
      # - task azure:setup-terraform-backend ARM_LOCATION="{{.ARM_LOCATION}}"
      - task terraform:one-click-deploy

  one-click-destroy:
    desc: One Click Destroy
    cmds:
      - task azure:login ARM_SUBSCRIPTION_ID="{{.ARM_SUBSCRIPTION_ID}}" ARM_TENANT_ID="{{.ARM_TENANT_ID}}"
      - task terraform:destroy
    # - task azure:destroy-terraform-backend
      - task azure:logout

  get-ai-cognitive-services:
    desc: Get the endpoint of the AI service
    cmds:
      - |
        # List all Cognitive Services accounts
        resources=$(az resource list --resource-type Microsoft.CognitiveServices/accounts --query "[].{name:name, resourceGroup:resourceGroup}" -o tsv)

        echo "$resources" | while read -r resource; do 
          name=$(echo $resource | awk '{print $1}')
          resourceGroup=$(echo $resource | awk '{print $2}')
          echo "Service Name: $name"
          echo "Resource Group: $resourceGroup"

          # Get the endpoint
          endpoint=$(az cognitiveservices account show --name $name --resource-group $resourceGroup --query "properties.endpoint" -o tsv)
          #echo "Endpoint: $endpoint"

          # Get the keys
          key=$(az cognitiveservices account keys list --name $name --resource-group $resourceGroup --query "{key1:key1}" -o tsv)
          #echo "Key: $key"

          echo "-------------------------"
          echo "ENDPOINT=$endpoint" > .env
          echo "KEY=$key" >> .env
        done
    
  analyze-image-from-url:
    desc: Analyze an url image using the AI service
    cmds:
    - task computervision:analyze-image-from-url
 
  analyze-image-from-local:
    desc: Analyze an local image using the AI service
    cmds:
    - task computervision:analyze-image-from-local FILE_PATH="../images/city-on-beach.jpg"

  analyze-image-for-brands:
    desc: Analyze an image using the AI service
    cmds:
    - task computervision:analyze-image-for-brands FILE_PATH="../images/brands.jpeg"

  describe-image-from-local:
    desc: Analyze an image using the AI service
    cmds:
    - task computervision:describe-image-from-local FILE_PATH="../images/city-on-beach.jpg"

  detect-objects-from-image:
    desc: Analyze an image using the AI service
    cmds:
    - task computervision:detect-objects-from-image FILE_PATH="../images/bike.jpg"

  add-captions-to-image:
    desc: Add Captions to an Image
    cmds:
    - task computervision:add-captions-to-image FILE_PATH="../images/bike.jpg"

  add-tags-to-image:
    desc: Add Tags to an Image
    cmds:
    - task computervision:add-tags-to-image FILE_PATH="../images/bike.jpg"

  analyze-for-adult-content:
    desc: Analyze image for adult content
    cmds:
      - task computervision:analyze-for-adult-content FILE_PATH="../images/bike.jpg"

  read-text-from-image:
    desc: Read the text from image
    cmds:
      - task computervision:read-text-from-image FILE_PATH="../images/bike.jpg"
  
  read-text-from-image-in-japanese:
    desc: Read the text from image
    cmds:
      - task computervision:read-text-from-image-in-japanese FILE_PATH="../images/bike.jpg"
  
  translate-text:
    desc: Translate Text
    cmds:
      - task translator:translate-text FROM_LANG="end" TO_LANG="ja" TEXT="Hello, How are you?"