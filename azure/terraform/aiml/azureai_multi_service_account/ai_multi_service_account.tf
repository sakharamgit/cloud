

# To create a resource group
resource "azurerm_resource_group" "example" {
  name     = "${var.prefix}-${var.project}-${var.env}-rg"
  location = var.az_location
}

resource "random_string" "random" {
  length  = 2
  special = false
}

resource "azurerm_cognitive_account" "example" {
  name                = "${var.prefix}-${var.project}-${var.env}-multi-svcaccount-${random_string.random.result}"
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name
  kind                = "CognitiveServices"

  sku_name = "S0"

  tags = {
    Enviroment = "Dev"
  }
}


