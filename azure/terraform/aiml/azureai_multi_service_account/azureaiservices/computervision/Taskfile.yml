version: '3'

vars:
  IMAGE_FILE_PATH: '{{.FILE_PATH | default "none"}}'

tasks:
  analyze-image-from-url:
    desc: Analyze an image using the AI service
    cmds:
    - | 
      source .env
      url="${ENDPOINT}vision/v3.1/analyze?visualFeatures=Categories,Description,Color"
      curl -sX POST $url \
      -H "Ocp-Apim-Subscription-Key: $KEY" \
      -H "Content-Type: application/json" \
      -d '{
        "url": "https://4.img-dpreview.com/files/p/TS1200x900~sample_galleries/1330372094/0455063184.jpg"
      }' | jq .

  analyze-image-from-local:
    desc: Analyze an image using the AI service
    cmds:
    - | 
      source .env
      if [[ "{{.IMAGE_FILE_PATH}}" == "none" ]]; then
        echo "ERROR: Please provide the file path"
        exit 1
      fi
      image_path="{{.IMAGE_FILE_PATH}}"
      url="${ENDPOINT}vision/v3.1/analyze?visualFeatures=Categories,Description,Color,Faces"
      curl -sX POST $url \
      -H "Ocp-Apim-Subscription-Key: $KEY" \
      -H "Content-Type: application/octet-stream" \
      --data-binary "@${image_path}" | jq .
    
    # Add different visual features to get more data
    # Possible values: Adult, Brands, Categories, Color, Description, Faces, ImageType, Objects, Tags

  analyze-image-for-brands:
    desc: Analyze an image using the AI service
    cmds:
    - | 
      source .env
      if [[ "{{.IMAGE_FILE_PATH}}" == "none" ]]; then
        echo "ERROR: Please provide the file path"
        exit 1
      fi
      image_path="{{.IMAGE_FILE_PATH}}"
      url="${ENDPOINT}vision/v3.1/analyze?visualFeatures=Categories,Description,Color,Brands"
      curl -sX POST $url \
      -H "Ocp-Apim-Subscription-Key: $KEY" \
      -H "Content-Type: application/octet-stream" \
      --data-binary "@${image_path}" | jq .

  describe-image-from-local:
    desc: Analyze an image using the AI service
    cmds:
    - | 
      source .env
      if [[ "{{.IMAGE_FILE_PATH}}" == "none" ]]; then
        echo "ERROR: Please provide the file path"
        exit 1
      fi
      image_path="{{.IMAGE_FILE_PATH}}"
      url="${ENDPOINT}vision/v3.2/describe?maxCandidates=1"
      curl -sX POST $url \
      -H "Ocp-Apim-Subscription-Key: $KEY" \
      -H "Content-Type: application/octet-stream" \
      --data-binary "@${image_path}" | jq .

  detect-objects-from-image:
    desc: Analyze an image using the AI service
    cmds:
    - | 
      source .env
      if [[ "{{.IMAGE_FILE_PATH}}" == "none" ]]; then
        echo "ERROR: Please provide the file path"
        exit 1
      fi
      image_path="{{.IMAGE_FILE_PATH}}"
      url="${ENDPOINT}/computervision/imageanalysis:analyze?api-version=2024-02-01&features=objects"
      curl -sX POST $url \
      -H "Ocp-Apim-Subscription-Key: $KEY" \
      -H "Content-Type: application/octet-stream" \
      --data-binary "@${image_path}" | jq .

  add-captions-to-image:
    desc: Add captions to the image
    cmds:
    - | 
      source .env
      if [[ "{{.IMAGE_FILE_PATH}}" == "none" ]]; then
        echo "ERROR: Please provide the file path"
        exit 1
      fi
      image_path="{{.IMAGE_FILE_PATH}}"
      url="${ENDPOINT}/computervision/imageanalysis:analyze?api-version=2024-02-01&features=caption"
      curl -sX POST $url \
      -H "Ocp-Apim-Subscription-Key: $KEY" \
      -H "Content-Type: application/octet-stream" \
      --data-binary "@${image_path}" | jq .

  add-tags-to-image:
    desc: Add tags to the image
    cmds:
    - | 
      source .env
      if [[ "{{.IMAGE_FILE_PATH}}" == "none" ]]; then
        echo "ERROR: Please provide the file path"
        exit 1
      fi
      image_path="{{.IMAGE_FILE_PATH}}"
      url="${ENDPOINT}/computervision/imageanalysis:analyze?api-version=2024-02-01&features=tags"
      curl -sX POST $url \
      -H "Ocp-Apim-Subscription-Key: $KEY" \
      -H "Content-Type: application/octet-stream" \
      --data-binary "@${image_path}" | jq .
  
  analyze-for-adult-content:
    desc: Analyze image for adult content
    cmds:
      - |
        source .env
        if [[ "{{.IMAGE_FILE_PATH}}" == "none" ]]; then
          echo "ERROR: Please provide the file path"
          exit 1
        fi
        image_path="{{.IMAGE_FILE_PATH}}"
        url="${ENDPOINT}/computervision/imageanalysis:analyze?api-version=2024-02-01&features=adult"
        curl -sX POST $url \
        -H "Ocp-Apim-Subscription-Key: $KEY" \
        -H "Content-Type: application/octet-stream" \
        --data-binary "@${image_path}" | jq .

  read-text-from-image:
    desc: Read the text from image
    cmds:
      - |
        source .env
        if [[ "{{.IMAGE_FILE_PATH}}" == "none" ]]; then
          echo "ERROR: Please provide the file path"
          exit 1
        fi
        image_path="{{.IMAGE_FILE_PATH}}"
        url="${ENDPOINT}/computervision/imageanalysis:analyze?api-version=2024-02-01&features=read"
        curl -sX POST $url \
        -H "Ocp-Apim-Subscription-Key: $KEY" \
        -H "Content-Type: application/octet-stream" \
        --data-binary "@${image_path}" | jq .

  read-text-from-image-in-japanese:
    desc: Read the text from image
    cmds:
      - |
        source .env
        if [[ "{{.IMAGE_FILE_PATH}}" == "none" ]]; then
          echo "ERROR: Please provide the file path"
          exit 1
        fi
        image_path="{{.IMAGE_FILE_PATH}}"
        url="${ENDPOINT}/computervision/imageanalysis:analyze?api-version=2024-02-01&features=read&language=pt"
        curl -sX POST $url \
        -H "Ocp-Apim-Subscription-Key: $KEY" \
        -H "Content-Type: application/octet-stream" \
        --data-binary "@${image_path}" | jq '.readResult.blocks[].lines[].text'

    # 

