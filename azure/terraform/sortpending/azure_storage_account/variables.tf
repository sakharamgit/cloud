##################################################################################
# VARIABLES
##################################################################################


variable "azure_key_path" {}

variable "az_prefix" {
  type    = string
  default = "ss"
}

variable "az_rg_name" {
  type    = string
  default = "sakharam_xor"
}

variable "az_location" {
  type    = string
  default = "East US"
}

variable "az_environment" {
  type    = string
  default = "dev"
}

variable "az_stor_acc_name" {
  type    = string
  default = "storage_account"
}

variable "az_stg_con_name" {
  type    = string
  default = "az_container"
}

resource "random_string" "random" {
  length  = 5
  upper   = false
  lower   = true
  numeric = true
  special = false
}



variable "blob_name_1" {
  type    = string
  default = "blob1"
}

variable "blob_name_1_path" {
  type = string
}

variable "blob_name_2" {
  type    = string
  default = "blob1"
}

variable "blob_name_2_path" {
  type = string
}

