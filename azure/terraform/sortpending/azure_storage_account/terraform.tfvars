//Azure Credentials
azure_key_path = "E:\\GitRepo\\automation\\common\\azure_terraform_user_key.pem"


az_rg_name       = "stg-acc"
az_location      = "Central India"
az_environment   = "dev"
az_stor_acc_name = "db"
az_stg_con_name  = "db-dacpac"

blob_name_1      = "schema_blob.dacpac"
blob_name_1_path = "./files/db_source.dacpac"

blob_name_2      = "movies.json"
blob_name_2_path = "./files/movies.json"
