#################################################
# Terraform script to create a new resource group
#################################################

//To create a resource group
resource "azurerm_resource_group" "example" {
  name     = var.az_resource_group
  location = var.az_location
}

// To create a virtual network
resource "azurerm_virtual_network" "example" {
  name                = var.az_vm_vnet
  address_space       = ["10.0.0.0/16"]
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name
}

// To create a virtual subnet
resource "azurerm_subnet" "example" {
  name                 = var.az_vnet_subnet
  resource_group_name  = azurerm_resource_group.example.name
  virtual_network_name = azurerm_virtual_network.example.name
  address_prefixes = [
    "10.0.2.0/24"
  ]
}

// To create an public ip for the instance
resource "azurerm_public_ip" "example" {
  name                = "azure-linux_vm_pip"
  resource_group_name = azurerm_resource_group.example.name
  location            = azurerm_resource_group.example.location
  allocation_method   = "Dynamic"
}

//To create a network interface to attach to the instance
resource "azurerm_network_interface" "example" {
  name                = "azure_linux_vm_nic"
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name

  ip_configuration {
    name                          = "azure-minux_vm_nic"
    subnet_id                     = azurerm_subnet.example.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.example.id
  }
}

// To create a network security group to allow traffic 
resource "azurerm_network_security_group" "example" {
  name                = "azure_linux_vm_nsg"
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name
  security_rule {
    access                     = "Allow"
    direction                  = "Inbound"
    name                       = "ssh"
    priority                   = 100
    protocol                   = "Tcp"
    source_port_range          = "*"
    source_address_prefixes    = ["0.0.0.0/0"]
    destination_port_range     = "22"
    destination_address_prefix = azurerm_network_interface.example.private_ip_address
  }
}

// To create a linux virtual machine
resource "azurerm_windows_virtual_machine" "example" {
  name                = var.az_vm_name
  resource_group_name = azurerm_resource_group.example.name
  location            = azurerm_resource_group.example.location
  size                = "Standard_F8"
  admin_username      = var.az_vm_admin_username
  admin_password      = var.az_vm_admin_password
  network_interface_ids = [
    azurerm_network_interface.example.id,
  ]

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "MicrosoftSQLServer"
    offer     = "sql2019-ws2019"
    sku       = "Standard"
    version   = "latest"
  }
}


