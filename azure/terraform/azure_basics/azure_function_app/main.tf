# To create an resource group
resource "azurerm_resource_group" "rg" {
  name     = "${var.az_prefix}-${var.environment}"
  location = var.az_location
}

resource "azurerm_storage_account" "storage" {
  name                     = "${var.az_prefix}-${var.environment}-${random_string.storage_name.result}"
  resource_group_name      = azurerm_resource_group.rg.name
  location                 = var.az_location
  account_tier             = "Standard"
  account_replication_type = "LRS"
}

resource "azurerm_app_service_plan" "asp" {
  name                = "asp"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  kind                = "FunctionApp"

  sku {
    tier = "Dynamic"
    size = "Y1"
  }
}
