//Azure Credentials
azure_key_path = "E:\\GitRepo\\common\\azure_terraform_user_key.pem"


az_linux_vnet_name   = "az_linux_vnet"
az_linux_subnet_name = "az_linux_subnet"
az_vnet_nic_name     = "az_linux_nic"
az_linux_vm_name     = "azlinuxsaphanavm"
az_rg_name           = "az_sap_hana_ss"
az_rg_location       = "East US"

dbadmin_password   = "Password1!"
server_config_path = "./files/server.cfg"
azure_script_path  = "./files/azure_script.sh"