//Azure Credentials
azure_key_path       = "E:\\GitRepo\\automation\\common\\azure_terraform_user_key.pem"
sample_txt_file_path = "sample_txt_file.txt"


az_resource_group = "xor_sakharam"
az_location       = "East US"

az_server_name   = "azwindowsvm"
az_vnet_name     = "az_win_vnet"
az_vnet_nic_name = "az_win_nic"

az_subnet1_name = "db_subnet"
az_subnet2_name = "web_app_subnet"
az_subnet3_name = "vm_subnet"

az_vm_admin_username = "winadmin"
az_vm_admin_password = "P@ssword1!"

az_vnet_address    = ["10.1.0.0/16"]
az_subnet1_address = ["10.1.1.0/24"]
az_subnet2_address = ["10.1.2.0/24"]
az_subnet3_address = ["10.1.3.0/24"]


sql_username    = "sqladmin"
sql_password    = "P@ssword1!"
sql_server_name = "ogmssqlserver"

az_prv_dns_zone_name = "privatelink.database.windows.net"

web_app_name = "og-web-app"

