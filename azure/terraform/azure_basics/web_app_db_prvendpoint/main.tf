# To create a resource group
resource "azurerm_resource_group" "example" {
  name     = var.az_resource_group
  location = var.az_location
}

# To create a virtual network
resource "azurerm_virtual_network" "example" {
  name                = var.az_vnet_name
  address_space       = var.az_vnet_address
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name
}

# To create a subnet for exposing db endpoint
resource "azurerm_subnet" "db" {
  name                                           = var.az_subnet1_name
  resource_group_name                            = azurerm_resource_group.example.name
  virtual_network_name                           = azurerm_virtual_network.example.name
  address_prefixes                               = var.az_subnet1_address
  enforce_private_link_endpoint_network_policies = true
}

# To create a subnet for exposing web app vnet integration
resource "azurerm_subnet" "webapp" {
  name                 = var.az_subnet2_name
  resource_group_name  = azurerm_resource_group.example.name
  virtual_network_name = azurerm_virtual_network.example.name
  address_prefixes     = var.az_subnet2_address

  delegation {
    name = "example-delegation"

    service_delegation {
      name    = "Microsoft.Web/serverFarms"
      actions = ["Microsoft.Network/virtualNetworks/subnets/action"]
    }
  }
}

# To create a subnet for virutal machine
resource "azurerm_subnet" "vm" {
  name                                           = var.az_subnet3_name
  resource_group_name                            = azurerm_resource_group.example.name
  virtual_network_name                           = azurerm_virtual_network.example.name
  address_prefixes                               = var.az_subnet3_address
  enforce_private_link_endpoint_network_policies = true
}

/*
# To create a Private DNS Zone
resource "azurerm_private_dns_zone" "og-private-dns" {
  name                = "cloudutsuk.lan"
  resource_group_name = azurerm_resource_group.example.name
}

# To link the Private DNS Zone with the VNET
resource "azurerm_private_dns_zone_virtual_network_link" "og-private-dns-link" {
  name                  = "ogprvdnszonevnetlink"
  resource_group_name   = azurerm_resource_group.example.name
  private_dns_zone_name = azurerm_private_dns_zone.og-private-dns.name
  virtual_network_id    = azurerm_virtual_network.example.id
}
*/

# To create a Private DNS Zone for Azure SQL
resource "azurerm_private_dns_zone" "og-db-private-dns" {
  name                = var.az_prv_dns_zone_name
  resource_group_name = azurerm_resource_group.example.name
}

# To create an A Record for VM in dns zone
resource "azurerm_private_dns_a_record" "og-endpoint-dns-a-record" {
  depends_on          = [azurerm_windows_virtual_machine.example]
  name                = lower(azurerm_windows_virtual_machine.example.name)
  zone_name           = azurerm_private_dns_zone.og-db-private-dns.name
  resource_group_name = azurerm_resource_group.example.name
  ttl                 = 300
  records             = [azurerm_windows_virtual_machine.example.private_ip_address]
}


# To create a link for private DNS and VNET 
resource "azurerm_private_dns_zone_virtual_network_link" "db-dns-zone-to-vnet-link" {
  name                  = "sql-db-dns-zone-vnet-link"
  resource_group_name   = azurerm_resource_group.example.name
  private_dns_zone_name = azurerm_private_dns_zone.og-db-private-dns.name
  virtual_network_id    = azurerm_virtual_network.example.id
}


