##################################################################################
# VARIABLES
##################################################################################


//Azure Variables
variable "subscription_id" {}
variable "client_id" {}
variable "client_secret" {}
variable "tenant_id" {}
variable "azure_key_path" {}
variable "sample_txt_file_path" {}


variable "az_resource_group" {}
variable "az_location" {}

variable "az_server_name" {}
variable "az_vnet_name" {}
variable "az_subnet1_name" {}
variable "az_subnet2_name" {}
variable "az_subnet3_name" {}
variable "az_vnet_nic_name" {}

variable "az_vm_admin_username" {}
variable "az_vm_admin_password" {}

variable "az_vnet_address" {}
variable "az_subnet1_address" {}
variable "az_subnet2_address" {}
variable "az_subnet3_address" {}

variable "sql_server_name" {}
variable "sql_username" {}
variable "sql_password" {}

variable "az_prv_dns_zone_name" {}

variable "web_app_name" {}
