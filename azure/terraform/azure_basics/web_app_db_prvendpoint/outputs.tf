#############################################################################################################
# OUTPUT
#############################################################################################################

output "azure_instance_public_dns" {
  value = azurerm_windows_virtual_machine.example.public_ip_address
}

output "az_sql_server_name" {
  value = azurerm_mssql_server.az_sql_server.fully_qualified_domain_name
}

output "az_web_app_url" {
  value = "${azurerm_app_service.example.name}.azurewebsites.net"
}

