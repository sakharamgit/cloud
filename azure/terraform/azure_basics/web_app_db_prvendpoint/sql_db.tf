# To create an sql server instance on Azure
resource "azurerm_mssql_server" "az_sql_server" {
  name                         = "ogmssqlserver"
  resource_group_name          = azurerm_resource_group.example.name
  location                     = azurerm_resource_group.example.location
  version                      = "12.0"
  administrator_login          = var.sql_username
  administrator_login_password = var.sql_password
  minimum_tls_version          = "1.2"

  /*
  azuread_administrator {
    login_username = "AzureAD Admin"
    object_id      = "00000000-0000-0000-0000-000000000000"
  }
  */
  tags = {
    environment = "development"
  }
}

# To create a firewall policy to allow access from internet
resource "azurerm_sql_firewall_rule" "az_sql_firewall_rule" {
  name                = "sqlfirewallrule"
  resource_group_name = azurerm_resource_group.example.name
  server_name         = azurerm_mssql_server.az_sql_server.name
  start_ip_address    = "0.0.0.0"
  end_ip_address      = "255.255.255.255"
}

# To create a sql elastic pool
resource "azurerm_mssql_elasticpool" "ms_elastic_pool" {
  name                = "ogelasticpool"
  resource_group_name = azurerm_resource_group.example.name
  location            = azurerm_resource_group.example.location
  server_name         = azurerm_mssql_server.az_sql_server.name
  license_type        = "LicenseIncluded"
  max_size_gb         = 50

  sku {
    name     = "GP_Gen5"
    tier     = "GeneralPurpose"
    family   = "Gen5"
    capacity = 2
  }

  per_database_settings {
    min_capacity = 0.25
    max_capacity = 2
  }
}

# To create a database on the sql server instance
resource "azurerm_mssql_database" "database1" {
  name         = "db1"
  server_id    = azurerm_mssql_server.az_sql_server.id
  collation    = "SQL_Latin1_General_CP1_CI_AS"
  license_type = "LicenseIncluded"
  max_size_gb  = 4
  //read_scale     = true
  //sku_name       = "BC_Gen5_2"
  zone_redundant  = true
  sample_name     = "AdventureWorksLT"
  elastic_pool_id = azurerm_mssql_elasticpool.ms_elastic_pool.id

  tags = {
    environment = "development"
  }
}

# To create a DB Private Endpoint
resource "azurerm_private_endpoint" "og-db-endpoint" {
  depends_on          = [azurerm_mssql_server.az_sql_server]
  name                = "og-sql-db-endpoint"
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name
  subnet_id           = azurerm_subnet.db.id
  # To integrate the endpoint with a private dns zone
  private_dns_zone_group {
    name                 = azurerm_private_dns_zone.og-db-private-dns.name
    private_dns_zone_ids = [azurerm_private_dns_zone.og-db-private-dns.id]
  }
  private_service_connection {
    name                           = "og-sql-db-endpoint"
    is_manual_connection           = "false"
    private_connection_resource_id = azurerm_mssql_server.az_sql_server.id
    subresource_names              = ["sqlServer"]
  }
}

# To create DB Private Endpoint Connecton
data "azurerm_private_endpoint_connection" "og-endpoint-connection" {
  depends_on          = [azurerm_private_endpoint.og-db-endpoint]
  name                = azurerm_private_endpoint.og-db-endpoint.name
  resource_group_name = azurerm_resource_group.example.name
}