##################################################################################
# VARIABLES
##################################################################################

//Common Variables
variable "prefix" {
  type    = string
  default = "ss"
}

variable "project" {
  type    = string
  default = "kv"
}

variable "env" {
  type    = string
  default = "lab"
}

//Azure Variables
variable "az_location" {}

variable "AA_LAB_ARM_SUBSCRIPTION_ID" {
  type = string
}

variable "AA_ARM_CLIENT_ID" {
  type = string
}

variable "AA_ARM_CLIENT_SECRET" {
  type = string
}

variable "AA_ARM_TENANT_ID" {
  type = string
}
