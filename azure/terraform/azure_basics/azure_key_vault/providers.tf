
// Configure the Azure Provider
provider "azurerm" {
  subscription_id = var.AA_LAB_ARM_SUBSCRIPTION_ID
  client_id       = var.AA_ARM_CLIENT_ID
  client_secret   = var.AA_ARM_CLIENT_SECRET
  tenant_id       = var.AA_ARM_TENANT_ID

  features {}
}