# To create the resource group
resource "azurerm_resource_group" "az_resource_group" {
  name     = var.az_resource_group
  location = var.az_location
}

/*
# To create an storage account for storing audit information
resource "azurerm_storage_account" "az_storage_acc" {
  name                     = "ogazstorageacc"
  resource_group_name      = azurerm_resource_group.az_resource_group.name
  location                 = azurerm_resource_group.az_resource_group.location
  account_tier             = "Standard"
  account_replication_type = "LRS"
}
*/

# To create an sql server instance on Azure
resource "azurerm_mssql_server" "az_sql_server" {
  name                         = var.sql_server_name
  resource_group_name          = azurerm_resource_group.az_resource_group.name
  location                     = azurerm_resource_group.az_resource_group.location
  version                      = "12.0"
  administrator_login          = var.sql_username
  administrator_login_password = var.sql_password
  minimum_tls_version          = "1.2"

  /*
  azuread_administrator {
    login_username = "AzureAD Admin"
    object_id      = "00000000-0000-0000-0000-000000000000"
  }
  */
  tags = {
    environment = "development"
  }
}

/*
# To create an SQL Audit Policy
resource "azurerm_mssql_server_extended_auditing_policy" "az_sql_audit_policy" {
  server_id                               = azurerm_mssql_server.az_sql_server.id
  storage_endpoint                        = azurerm_storage_account.az_storage_acc.primary_blob_endpoint
  storage_account_access_key              = azurerm_storage_account.az_storage_acc.primary_access_key
  storage_account_access_key_is_secondary = false
  retention_in_days                       = 6
}
*/

# To create a firewall policy to allow access from internet
resource "azurerm_sql_firewall_rule" "az_sql_firewall_rule" {
  name                = "sqlfirewallrule"
  resource_group_name = azurerm_resource_group.az_resource_group.name
  server_name         = azurerm_mssql_server.az_sql_server.name
  start_ip_address    = "0.0.0.0"
  end_ip_address      = "255.255.255.255"
}

# To create a sql elastic pool
resource "azurerm_mssql_elasticpool" "ms_elastic_pool" {
  name                = var.sql_elastic_pool1
  resource_group_name = azurerm_resource_group.az_resource_group.name
  location            = azurerm_resource_group.az_resource_group.location
  server_name         = azurerm_mssql_server.az_sql_server.name
  license_type        = "LicenseIncluded"
  max_size_gb         = 50

  sku {
    name     = "GP_Gen5"
    tier     = "GeneralPurpose"
    family   = "Gen5"
    capacity = 2
  }

  per_database_settings {
    min_capacity = 0.25
    max_capacity = 2
  }
}

# To create a database on the sql server instance
resource "azurerm_mssql_database" "database1" {
  name         = var.database_name1
  server_id    = azurerm_mssql_server.az_sql_server.id
  collation    = "SQL_Latin1_General_CP1_CI_AS"
  license_type = "LicenseIncluded"
  max_size_gb  = 4
  //read_scale     = true
  //sku_name       = "BC_Gen5_2"
  zone_redundant  = true
  sample_name     = "AdventureWorksLT"
  elastic_pool_id = azurerm_mssql_elasticpool.ms_elastic_pool.id

  tags = {
    environment = "development"
  }

}

# To create a virtual network for private endpoint
resource "azurerm_virtual_network" "vnet" {
  name                = "ogprivateendvnet"
  address_space       = ["10.0.0.0/16"]
  location            = azurerm_resource_group.az_resource_group.location
  resource_group_name = azurerm_resource_group.az_resource_group.name
}

# To create a subnet for database 
resource "azurerm_subnet" "dbsubnet" {
  name                 = "ogsubnetservice"
  resource_group_name  = azurerm_resource_group.az_resource_group.name
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefixes     = ["10.0.1.0/24"]
  enforce_private_link_endpoint_network_policies = true
}

# To create a Private DNS Zone
resource "azurerm_private_dns_zone" "og-private-dns" {
  name                = "ogprvdnszone.lan"
  resource_group_name = azurerm_resource_group.az_resource_group.name
}

# To link the Private DNS Zone with the VNET
resource "azurerm_private_dns_zone_virtual_network_link" "og-private-dns-link" {
  name                  = "ogprvdnszonevnetlink"
  resource_group_name   = azurerm_resource_group.az_resource_group.name
  private_dns_zone_name = azurerm_private_dns_zone.og-private-dns.name
  virtual_network_id    = azurerm_virtual_network.vnet.id
}

# Create a DB Private DNS Zone
resource "azurerm_private_dns_zone" "ogendpoint-dns-private-zone" {
  name                = "cloudutsuk.database.windows.net"
  resource_group_name = azurerm_resource_group.az_resource_group.name
}

# To create a DB Private Endpoint
resource "azurerm_private_endpoint" "og-db-endpoint" {
  depends_on          = [azurerm_mssql_server.az_sql_server]
  name                = "og-sql-db-endpoint"
  location            = azurerm_resource_group.az_resource_group.location
  resource_group_name = azurerm_resource_group.az_resource_group.name
  subnet_id           = azurerm_subnet.dbsubnet.id
  private_service_connection {
    name                           = "og-sql-db-endpoint"
    is_manual_connection           = "false"
    private_connection_resource_id = azurerm_mssql_server.az_sql_server.id
    subresource_names              = ["sqlServer"]
  }
}

# To create DB Private Endpoint Connecton
data "azurerm_private_endpoint_connection" "og-endpoint-connection" {
  depends_on          = [azurerm_private_endpoint.og-db-endpoint]
  name                = azurerm_private_endpoint.og-db-endpoint.name
  resource_group_name = azurerm_resource_group.az_resource_group.name
}

# To create a DB Private DNS A Record
resource "azurerm_private_dns_a_record" "og-endpoint-dns-a-record" {
  depends_on          = [azurerm_mssql_server.az_sql_server]
  name                = lower(azurerm_mssql_server.az_sql_server.name)
  zone_name           = azurerm_private_dns_zone.og-private-dns.name
  resource_group_name = azurerm_resource_group.az_resource_group.name
  ttl                 = 300
  records             = [data.azurerm_private_endpoint_connection.og-endpoint-connection.private_service_connection.0.private_ip_address]
}
