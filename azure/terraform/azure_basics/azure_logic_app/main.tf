#################################################
# Terraform script to create a an Azure Logic App
#################################################

# Locals 
locals {
  #arm_file_path = "./deploy_logic_app"
  common_tags = merge(
    { "environment" = var.env },
    { "terraform" = true }
  )
}

//To create a resource group
resource "azurerm_resource_group" "az-logicapp-rg" {
  name     = "${var.prefix}-${var.project}-rg"
  location = var.az_location
}

// Create an instance of logic app and configure the tags
resource "azurerm_logic_app_workflow" "logicapp" {
  name                = "${var.prefix}-${var.project}-workflow"
  location            = var.az_location
  resource_group_name = azurerm_resource_group.az-logicapp-rg.name
  tags                = local.common_tags
}

# // Deploy the ARM template to configure the workflow in the Logic App
# data "template_file" "workflow" {
#   template = file(local.arm_file_path)
# }

# // Deploy the ARM template workflow
# resource "azurerm_template_deployment" "workflow" {
#   depends_on = [azurerm_logic_app_workflow.logicapp]

#   resource_group_name = var.shared_env.rg.name
#   parameters = merge({
#     "workflowName" = var.workflow_name,
#     "location"     = "westeurope"
#   }, var.parameters)

#   template_body = data.template_file.workflow.template
# }