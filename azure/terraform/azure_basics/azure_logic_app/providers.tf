// Configure the Azure Provider
provider "azurerm" {
  subscription_id = var.RM_ARM_SUBSCRIPTION_ID
  client_id       = var.RM_ARM_CLIENT_ID
  client_secret   = var.RM_ARM_CLIENT_SECRET
  tenant_id       = var.RM_ARM_TENANT_ID
  features {}
}