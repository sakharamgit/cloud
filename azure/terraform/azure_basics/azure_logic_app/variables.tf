##################################################################################
# VARIABLES
##################################################################################

// Common Variables
variable "prefix" {
  type    = string
  default = "ss"
}

variable "project" {
  type    = string
  default = "azlogicapp"
}

variable "az_location" {
  type    = string
  default = "eastus"
}

variable "env" {
  type    = string
  default = "dev"
}

resource "random_string" "random" {
  length  = 5
  upper   = false
  lower   = true
  number  = true
  special = false
}

//Azure Variables
variable "RM_ARM_SUBSCRIPTION_ID" {}
variable "RM_ARM_CLIENT_ID" {}
variable "RM_ARM_CLIENT_SECRET" {}
variable "RM_ARM_TENANT_ID" {}