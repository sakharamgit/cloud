//Azure Credentials
azure_key_path  = "E:\\GitRepo\\automation\\common\\azure_terraform_user_key.pem"

az_resource_group        = "az_sql_db_fg_ss"
sql_username             = "sqladmin"
sql_password             = "P@ssword1!"
sql_server_name1         = "sql-srvr-primary"
sql_server_name2         = "sql-srvr-dr"
az_location              = "East US"
az_locationdr            = "East US 2"
sql_elastic_pool_name1   = "pool1"
sql_elastic_pool_name2   = "pool2"
sql_failover_group_name1 = "az-sql-fg-group1"