##################################################################################
# VARIABLES
##################################################################################


//Azure Variables
variable "subscription_id" {}
variable "client_id" {}
variable "client_secret" {}
variable "tenant_id" {}
variable "azure_key_path" {}

variable "sql_server_name1" {}
variable "sql_server_name2" {}
variable "sql_username" {}
variable "sql_password" {}
variable "az_resource_group" {}
variable "az_location" {}
variable "az_locationdr" {}
variable "sql_elastic_pool_name1" {}
variable "sql_elastic_pool_name2" {}
variable "sql_failover_group_name1" {}


