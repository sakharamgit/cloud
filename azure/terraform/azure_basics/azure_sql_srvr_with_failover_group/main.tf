# To create the resource group
resource "azurerm_resource_group" "az_resource_group" {
  name     = var.az_resource_group
  location = var.az_location
}


# To create an sql server instance in primary zone on Azure
resource "azurerm_mssql_server" "az_sql_server" {
  name                         = var.sql_server_name1
  resource_group_name          = azurerm_resource_group.az_resource_group.name
  location                     = azurerm_resource_group.az_resource_group.location
  version                      = "12.0"
  administrator_login          = var.sql_username
  administrator_login_password = var.sql_password
  minimum_tls_version          = "1.2"

  /*
  azuread_administrator {
    login_username = "AzureAD Admin"
    object_id      = "00000000-0000-0000-0000-000000000000"
  }
  */
  tags = {
    environment = "development"
  }
}


# To create an sql server instance in DR zone on Azure
resource "azurerm_mssql_server" "az_sql_serverdr" {
  name                         = var.sql_server_name2
  resource_group_name          = azurerm_resource_group.az_resource_group.name
  location                     = var.az_locationdr
  version                      = "12.0"
  administrator_login          = var.sql_username
  administrator_login_password = var.sql_password
  minimum_tls_version          = "1.2"

  /*
  azuread_administrator {
    login_username = "AzureAD Admin"
    object_id      = "00000000-0000-0000-0000-000000000000"
  }
  */
  tags = {
    environment = "development"
  }
}


# To create a firewall policy to allow access from internet
resource "azurerm_sql_firewall_rule" "az_sql_firewall_rule" {
  name                = "sqlfirewallrule"
  resource_group_name = azurerm_resource_group.az_resource_group.name
  server_name         = azurerm_mssql_server.az_sql_server.name
  start_ip_address    = "0.0.0.0"
  end_ip_address      = "255.255.255.255"
}


resource "azurerm_sql_firewall_rule" "az_sql_firewall_ruledr" {
  name                = "sqlfirewallruledr"
  resource_group_name = azurerm_resource_group.az_resource_group.name
  server_name         = azurerm_mssql_server.az_sql_serverdr.name
  start_ip_address    = "0.0.0.0"
  end_ip_address      = "255.255.255.255"
}

# To create a sql elastic pool on Primary
resource "azurerm_mssql_elasticpool" "ms_elastic_pool1" {
  name                = var.sql_elastic_pool_name1
  resource_group_name = azurerm_resource_group.az_resource_group.name
  location            = azurerm_resource_group.az_resource_group.location
  server_name         = azurerm_mssql_server.az_sql_server.name
  license_type        = "LicenseIncluded"
  max_size_gb         = 50

  sku {
    name     = "GP_Gen5"
    tier     = "GeneralPurpose"
    family   = "Gen5"
    capacity = 2
  }

  per_database_settings {
    min_capacity = 0.25
    max_capacity = 2
  }
}


resource "azurerm_mssql_elasticpool" "ms_elastic_pool2" {
  name                = var.sql_elastic_pool_name2
  resource_group_name = azurerm_resource_group.az_resource_group.name
  location            = azurerm_resource_group.az_resource_group.location
  server_name         = azurerm_mssql_server.az_sql_server.name
  license_type        = "LicenseIncluded"
  max_size_gb         = 50

  sku {
    name     = "GP_Gen5"
    tier     = "GeneralPurpose"
    family   = "Gen5"
    capacity = 2
  }

  per_database_settings {
    min_capacity = 0.25
    max_capacity = 2
  }
}


# To create a sql elastic pool on DR
resource "azurerm_mssql_elasticpool" "ms_elastic_pooldr1" {
  name                = var.sql_elastic_pool_name1
  resource_group_name = azurerm_resource_group.az_resource_group.name
  location            = var.az_locationdr
  server_name         = azurerm_mssql_server.az_sql_serverdr.name
  license_type        = "LicenseIncluded"
  max_size_gb         = 50

  sku {
    name     = "GP_Gen5"
    tier     = "GeneralPurpose"
    family   = "Gen5"
    capacity = 2
  }

  per_database_settings {
    min_capacity = 0.25
    max_capacity = 2
  }
}


resource "azurerm_mssql_elasticpool" "ms_elastic_pooldr2" {
  name                = var.sql_elastic_pool_name2
  resource_group_name = azurerm_resource_group.az_resource_group.name
  location            = var.az_locationdr
  server_name         = azurerm_mssql_server.az_sql_serverdr.name
  license_type        = "LicenseIncluded"
  max_size_gb         = 50

  sku {
    name     = "GP_Gen5"
    tier     = "GeneralPurpose"
    family   = "Gen5"
    capacity = 2
  }

  per_database_settings {
    min_capacity = 0.25
    max_capacity = 2
  }
}


# To create databases on primary
resource "azurerm_mssql_database" "database1" {
  name         = "db1"
  server_id    = azurerm_mssql_server.az_sql_server.id
  collation    = "SQL_Latin1_General_CP1_CI_AS"
  license_type = "LicenseIncluded"
  max_size_gb  = 4
  //read_scale     = true
  //sku_name       = "BC_Gen5_2"
  //zone_redundant  = true
  //sample_name     = "AdventureWorksLT"
  elastic_pool_id = azurerm_mssql_elasticpool.ms_elastic_pool1.id

  tags = {
    environment = "development"
  }

}

# To create databases on primary
resource "azurerm_mssql_database" "database2" {
  name         = "db2"
  server_id    = azurerm_mssql_server.az_sql_server.id
  collation    = "SQL_Latin1_General_CP1_CI_AS"
  license_type = "LicenseIncluded"
  max_size_gb  = 4
  //read_scale     = true
  //sku_name       = "BC_Gen5_2"
  //zone_redundant  = true
  //sample_name     = "AdventureWorksLT"
  elastic_pool_id = azurerm_mssql_elasticpool.ms_elastic_pool2.id

  tags = {
    environment = "development"
  }
}

# To create a azure sql failover group
resource "azurerm_sql_failover_group" "az_sql_fg" {
  name                = var.sql_failover_group_name1
  resource_group_name = azurerm_mssql_server.az_sql_server.resource_group_name
  server_name         = azurerm_mssql_server.az_sql_server.name
  databases           = [azurerm_mssql_database.database1.id, azurerm_mssql_database.database2.id]
  partner_servers {
    id = azurerm_mssql_server.az_sql_serverdr.id
  }

  read_write_endpoint_failover_policy {
    mode          = "Automatic"
    grace_minutes = 60
  }

  depends_on = [azurerm_mssql_database.database1, azurerm_mssql_database.database2]
}

# To create databases on secondary
resource "azurerm_mssql_database" "drdatabase1" {
  name         = "db1"
  server_id    = azurerm_mssql_server.az_sql_serverdr.id
  collation    = "SQL_Latin1_General_CP1_CI_AS"
  license_type = "LicenseIncluded"
  max_size_gb  = 4
  //read_scale     = true
  //sku_name       = "BC_Gen5_2"
  //zone_redundant  = true
  //sample_name     = "AdventureWorksLT"
  elastic_pool_id = azurerm_mssql_elasticpool.ms_elastic_pooldr1.id
  depends_on      = [azurerm_mssql_elasticpool.ms_elastic_pooldr1, azurerm_sql_failover_group.az_sql_fg]

  tags = {
    environment = "development"
  }

}

# To create databases on secondary
resource "azurerm_mssql_database" "drdatabase2" {
  name         = "db2"
  server_id    = azurerm_mssql_server.az_sql_serverdr.id
  collation    = "SQL_Latin1_General_CP1_CI_AS"
  license_type = "LicenseIncluded"
  max_size_gb  = 4
  //read_scale     = true
  //sku_name       = "BC_Gen5_2"
  //zone_redundant  = true
  //sample_name     = "AdventureWorksLT"
  elastic_pool_id = azurerm_mssql_elasticpool.ms_elastic_pooldr2.id

  depends_on = [azurerm_mssql_elasticpool.ms_elastic_pooldr2, azurerm_sql_failover_group.az_sql_fg]
  tags = {
    environment = "development"
  }
}

# To create geo-replicated databases on primary
resource "azurerm_mssql_database" "database3" {
  name         = "db3"
  server_id    = azurerm_mssql_server.az_sql_server.id
  collation    = "SQL_Latin1_General_CP1_CI_AS"
  license_type = "LicenseIncluded"
  max_size_gb  = 4
  read_scale   = true
  sku_name     = "BC_Gen5_2"
  //zone_redundant  = true
  //sample_name     = "AdventureWorksLT"

  tags = {
    environment = "development"
  }
}


# To create database replica on secondary
resource "azurerm_mssql_database" "drdatabase3" {
  name                        = "db3"
  server_id                   = azurerm_mssql_server.az_sql_serverdr.id
  create_mode                 = "Secondary"
  creation_source_database_id = azurerm_mssql_database.database3.id

  tags = {
    environment = "development"
  }
}