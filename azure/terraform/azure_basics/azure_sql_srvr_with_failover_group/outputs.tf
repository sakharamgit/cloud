#############################################################################################################
# OUTPUT
#############################################################################################################

output "az_sql_server_name_primary" {
  value = azurerm_mssql_server.az_sql_server.fully_qualified_domain_name
}


output "az_sql_server_name_dr" {
  value = azurerm_mssql_server.az_sql_serverdr.fully_qualified_domain_name
}