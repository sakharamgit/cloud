resource "tls_private_key" "le_private_key" {
  algorithm = "RSA"
}

resource "acme_registration" "registration" {
  account_key_pem = tls_private_key.le_private_key.private_key_pem
  email_address   = var.email_address
}

resource "acme_certificate" "certificates" {
  account_key_pem           = acme_registration.registration.account_key_pem
  common_name               = var.common_name
  subject_alternative_names = var.subject_alternative_names
  min_days_remaining        = var.min_days_remaining
  dns_challenge {
    provider = "azure"
    config = {
      AZURE_CLIENT_ID       = var.AA_ARM_CLIENT_ID
      AZURE_CLIENT_SECRET   = var.AA_ARM_CLIENT_SECRET
      AZURE_ENVIRONMENT     = "public"
      AZURE_RESOURCE_GROUP  = var.dns_resource_group
      AZURE_SUBSCRIPTION_ID = var.AA_LAB_ARM_SUBSCRIPTION_ID
      AZURE_TENANT_ID       = var.AA_ARM_TENANT_ID
    }
  }
}
data "azurerm_key_vault" "key_vault" {
  name                = var.keyvault_name
  resource_group_name = var.keyvault_resource_group
}

resource "random_string" "random" {
  length  = 5
  lower   = true
  special = false
}

resource "azurerm_key_vault_certificate" "key_vault_certificates" {

  name         = join("", ["wildcard-", split("T", timestamp())[0]])
  key_vault_id = data.azurerm_key_vault.key_vault.id

  certificate {
    contents = acme_certificate.certificates.certificate_p12
    password = ""
  }

  certificate_policy {
    issuer_parameters {
      name = "Unknown"
    }

    key_properties {
      exportable = true
      key_size   = 2048
      key_type   = "RSA"
      reuse_key  = false
    }

    secret_properties {
      content_type = "application/x-pkcs12"
    }
  }
}

output "certificate_name" {
  value = azurerm_key_vault_certificate.key_vault_certificates.name
}
