
// Configure the Azure Provider
provider "azurerm" {
  subscription_id = var.SS_ARM_SUBSCRIPTION_ID
  client_id       = var.SS_ARM_CLIENT_ID
  client_secret   = var.SS_ARM_CLIENT_SECRET
  tenant_id       = var.SS_ARM_TENANT_ID
  features {}
}

provider "azuread" {}