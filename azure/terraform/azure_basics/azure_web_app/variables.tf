##################################################################################
# VARIABLES
##################################################################################


//Azure Variables
variable "subscription_id" {}
variable "client_id" {}
variable "client_secret" {}
variable "tenant_id" {}
variable "azure_key_path" {}

variable "web_app_name" {}
variable "az_resource_group" {}
variable "az_location" {}


variable "scm_repoUrl" {}
variable "scm_branch" {}