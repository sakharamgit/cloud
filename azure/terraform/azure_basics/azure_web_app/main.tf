# To create the resource group
resource "azurerm_resource_group" "az_resource_group" {
  name     = var.az_resource_group
  location = var.az_location
}


# To create an app service plan
resource "azurerm_app_service_plan" "appsvcplan" {
  name                = "ogappserviceplan"
  location            = azurerm_resource_group.az_resource_group.location
  resource_group_name = azurerm_resource_group.az_resource_group.name
  kind                = "Linux"
  reserved            = true

  sku {
    tier = "Standard"
    size = "S1"
  }
}

# To create an azure web app
resource "azurerm_app_service" "webapp" {
  name                = var.web_app_name
  location            = azurerm_resource_group.az_resource_group.location
  resource_group_name = azurerm_resource_group.az_resource_group.name
  app_service_plan_id = azurerm_app_service_plan.appsvcplan.id

  site_config {
    linux_fx_version = "PHP|7.4"
  }

  app_settings = {
    "SOME_KEY" = "some-value"
  }

  connection_string {
    name  = "Database"
    type  = "SQLServer"
    value = "Server=some-server.mydomain.com;Integrated Security=SSPI"
  }
}

/*
# To configure SCM for the web app
resource "null_resource" "scm_integration" { 
#using powershell 4-now, replacing when scm_type externalGit is avaliable in provider
provisioner "local-exec" {

 command = "${path.module}/add_scm.ps1 -webAppName ${var.web_app_name} -appResourceGroupName ${var.az_resource_group} -scmBranch ${var.scm_branch} -repoUrl ${var.scm_repoUrl}"
    interpreter = ["PowerShell", "-Command"]
  }
}
*/