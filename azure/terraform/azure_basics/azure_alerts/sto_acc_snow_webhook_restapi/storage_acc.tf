// To create a storage account to monitor
resource "azurerm_storage_account" "az_storage_account" {
  name                     = "${local.prefix}${local.project}storacc"
  resource_group_name      = azurerm_resource_group.az-stoacc-snow-rg.name
  location                 = azurerm_resource_group.az-stoacc-snow-rg.location
  account_tier             = "Standard"
  account_replication_type = "LRS"
}

