Integration Azure Monitor with Service Now using Webhook and Event Management:
Ref Article:
https://docs.servicenow.com/bundle/paris-it-operations-management/page/product/event-management/task/azure-events-webhook.html


1. Setup Azure Alert on a resource with a webhook:
https://admin:mHdqab3PUOE6@dev109039.service-now.com/api/sn_em_connector/em/inbound_event?source=azuremonitor

2. Service Now:

a. Login into the developer account: https://developer.servicenow.com/dev.do
b. Select the Instance-> Active Plugin -> Enable 'Event Management'
c. In SNOW portal, navigate to 'ITOM Guided Setup' -> 'Event Management' -> Get Started -> Connectors 