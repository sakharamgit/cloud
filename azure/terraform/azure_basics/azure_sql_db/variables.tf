##################################################################################
# VARIABLES
##################################################################################


//Azure Variables
variable "subscription_id" {}
variable "client_id" {}
variable "client_secret" {}
variable "tenant_id" {}
variable "azure_key_path" {}

variable "sql_server_name" {}
variable "sql_username" {}
variable "sql_password" {}
variable "az_resource_group" {}
variable "az_location" {}
variable "database_name1" {}
variable "database_name2" {}
variable "database_name3" {}
variable "sql_elastic_pool1" {}
