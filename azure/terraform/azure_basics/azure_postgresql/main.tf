# To create the resource group
resource "azurerm_resource_group" "pgsql-rg" {
  name     = "${var.prefix}-${var.project}-${var.env}-rg"
  location = var.az_location
}

# To create an postgresql server on Azure
resource "azurerm_postgresql_server" "pgsql-srvr" {
  name                = "example-psqlserver"
  location            = azurerm_resource_group.pgsql-rg.location
  resource_group_name = azurerm_resource_group.pgsql-rg.name

  administrator_login          = var.sql_username
  administrator_login_password = var.sql_password

  sku_name   = "B_Gen5_1"
  version    = "11"
  storage_mb = 5120

  backup_retention_days        = 7
  geo_redundant_backup_enabled = false
  auto_grow_enabled            = false

  public_network_access_enabled    = true
  ssl_enforcement_enabled          = true
  ssl_minimal_tls_version_enforced = "TLS1_2"
}

# To create a Postgresql database
resource "azurerm_postgresql_database" "postgresql-db1" {
  name                = "${var.prefix}-${var.project}-${var.env}-db1"
  resource_group_name = azurerm_resource_group.pgsql-rg.name
  server_name         = azurerm_postgresql_server.pgsql-srvr.name
  charset             = "utf8"
  collation           = "English_United States.1252"
}

# To create a firewall policy to allow access from internet
resource "azurerm_sql_firewall_rule" "pgsql-fw" {
  name                = "${var.prefix}-${var.project}-${var.env}-sql-fw"
  resource_group_name = azurerm_resource_group.pgsql-rg.name
  server_name         = azurerm_postgresql_server.pgsql-srvr.name
  start_ip_address    = "0.0.0.0"
  end_ip_address      = "255.255.255.255"
}