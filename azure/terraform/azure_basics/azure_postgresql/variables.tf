##################################################################################
# VARIABLES
##################################################################################
//Azure Variables
variable "az_location" {}

variable prefix {
    type = string
    default = "ss"
}

variable project {
    type = string
    default = "pgsql"
}

variable "env" {
  type = string
  default = "dev"
}

variable "sql_username" {}
variable "sql_password" {}

