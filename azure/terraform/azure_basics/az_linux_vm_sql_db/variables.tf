##################################################################################
# VARIABLES
##################################################################################

//Azure Variables
variable "azure_key_path" {}
variable "az_linux_vnet_name" {}
variable "az_linux_subnet_name" {}
variable "az_vnet_nic_name" {}
variable "az_linux_vm_name" {}
variable "az_rg_name" {}
variable "az_rg_location" {}
variable "sa_password" {}

