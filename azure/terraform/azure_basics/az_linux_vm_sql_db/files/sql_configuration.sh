#! /bin/bash

# To the user context to root user
sudo su

# To stop the sql server service
sudo systemctl stop mssql-server

# To set the SA password
sudo MSSQL_SA_PASSWORD=${sa_password} /opt/mssql/bin/mssql-conf set-sa-password

# To start the sql server
sudo systemctl start mssql-server

# To add the sql tools sqlcmd and bcp to the environmental variables
echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> ~/.bash_profile
echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> ~/.bashrc
source ~/.bashrc

# To enable remote access to the sql server
sudo firewall-cmd --zone=public --add-port=1433/tcp --permanent
sudo firewall-cmd --reload