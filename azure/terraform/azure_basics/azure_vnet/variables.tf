#################################################
# Variables
#################################################

variable "prefix" {
  type    = string
  default = "ss"
}

variable "project" {
  type    = string
  default = "demo"
}

variable "env" {
  type    = string
  default = "dev"
}

variable "az_location" {
  type    = string
  default = "eastus"
}

variable "vnet_cidr_range" {
  type    = list
  default = ["10.0.0.0/16"]
}

variable "subnet_prefixes" {
  type    = list(string)
  default = ["10.0.0.0/24", "10.0.1.0/24"]
}