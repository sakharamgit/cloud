##################################################
# Resources
##################################################

# To create a resource group
resource "azurerm_resource_group" "example" {
  name     = "${var.prefix}-${var.project}-${var.env}-rg"
  location = var.az_location
}

# To create a network security group to allow traffic 
resource "azurerm_network_security_group" "example" {
  name                = "${var.prefix}-${var.project}-${var.env}-nsg-443"
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name
  security_rule {
    access                     = "Allow"
    direction                  = "Inbound"
    name                       = "https"
    priority                   = 100
    protocol                   = "Tcp"
    source_port_range          = "*"
    source_address_prefixes    = ["0.0.0.0/0"]
    destination_port_range     = "443"
    destination_address_prefix = "*"
  }
}


// To create a virtual network
resource "azurerm_virtual_network" "example" {
  name                = "${var.prefix}-${var.project}-${var.env}-vnet"
  address_space       = var.vnet_cidr_range
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name
}

// To create a virtual subnet
resource "azurerm_subnet" "example" {
  name                 = "${var.prefix}-${var.project}-${var.env}-subnet1"
  resource_group_name  = azurerm_resource_group.example.name
  virtual_network_name = azurerm_virtual_network.example.name
  address_prefixes = var.subnet1_cidr_range
}

// To assoicate the subnet with the security group
resource "azurerm_subnet_network_security_group_association" "example" {
  subnet_id                 = azurerm_subnet.example.id
  network_security_group_id = azurerm_network_security_group.example.id
}

# To create a virtual network
# resource "azurerm_virtual_network" "example" {
#   name                = "${var.prefix}-${var.project}-${var.env}-vnet"
#   location            = azurerm_resource_group.example.location
#   resource_group_name = azurerm_resource_group.example.name
#   address_space       = var.vnet_cidr_range

#   subnet {
#     name           = "${var.prefix}-${var.project}-${var.env}-subnet1"
#     address_prefix = "10.0.1.0/24"
#     security_group = azurerm_network_security_group.example.id
#   }

#   tags = {
#   environment = var.env 
#   }
# }

