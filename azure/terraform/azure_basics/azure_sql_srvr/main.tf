# To create the resource group
resource "azurerm_resource_group" "example" {
  name     = "${var.prefix}-${var.project}-${var.env}-rg"
  location = var.az_location
}

# To create an sql server instance on Azure
resource "azurerm_mssql_server" "example" {
  name                         = "${var.prefix}-${var.project}-${var.env}-mssql"
  resource_group_name          = azurerm_resource_group.example.name
  location                     = azurerm_resource_group.example.location
  version                      = "12.0"
  administrator_login          = var.sql_username
  administrator_login_password = var.sql_password
  minimum_tls_version          = "1.2"

  tags = {
    environment = var.env
  }
}

# To create a firewall policy to allow access from internet
resource "azurerm_sql_firewall_rule" "example" {
  name                = "${var.prefix}-${var.project}-${var.env}-sql-fw"
  resource_group_name = azurerm_resource_group.example.name
  server_name         = azurerm_mssql_server.example.name
  start_ip_address    = "0.0.0.0"
  end_ip_address      = "255.255.255.255"
}

# To create a sql elastic pool
resource "azurerm_mssql_elasticpool" "example" {
  name                = "${var.prefix}-${var.project}-${var.env}-pool"
  resource_group_name = azurerm_resource_group.example.name
  location            = azurerm_resource_group.example.location
  server_name         = azurerm_mssql_server.example.name
  license_type        = "LicenseIncluded"
  max_size_gb         = 50

  sku {
    name     = "GP_Gen5"
    tier     = "GeneralPurpose"
    family   = "Gen5"
    capacity = 2
  }

  per_database_settings {
    min_capacity = 0.25
    max_capacity = 2
  }
}

# To create a database on the sql server instance
resource "azurerm_mssql_database" "database1" {
  name         = "${var.prefix}-${var.project}-${var.env}-db1"
  server_id    = azurerm_mssql_server.example.id
  collation    = "SQL_Latin1_General_CP1_CI_AS"
  license_type = "LicenseIncluded"
  max_size_gb  = 4
  //read_scale     = true
  //sku_name       = "BC_Gen5_2"
  zone_redundant  = true
  sample_name     = "AdventureWorksLT"
  elastic_pool_id = azurerm_mssql_elasticpool.example.id

  tags = {
    environment = var.env
  }

}

# To create a database on the sql server instance
resource "azurerm_mssql_database" "database2" {
  name         = "${var.prefix}-${var.project}-${var.env}-db2"
  server_id    = azurerm_mssql_server.example.id
  collation    = "SQL_Latin1_General_CP1_CI_AS"
  license_type = "LicenseIncluded"
  max_size_gb  = 4
  //read_scale     = true
  //sku_name       = "BC_Gen5_2"
  zone_redundant  = true
  sample_name     = "AdventureWorksLT"
  elastic_pool_id = azurerm_mssql_elasticpool.example.id

  tags = {
    environment = var.env
  }

}
