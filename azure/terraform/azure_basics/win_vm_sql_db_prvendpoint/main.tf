# To create a resource group
resource "azurerm_resource_group" "example" {
  name     = var.az_resource_group
  location = var.az_location
}

# To create a virtual network
resource "azurerm_virtual_network" "example" {
  name                = var.az_vnet_name
  address_space       = var.az_vnet_address
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name
}

# To create a subnet
resource "azurerm_subnet" "example" {
  name                 = var.az_subnet1_name
  resource_group_name  = azurerm_resource_group.example.name
  virtual_network_name = azurerm_virtual_network.example.name
  address_prefixes     = var.az_subnet1_address
  enforce_private_link_endpoint_network_policies = true
}

# To create a network interface
resource "azurerm_network_interface" "example" {
  name                = var.az_vnet_nic_name
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.example.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.example.id
  }
}

# To create an public ip for the instance
resource "azurerm_public_ip" "example" {
  name                = "az_winvm_pip"
  resource_group_name = azurerm_resource_group.example.name
  location            = azurerm_resource_group.example.location
  allocation_method   = "Dynamic"
}


# To create a virtual machine
resource "azurerm_windows_virtual_machine" "example" {
  name                = var.az_server_name
  resource_group_name = azurerm_resource_group.example.name
  location            = azurerm_resource_group.example.location
  size                = "Standard_F2"
  admin_username      = var.az_vm_admin_username
  admin_password      = var.az_vm_admin_password
  network_interface_ids = [
    azurerm_network_interface.example.id,
  ]

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "MicrosoftWindowsServer"
    offer     = "WindowsServer"
    sku       = "2016-Datacenter"
    version   = "latest"
  }
}


# To create an sql server instance on Azure
resource "azurerm_mssql_server" "az_sql_server" {
  name                         = "ogmssqlserver"
  resource_group_name          = azurerm_resource_group.example.name
  location                     = azurerm_resource_group.example.location
  version                      = "12.0"
  administrator_login          = var.sql_username
  administrator_login_password = var.sql_password
  minimum_tls_version          = "1.2"

  /*
  azuread_administrator {
    login_username = "AzureAD Admin"
    object_id      = "00000000-0000-0000-0000-000000000000"
  }
  */
  tags = {
    environment = "development"
  }
}

# To create a firewall policy to allow access from internet
resource "azurerm_sql_firewall_rule" "az_sql_firewall_rule" {
  name                = "sqlfirewallrule"
  resource_group_name = azurerm_resource_group.example.name
  server_name         = azurerm_mssql_server.az_sql_server.name
  start_ip_address    = "0.0.0.0"
  end_ip_address      = "255.255.255.255"
}

# To create a sql elastic pool
resource "azurerm_mssql_elasticpool" "ms_elastic_pool" {
  name                = "ogelasticpool"
  resource_group_name = azurerm_resource_group.example.name
  location            = azurerm_resource_group.example.location
  server_name         = azurerm_mssql_server.az_sql_server.name
  license_type        = "LicenseIncluded"
  max_size_gb         = 50

  sku {
    name     = "GP_Gen5"
    tier     = "GeneralPurpose"
    family   = "Gen5"
    capacity = 2
  }

  per_database_settings {
    min_capacity = 0.25
    max_capacity = 2
  }
}

# To create a database on the sql server instance
resource "azurerm_mssql_database" "database1" {
  name         = "db1"
  server_id    = azurerm_mssql_server.az_sql_server.id
  collation    = "SQL_Latin1_General_CP1_CI_AS"
  license_type = "LicenseIncluded"
  max_size_gb  = 4
  //read_scale     = true
  //sku_name       = "BC_Gen5_2"
  zone_redundant  = true
  sample_name     = "AdventureWorksLT"
  elastic_pool_id = azurerm_mssql_elasticpool.ms_elastic_pool.id

  tags = {
    environment = "development"
  }
}
/*
# To create a Private DNS Zone
resource "azurerm_private_dns_zone" "og-private-dns" {
  name                = "cloudutsuk.lan"
  resource_group_name = azurerm_resource_group.example.name
}

# To link the Private DNS Zone with the VNET
resource "azurerm_private_dns_zone_virtual_network_link" "og-private-dns-link" {
  name                  = "ogprvdnszonevnetlink"
  resource_group_name   = azurerm_resource_group.example.name
  private_dns_zone_name = azurerm_private_dns_zone.og-private-dns.name
  virtual_network_id    = azurerm_virtual_network.example.id
}
*/

# To create a Private DNS Zone for Azure SQL
resource "azurerm_private_dns_zone" "og-db-private-dns" {
  name                = var.az_prv_dns_zone_name
  resource_group_name = azurerm_resource_group.example.name
}

# To create an A Record for VM in dns zone
resource "azurerm_private_dns_a_record" "og-endpoint-dns-a-record" {
  depends_on          = [azurerm_windows_virtual_machine.example]
  name                = lower(azurerm_windows_virtual_machine.example.name)
  zone_name           = azurerm_private_dns_zone.og-db-private-dns.name
  resource_group_name = azurerm_resource_group.example.name
  ttl                 = 300
  records             = [azurerm_windows_virtual_machine.example.private_ip_address]
}

# To create a DB Private Endpoint
resource "azurerm_private_endpoint" "og-db-endpoint" {
  depends_on          = [azurerm_mssql_server.az_sql_server]
  name                = "og-sql-db-endpoint"
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name
  subnet_id           = azurerm_subnet.example.id
  # To integrate the endpoint with a private dns zone
  private_dns_zone_group {
    name = azurerm_private_dns_zone.og-db-private-dns.name
    private_dns_zone_ids = [azurerm_private_dns_zone.og-db-private-dns.id]
  }
  private_service_connection {
    name                           = "og-sql-db-endpoint"
    is_manual_connection           = "false"
    private_connection_resource_id = azurerm_mssql_server.az_sql_server.id
    subresource_names              = ["sqlServer"]
  }
}

# To create DB Private Endpoint Connecton
data "azurerm_private_endpoint_connection" "og-endpoint-connection" {
  depends_on          = [azurerm_private_endpoint.og-db-endpoint]
  name                = azurerm_private_endpoint.og-db-endpoint.name
  resource_group_name = azurerm_resource_group.example.name
}

# To create a DB Private DNS to VNET link
resource "azurerm_private_dns_zone_virtual_network_link" "db-dns-zone-to-vnet-link" {
  name = "sql-db-dns-zone-vnet-link"
  resource_group_name = azurerm_resource_group.example.name
  private_dns_zone_name = azurerm_private_dns_zone.og-db-private-dns.name
  virtual_network_id = azurerm_virtual_network.example.id
}