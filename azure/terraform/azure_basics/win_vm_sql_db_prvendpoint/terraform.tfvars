//Azure Credentials
azure_key_path       = "E:\\GitRepo\\automation\\common\\azure_terraform_user_key.pem"
sample_txt_file_path = "sample_txt_file.txt"


az_resource_group = "xor_sakharam"
az_location       = "East US"

az_server_name   = "azwindowsvm"
az_vnet_name     = "az_win_vnet"
az_subnet1_name  = "az_win_subnet"
az_vnet_nic_name = "az_win_nic"

az_vm_admin_username = "winadmin"
az_vm_admin_password = "P@ssword1!"

az_vnet_address    = ["10.1.0.0/16"]
az_subnet1_address = ["10.1.0.0/24"]


sql_username      = "sqladmin"
sql_password      = "P@ssword1!"
sql_server_name   = "ogmssqlserver"

az_prv_dns_zone_name = "privatelink.database.windows.net"