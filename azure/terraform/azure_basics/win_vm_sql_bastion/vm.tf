################################
# Data
################################

# To get the image id for creating a windows machine
# data "azurerm_platform_image" "windows" {
#   location  = var.az_location
#   publisher = var.publisher
#   offer     = var.offer
#   sku       = var.sku
#   version   = var.osversion
# }

// To source the template from a file
data "template_file" "windows" {
  template = file("${path.module}/files/winrm_config.ps1")
}

#####################################
# Resources
####################################

# To create a resource group
resource "azurerm_resource_group" "example" {
  name     = "${var.prefix}-${var.project}-${var.env}-rg"
  location = var.az_location
}

# To create a virtual network
resource "azurerm_virtual_network" "example" {
  name                = "${var.prefix}-${var.project}-${var.env}-vnet"
  address_space       = var.az_vnet_address
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name
}

# To create a subnet
resource "azurerm_subnet" "example" {
  name                 = "${var.prefix}-${var.project}-${var.env}-subnet1"
  resource_group_name  = azurerm_resource_group.example.name
  virtual_network_name = azurerm_virtual_network.example.name
  address_prefixes     = var.az_subnet1_address
}

# To create a network interface for sql vm instances
resource "azurerm_network_interface" "example" {
  count               = var.vm_instances_count
  name                = "${var.prefix}-${var.project}-${var.env}-nic-${count.index}"
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.example.id
    private_ip_address_allocation = "Dynamic"
  }

  ip_configuration {
    name                          = "internal2"
    subnet_id                     = azurerm_subnet.example.id
    private_ip_address_allocation = "Dynamic"
  }

  ip_configuration {
    name                          = "internal3"
    subnet_id                     = azurerm_subnet.example.id
    private_ip_address_allocation = "Dynamic"
  }
}

# To create a network interface for bastion host
resource "azurerm_network_interface" "bastion" {
  name                = "${var.prefix}-${var.project}-${var.env}-bastion-nic"
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.example.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.bastion.id
  }
}

# To create an public ip for the instance
resource "azurerm_public_ip" "bastion" {
  name                = "${var.prefix}-${var.project}-${var.env}-pip"
  resource_group_name = azurerm_resource_group.example.name
  location            = azurerm_resource_group.example.location
  allocation_method   = "Dynamic"
}

# To create a virtual machine for sql vm's
resource "azurerm_windows_virtual_machine" "example" {
  count               = var.vm_instances_count
  name                = "${var.prefix}-${var.project}-vm-${count.index}"
  resource_group_name = azurerm_resource_group.example.name
  location            = azurerm_resource_group.example.location
  size                = "Standard_F4"
  admin_username      = var.az_vm_admin_username
  admin_password      = var.az_vm_admin_password

  network_interface_ids = [
    azurerm_network_interface.example[count.index].id
  ]

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = var.publisher
    offer     = var.offer
    sku       = var.sku
    version   = var.osversion
  }
}

# To create a virtual machine for bastion
resource "azurerm_windows_virtual_machine" "bastion" {
  name                = "${var.prefix}-bastion"
  resource_group_name = azurerm_resource_group.example.name
  location            = azurerm_resource_group.example.location
  size                = "Standard_F4"
  admin_username      = var.az_vm_admin_username
  admin_password      = var.az_vm_admin_password

  network_interface_ids = [
    azurerm_network_interface.bastion.id,
  ]

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = var.publisher
    offer     = var.offer
    sku       = var.sku
    version   = var.osversion
  }
}


# Virtual Machine Extension to execute Powershell script
resource "azurerm_virtual_machine_extension" "example" {
  depends_on           = [azurerm_windows_virtual_machine.bastion]
  name                 = "${var.prefix}-${var.project}-${var.env}-vm-extension"
  virtual_machine_id   = azurerm_windows_virtual_machine.bastion.id
  publisher            = "Microsoft.Compute"
  type                 = "CustomScriptExtension"
  type_handler_version = "1.9"
  protected_settings   = <<SETTINGS
  {
    "commandToExecute": "powershell -command \"[System.Text.Encoding]::UTF8.GetString([System.Convert]::FromBase64String('${base64encode(data.template_file.windows.rendered)}')) | Out-File -filepath install.ps1\" && powershell -ExecutionPolicy Unrestricted -File install.ps1"
  } 
  SETTINGS
  tags = {
    environment = var.env
  }
}
