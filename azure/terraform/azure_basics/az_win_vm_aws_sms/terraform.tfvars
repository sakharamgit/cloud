//Azure Credentials
azure_key_path       = "E:\\GitRepo\\automation\\common\\azure_terraform_user_key.pem"
sample_txt_file_path = "sample_txt_file.txt"
subscription_id      = "ae23e82b-54bd-446e-871b-faa85c75f079"
az_location          = "East US"

winusername = "winadmin"
winpassword = "Password1!"

vnet_cidr_range    = ["10.1.0.0/16"]
subnet1_cidr_range = ["10.1.0.0/24"]

publisher = "MicrosoftSQLServer"
offer     = "SQL2016SP1-WS2016"
sku       = "Enterprise"
osversion = "13.2.210312"
