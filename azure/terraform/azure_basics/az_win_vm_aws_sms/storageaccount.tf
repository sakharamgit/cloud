
# To create a storage account
resource "azurerm_storage_account" "example" {
  name                     = "${var.prefix}${var.project}${var.env}${random_string.random.result}"
  resource_group_name      = azurerm_resource_group.example.name
  location                 = var.az_location
  account_tier             = "Standard"
  account_replication_type = "LRS"
  allow_blob_public_access = true
}

# To create a container in the storage account
resource "azurerm_storage_container" "example" {
  name                  = "${var.prefix}-${var.project}-${var.env}-container1"
  storage_account_name  = azurerm_storage_account.example.name
  container_access_type = "private"
}

# To create a blob in the container (Upload a file)
resource "azurerm_storage_blob" "example1" {
  name                   = "${var.prefix}-${var.project}-${var.env}-blob1"
  storage_account_name   = azurerm_storage_account.example.name
  storage_container_name = azurerm_storage_container.example.name
  type                   = "Block"
}