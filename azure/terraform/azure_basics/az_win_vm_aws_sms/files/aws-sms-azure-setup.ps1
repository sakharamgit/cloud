﻿# AWS Server Migration Service Deployment Script for Azure Connector.
# Copyright 2019, Amazon Web Services, Inc. or its affiliates. All rights reserved.

<#

.SYNOPSIS
This powershell script will deploy AWS SMS Connector VM for Azure.

.DESCRIPTION
This powershell script will deploy AWS SMS Connector VM for Azure. It takes two required parameters and two optional parameters.
- StorageAccountName - Required. This is the name of the (existing) storage account where you want the connector to be deployed. Read notes section before you choose one.
- ExistingVNetName - Required. Existing Virtual Network name where you want the Connector VM to be deployed.
- SubscriptionId - Optional. Id of the specific subscription to use. If not specified, the default Subscription for the account is used.
- SubnetName - Optional. Existing Subnet name in the provided Virtual Network above. If not specified, Subnet named "default" is used.

.EXAMPLE
./aws-sms-azure-setup.ps1 -StorageAccountName <StorageAccountName> -ExistingVNetName <VirtualNetworkName> -SubscriptionId <SubscriptionId> -SubnetName <SubnetName>

.LINK
https://docs.aws.amazon.com/server-migration-service/latest/userguide/Azure.html

.NOTES
The script uses Azure's new "Az" commandlets. This requires Powershell version >= 5.1. Az commandlets will be installed if necessary.

The Storage account that you choose is used to store the Connector related artifacts (Disk Image, etc..) and some transient
SMS related artifacts during the migration. It is recommended that you create a separate storage account so that all the connector resources
are isolated and easily managed.

Whether you are creating a new one or using an existing storage account, make sure you choose the
one that is located (primary location) in the same region as all the VMs that you want to migrate. This is because a single
Connector can only migrate VMs from a single subscription and a location pair. Only Azure Resource Manager (ARM) deployed VMs can
be migrated by the Connector.

If the Storage account is not under the default subscription, please specify the SubscriptionId under which the Storage account exists.
All Connector related resources will be created under this subscription.

The deployed Connector will only have inbound access from within its own Virtual Network (ARM deployed default VM configuration).
The Connector VM will be setup with a NIC/NSG with default security rules which will let the Connector work out of the box.
However, if the chosen subnet has a network security group attached, then this can impact/override the default security rules.
You will need to ensure that the Connector VM has inbound HTTPS (443) access from a VM from within its subnet to complete the registration.
You also need to ensure that the Connector VM has outbound internet connectivity to be able to reach AWS and Azure services.

You can further configure network firewall settings from Azure Portal by clicking on the Network Settings section of the Connector
VM once it is deployed.

If you run into any issue, please contact aws-server-migration-azure-support@amazon.com

#>

#################### SCRIPT PARAMETERS ########################

param(
    [parameter(Mandatory=$true, HelpMessage="This is the name of the existing storage account where you want the connector to be deployed.")] [ValidateNotNullOrEmpty()] [String] $StorageAccountName,
    [parameter(Mandatory=$true, HelpMessage="Existing Virtual Network name where you want the Connector VM to be deployed.")] [ValidateNotNullOrEmpty()] [String] $ExistingVNetName,
    [parameter(Mandatory=$true, HelpMessage="Id of the specific subscription to use. If not specified, the default Subscription for the account is used.")] [AllowEmptyString()] [String] $SubscriptionId,
    [parameter(Mandatory=$true, HelpMessage="Existing Subnet name in the provided Virtual Network above. If not specified, Subnet named 'default' is used.")] [AllowEmptyString()] [String] $SubnetName
)

#################### SCRIPT GLOBALS ########################
$Global:AWS_AZURE_SMS_CONFIG_SCRIPT_VERSION = "1.2.0.3"
$Global:AWS_AZURE_SMS_CONNECTOR_DEFAULT_VERSION = "1.2.0.1"
$Global:POWERSHELL_MIN_VERSION = "5.1"
$Global:AZ_MODULE_MIN_VERSION = "1.1.0"
$Global:ErrorActionPreference = "Stop"
$Global:WarningPreference = "Continue"

$Global:DEPLOYMENT_STAGE_INITIALIZE = "Initialize"
$Global:DEPLOYMENT_STAGE_RG_CREATE = "ResourceGroup_Create"
$Global:DEPLOYMENT_STAGE_BLOB_COPY = "Blob_Copy"
$Global:DEPLOYMENT_STAGE_BLOB_VERIFY = "Blob_Verify"
$Global:DEPLOYMENT_STAGE_DISK_CREATE = "Disk_Create"
$Global:DEPLOYMENT_STAGE_VM_CREATE = "VM_Create"
$Global:DEPLOYMENT_STAGE_POST_CONFIG = "Post_Config"
$Global:DEPLOYMENT_STAGE_COMPLETE = "Complete"

$Global:DEPLOYMENT_BLOB_NAME = "sms-deployment-tracker-donot-delete"
$Global:TRANSCRIPT_FILE_NAME = "sms-deployment-transcript.txt"

#################### DEPLOYMENT LOGIC CORE FUNCTIONS ########################

# Function to display help text about script parameters before proceeding
# It also retrieves and caches the necessary data to execute this script
Function Initialize
{
  Write-Status "Performing Validation and Initialization"
  Try {
    # Check if Storage Account exists/accessible
    Write-Status "Checking if Storage Account exists and is accessible by the logged in user"
    $SAObj = (Get-AzStorageAccount | Where-Object {$_.StorageAccountName -eq "$StorageAccountName"}) | GetNonBlankValue -Field "StorageAccount"
    $LocationName = $SAObj.PrimaryLocation
    if ($LocationName -eq "") {
        $LocationName = $SAObj.Location
    }
    $RgName = $SAObj.ResourceGroupName
    $StorageContext = Get-Storage-Context -SAName $StorageAccountName -RgName $RgName

    # Check if Storage Account is Standard and General Purpose
    if (-Not $SAObj.Kind.toString().StartsWith("Storage")) {
        Write-Failure "The Storage Account needs to be of type Standard General Purpose."
    }
    if (-Not $SaObj.Sku.Tier.ToString().StartsWith("Standard")) {
        Write-Failure "The Storage Account needs to be of type Standard General Purpose."
    }

    # Get Subscription Details
    Write-Status "Getting subscription details"
    $RgObj = (Get-AzResourceGroup -Name $RgName -Location $LocationName) | GetNonBlankValue -Field "ResourceGroup"
    $SubId = $RgObj.ResourceId.Split("/")[2]
    $SubObj = Select-AzSubscription -SubscriptionId $SubId

    # Check if VNet exists
    Write-Status "Checking if $ExistingVNetName exists"
    $VnetObj = (Get-AzVirtualNetwork | Where-Object {$_.Name -eq "$ExistingVNetName"}) | GetNonBlankValue -Field "VirtualNetwork"
    if ($VnetObj.count -gt 1) {
        Write-Warning "There are multiple Virtual Networks found under the same name $ExistingVNetName. Here is the list."
        Write-Info ($VnetObj | Select-Object Name, ResourceGroupName)
        $VnetRgName = Read-Host "Enter the resource group name corresponding to the Virtual Network you would like to use: "
        $VnetObj = (Get-AzVirtualNetwork -Name $ExistingVNetName -ResourceGroupName $VnetRgName) | GetNonBlankValue -Field "VirtualNetwork"
    } else {
        $VnetRgName = $VnetObj.ResourceGroupName
    }

    # Check VNet location
    if ($VnetObj.Location -ne $LocationName) {
        Write-Failure "The Virtual Network Location does not match. It needs to be in $LocationName."
    }

    # Check if Subnet exists
    if (($SubnetName -eq $null) -Or ($SubnetName -eq "")) {
        $SubnetName = "default"
    }
    $SubnetObj = (Get-AzVirtualNetworkSubnetConfig -Name $SubnetName -VirtualNetwork $VnetObj) | GetNonBlankValue -Field "Subnet"

    # Collect all resource names into DeploymentTracker
    Write-Status "Computing resource names"
    $RgIdx = ""
    $FoundNewName = $false
    Do {
        # Resource Group is special and needs to handle indexing
        # for deploying multiple connectors in the same location
        $RGroupName = Derive-Resource-Name -ResourceType "rg" -Location $LocationName -Index $RgIdx
        $ExistingRgObj = Get-AzResourceGroup -Name $RGroupName -ErrorAction Ignore
        if ($ExistingRgObj -eq $null) {
            $FoundNewName = $true
        } else {
            if ($RgIdx -eq "") {
                $RgIdx = 1
            } else {
                $RgIdx++
            }
        }
    } While ($FoundNewName -eq $false)

    # Get Connector Version
    Try {
        $TempFile = New-TemporaryFile
        $VersionBlobUri =  Get-Location-Based-Version-BlobUri
        Invoke-WebRequest -Uri $VersionBlobUri -OutFile $TempFile.FullName
        $ConnectorVersion = Get-Content -Path $TempFile.FullName
        Write-Success "Obtained Connector Version: $ConnectorVersion"
        $Removed = Remove-Item -Path $TempFile.FullName -ErrorAction Ignore -Force
    } Catch {
        $ConnectorVersion = $Global:AWS_AZURE_SMS_CONNECTOR_DEFAULT_VERSION
        Write-Warning "Could not read Connector Version. Will name blob with Default Version: $ConnectorVersion"
    }

    $VMName       = Derive-Resource-Name -ResourceType "vm" -Location $LocationName
    $DiskName     = Derive-Resource-Name -ResourceType "disk" -Location $LocationName
    $BlobName     = Derive-Resource-Name -ResourceType "blob" -Index $ConnectorVersion -Location $LocationName
    $ContainerName= Derive-Resource-Name -ResourceType "container" -Location $LocationName
    $NSGName      = Derive-Resource-Name -ResourceType "nsg" -Location $LocationName
    $NICName      = Derive-Resource-Name -ResourceType "nic" -Location $LocationName
    $PIPName      = Derive-Resource-Name -ResourceType "pip" -Location $LocationName
    $RoleName     = Derive-Resource-Name -ResourceType "role" -Location $SubId
    $RoleSAName   = Derive-Resource-Name -ResourceType "role" -Location $StorageAccountName
    $VMTag        = Derive-Resource-Name -ResourceType "tag" -Location $LocationName
    $VMSize       = Get-Location-Based-VM-Size $LocationName

    $DeploymentTracker = New-Object -TypeName PSObject
    Add-Member -InputObject $DeploymentTracker -MemberType NoteProperty -Name "SubscriptionId" -Value $SubId
    Add-Member -InputObject $DeploymentTracker -MemberType NoteProperty -Name "ResourceGroupName" -Value $RGroupName
    Add-Member -InputObject $DeploymentTracker -MemberType NoteProperty -Name "StorageAccountName" -Value $StorageAccountName
    Add-Member -InputObject $DeploymentTracker -MemberType NoteProperty -Name "StorageAccountResourceGroupName" -Value $RgName
    Add-Member -InputObject $DeploymentTracker -MemberType NoteProperty -Name "LocationName" -Value $LocationName
    Add-Member -InputObject $DeploymentTracker -MemberType NoteProperty -Name "ContainerName" -Value $ContainerName
    Add-Member -InputObject $DeploymentTracker -MemberType NoteProperty -Name "BlobName" -Value $BlobName
    Add-Member -InputObject $DeploymentTracker -MemberType NoteProperty -Name "DiskName" -Value $DiskName
    Add-Member -InputObject $DeploymentTracker -MemberType NoteProperty -Name "RoleName" -Value $RoleName
    Add-Member -InputObject $DeploymentTracker -MemberType NoteProperty -Name "RoleSAName" -Value $RoleSAName
    Add-Member -InputObject $DeploymentTracker -MemberType NoteProperty -Name "ExistingVirtualNetworkName" -Value $ExistingVNetName
    Add-Member -InputObject $DeploymentTracker -MemberType NoteProperty -Name "VirtualNetworkResourceGroupName" -Value $VnetRgName
    Add-Member -InputObject $DeploymentTracker -MemberType NoteProperty -Name "SubnetName" -Value $SubnetName
    Add-Member -InputObject $DeploymentTracker -MemberType NoteProperty -Name "NICName" -Value $NICName
    Add-Member -InputObject $DeploymentTracker -MemberType NoteProperty -Name "NSGName" -Value $NSGName
    Add-Member -InputObject $DeploymentTracker -MemberType NoteProperty -Name "PIPName" -Value $PIPName
    Add-Member -InputObject $DeploymentTracker -MemberType NoteProperty -Name "VirtualMachineName" -Value $VMName
    Add-Member -InputObject $DeploymentTracker -MemberType NoteProperty -Name "VirtualMachineSize" -Value $VMSize
    Add-Member -InputObject $DeploymentTracker -MemberType NoteProperty -Name "VirtualMachineTagKey" -Value $VMTag
    Add-Member -InputObject $DeploymentTracker -MemberType NoteProperty -Name "Stage" -Value $Global:DEPLOYMENT_STAGE_INITIALIZE

    # Display Input summary. Get User Consent
    Write-Text ""
    Write-Warning "Please read the following carefully before proceeding"
    Write-Info "Storage Account ""$StorageAccountName"" resides in Azure Location ""$LocationName""."
    Write-Info "The Connector and its storage resources will be deployed into this account."
    Write-Info "This means the Connector being deployed can only migrate VMs in ""$LocationName"" and under the subscription ""$SubId""."
    Write-Info "The Connector VM access is restricted and its NIC is configured with the a Security Group with default rules."
    Write-Info "Connector VM will be configured to have Inbound SSH/HTTPS access only from within its Virtual Network ($ExistingVNetName)."
    Write-Info "Connector will have Internet Outbound access enabled in order to access AWS services."
    Write-Info "If the Subnet's Network Security Group overrides the default security rules, then rule changes may be required to satisfy the above."
    Write-Info "Following the deployment, you need to register the connector."
    Write-Info "In order to register, please use a machine from ""$ExistingVNetName"" to access https://<ConnectorPrivateIp> from its browser."

    Write-Text ""
    Write-Info "The following resources will be created under the subscription: $SubId"
    Write-Text ($DeploymentTracker | Format-List | Out-String)
    # $Conf = Read-Host "Would you like to proceed with the deployment (y/n)? "
    # if ($Conf -ne 'y') {
    #   Exit-Application
    # }

    Write-Success "Validation and Initialization Complete"

    Transition-To-Next-Deployment-Stage $DeploymentTracker

  } Catch {
    Write-Failure -Message "Error initializing" -Exception $_.Exception
  }
}

# Function to create a new resource group where all connector related
# resources will be grouped under.
Function ResourceGroup-Create
{
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [PSObject]$DeploymentTracker
    )

    Write-Status "Creating Resource Group"

    Try {
        $RgObj = Get-AzResourceGroup -Name $DeploymentTracker.ResourceGroupName -ErrorAction Ignore
        if ($RgObj -ne $nusll) {
            Write-Success "Resource Group $($DeploymentTracker.ResourceGroupName) already exists. Skipping creation."
            Transition-To-Next-Deployment-Stage $DeploymentTracker
            Return
        }
        $RgObj = New-AzResourceGroup -Name $DeploymentTracker.ResourceGroupName -Location $DeploymentTracker.LocationName
        Write-Success "Resource Group Created"
        Transition-To-Next-Deployment-Stage $DeploymentTracker
    } Catch {
        Write-Failure "Error initializing" $_.Exception
    }
}

# Function to download the connector disk image to customer's account (blob-blob copy)
Function Blob-Copy
{
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [PSObject]$DeploymentTracker
    )

    Write-Status "Copying Connector Disk Image Blob to $($DeploymentTracker.StorageAccountName)"
    Try {
        $StorageContext = Get-Storage-Context -SAName $DeploymentTracker.StorageAccountName -RgName $DeploymentTracker.StorageAccountResourceGroupName
        Write-Status "Creating Container if needed"
        Try {
            $ContainerObj = Get-AzStorageContainer -Name $DeploymentTracker.ContainerName -Context $StorageContext
        } Catch {
            $ContainerObj = New-AzStorageContainer -Name $DeploymentTracker.ContainerName -Context $StorageContext -Permission Off
        }

        Write-Status "Checking previously initiated Blob Copy"
        Try {
            $Blob = Get-AzStorageBlob -Blob $DeploymentTracker.BlobName -Container $ContainerObj.Name -Context $StorageContext
            Write-Success "Previously Intiated BlobCopy found"
        } Catch {
            $SrcUri = Get-Location-Based-Disk-Image-BlobUri $DeploymentTracker
            Write-Status "Initiating Blob Copy from ""$SrcUri"""
            $Blob = Start-AzStorageBlobCopy -DestBlob $DeploymentTracker.BlobName -DestContainer $ContainerObj.Name -AbsoluteUri $SrcUri -DestContext $StorageContext
        }

        Write-Status "Waiting for Blob Copy to Finish"
        Do {
            $Blob = Get-AzStorageBlob -Blob $DeploymentTracker.BlobName -Container $ContainerObj.Name -Context $StorageContext
            $CopyStatus = $Blob.ICloudBlob.CopyState.Status
            $CopiedBytes = $Blob.ICloudBlob.CopyState.BytesCopied
            $TotalBytes = $Blob.ICloudBlob.CopyState.TotalBytes
            if ($TotalBytes -gt 0) {
                $Progress = ($CopiedBytes / $TotalBytes) * 100
                $Progress = [int]$Progress
            }
            Write-Status "CopyState: $CopyStatus; Bytes Copied: $CopiedBytes; Progress: $Progress%"
            if ($CopyStatus -like "pending") {
                Start-Sleep -Seconds 30
            }
        } While ($CopyStatus -like "pending")

        if (($CopyStatus -like "complete") -Or ($CopyStatus -like "success")) {
            Write-Success "Blob Copy Complete"
            Transition-To-Next-Deployment-Stage $DeploymentTracker
        } else {
            Write-Failure -Message "Blob Copy did not complete. Last Copy Status: $CopyStatus"
        }

    } Catch {
        Write-Failure -Message "Error while copying connector blob" -Exception $_.Exception
    }
}

# Function to verify checksums of downloaded blob
Function Blob-Verify
{
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [PSObject]$DeploymentTracker
    )
    Write-Status "Verifying Blob for Integrity"
<#
    Try {
        # Get access to Copied blob
        $StorageContext = Get-Storage-Context -SAName $DeploymentTracker.StorageAccountName -RgName $DeploymentTracker.StorageAccountResourceGroupName
        $Blob = Get-AzStorageBlob -Blob $DeploymentTracker.BlobName -Container $DeploymentTracker.ContainerName -Context $StorageContext
        # Give it url access for Putblock api (will be revoked after expiration)
        $StartTime = (Get-Date)
        $EndTime = $StartTime.AddMinutes(30)
        $SASUri = New-AzStorageBlobSASToken -Container $DeploymentTracker.ContainerName -Blob $DeploymentTracker.BlobName -Context $StorageContext -Permission r -Protocol HttpsOnly -FullUri -StartTime $StartTime -ExpiryTime $EndTime
        $ChunkSize = (100L * 1024L * 1024L)

        # Download source checksums into ArrayList
        $TempFile1 = New-TemporaryFile
        $S3ChecksumListUrl = "https://s3.amazonaws.com/sms-connector/AWS-SMS-Connector-for-Azure.vhd.md5list"
        $Req = Invoke-WebRequest -Uri $S3ChecksumListUrl -OutFile $TempFile1.FullName
        $SourceChecksums = Get-Content $TempFile1.FullName
        $RemoveTempFile = Remove-Item $TempFile1.FullName -ErrorAction Ignore

        # Setup Temporary block blob
        $TempFile2 = New-TemporaryFile
        "Calculating Checksum" | Out-File $TempFile2.FullName
        $BlockBlobName = "ChecksumCalculationBlob"
        $TempBlockBlob = Set-AzStorageBlobContent -Context $StorageContext -File $TempFile2.FullName -Container $DeploymentTracker.ContainerName -Blob $BlockBlobName -BlobType Block -Force

        $BlobSize = $Blob.Length
        $MD5Zero100MB = "LygrhOfmCNWFJEntlAv8UQ=="

        $Bytes = [System.Text.Encoding]::UTF8.GetBytes("BlockId-1")
        $BlockId = [System.Convert]::ToBase64String($Bytes);

        $RemainingLen = $BlobSize
        $SrcOffset = 0L
        $Tasks = New-Object -Type System.Collections.ArrayList
        $Retries = 5
        while ($RemainingLen -gt 0) {
            $TryCount = 0
            while ($TryCount -lt $Retries) {
                Try {
                    $Task = $TempBlockBlob.ICloudBlob.PutBlockAsync($BlockId, $SASUri, $SrcOffset, $ChunkSize, $MD5Zero100MB)
                    break
                } Catch {
                    Start-Sleep -Seconds 1
                    $TryCount++
                }
            }
            $Added = $Tasks.Add($Task)
            $SrcOffset += $ChunkSize
            $RemainingLen -= $ChunkSize
        }

        # Check if total chunks match
        if ($Tasks.count -ne $SourceChecksums.Count) {
            $RemoveTempFile = Remove-Item $TempFile2 -ErrorAction Ignore
            $RemoveBlockBlob = Remove-AzStorageBlob -Blob $BlockBlobName -Container $DeploymentTracker.ContainerName -Context $StorageContext -Force -ErrorAction Ignore
            Write-Failure "Blob Verification Failed. The downloaded blob failed the integrity check. Size mismatch."
        }

        # Check if individual chunk checksums match
        $WaitTimeMs = 30000
        for ($i = 0; $i -lt $Tasks.Count; $i++) {
            $Task = $Tasks[$i]
            try {
                $w = $Task.Wait($WaitTimeMs)
            } Catch {
            }

            if ($Task.IsFaulted -eq $true) {
                $TryCount = 0
                $CorrectCksum = ""
                while ($TryCount -lt $Retries) {
                    if ($Task.Exception.InnerException.RequestInformation.ErrorCode -like "Md5Mismatch") {
                        $CorrectCksum = $task.Exception.InnerException.RequestInformation.ExtendedErrorInformation.AdditionalDetails.ServerCalculatedMd5
                        break
                    }
                    # A small number of tasks can fail with Storage Exception (due to throttling) and needs to be retried
                    Start-Sleep -Seconds 1
                    $Task = $TempBlockBlob.ICloudBlob.PutBlockAsync($BlockId, $SASUri, ($i * $ChunkSize), $ChunkSize, $MD5Zero100MB)
                    try {
                        $w = $Task.Wait($WaitTimeMs)
                    } Catch {
                    }
                    $TryCount++
                }
            } else {
                $CorrectCksum = $MD5Zero100MB
            }

            if ($CorrectCksum -ne $SourceChecksums[$i]) {
                $RemoveTempFile = Remove-Item $TempFile2 -ErrorAction Ignore
                $RemoveBlockBlob = Remove-AzStorageBlob -Blob $BlockBlobName -Container $DeploymentTracker.ContainerName -Context $StorageContext -Force -ErrorAction Ignore
                Write-Failure "Blob Verification Failed ($i). The downloaded blob failed the integrity check."
            }
        }

        # Cleanup
        $RemoveTempFile = Remove-Item $TempFile2 -ErrorAction Ignore
        $RemoveBlockBlob = Remove-AzStorageBlob -Blob $BlockBlobName -Container $DeploymentTracker.ContainerName -Context $StorageContext -Force -ErrorAction Ignore

        Write-Success "Blob Integrity Check Succeeded!"
        Transition-To-Next-Deployment-Stage $DeploymentTracker

    } Catch {
        Write-Failure -Message "Error while checking for Blob Integrity: " -Exception $_.Exception
    }
#>
    Write-Success "Blob Integrity Check Succeeded!"
    Transition-To-Next-Deployment-Stage $DeploymentTracker
}

# Function to Create Disk from Disk Blob
Function Disk-Create
{
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [PSObject]$DeploymentTracker
    )

    Write-Status "Creating Disk from Copied DiskBlob. This can take a while."

    Try {
        $ExistingDisk = Get-AzDisk -ResourceGroupName $DeploymentTracker.ResourceGroupName -DiskName $DeploymentTracker.DiskName -ErrorAction Ignore
        if ($ExistingDisk -ne $null) {
            Write-Success "Disk $($DeploymentTracker.DiskName) already exists. Skipping creation."
            Transition-To-Next-Deployment-Stage $DeploymentTracker
            Return
        }

        $ContainerName = $DeploymentTracker.ContainerName
        $BlobName = $DeploymentTracker.BlobName
        $StorageAccountName = $DeploymentTracker.StorageAccountName
        $BlobUri="https://$StorageAccountName.blob.core.windows.net/$ContainerName/$BlobName"
        $StorageContext = Get-Storage-Context -SAName $StorageAccountName -RgName $DeploymentTracker.StorageAccountResourceGroupName
        $Blob = Get-AzStorageBlob -Blob $BlobName -Container $ContainerName -Context $StorageContext
        $BlobSizeInGB = [int]($Blob.Length / 1024 / 1024 / 1024)

        # Handle breaking Change in New-AzDiskConfig - https://docs.microsoft.com/en-us/powershell/azure/release-notes-azureps?view=azps-3.0.0
        Try {
            $AzModule = Get-InstalledModule -Name Az -MinimumVersion 3.0.0
        } Catch {
        }
        if ($AzModule -eq $null) {
            # Az Version is < 3.0.0 - Does not need storageAccountId
            $DiskConfig = New-AzDiskConfig -SkuName Standard_LRS -OsType Linux -DiskSizeGB $BlobSizeInGB -Location $DeploymentTracker.LocationName -SourceUri $BlobUri -CreateOption Import
        } else {
            # Az Version is >= 3.0.0 - Needs storageAccountId
            $SAObj = (Get-AzStorageAccount | Where-Object {$_.StorageAccountName -eq "$StorageAccountName"}) | GetNonBlankValue -Field "StorageAccount"
            $DiskConfig = New-AzDiskConfig -SkuName Standard_LRS -OsType Linux -DiskSizeGB $BlobSizeInGB -Location $DeploymentTracker.LocationName -SourceUri $BlobUri -CreateOption Import -StorageAccountId $SAObj.Id
        }

        $Disk = New-AzDisk -ResourceGroupName $DeploymentTracker.ResourceGroupName -DiskName $DeploymentTracker.DiskName -Disk $DiskConfig
        Write-Success "Disk Creation Complete"
        Transition-To-Next-Deployment-Stage $DeploymentTracker
    } Catch {
        Write-Failure -Message "Error while creating Disk" -Exception $_.Exception
    }
}

# Function to create Connector VM from Managed Disk
Function VirtualMachine-Create
{
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [PSObject]$DeploymentTracker
    )

    Write-Status "Creating Connector Virtual Machine. This can take a while."

    Try {
        $ExistingVM = Get-AzVM -ResourceGroupName $DeploymentTracker.ResourceGroupName -Name $DeploymentTracker.VirtualMachineName -ErrorAction Ignore
        if ($ExistingVM -ne $null) {
            Write-Success "Virtual Machine $($DeploymentTracker.VirtualMachineName) is already created. Skipping creation."
            Transition-To-Next-Deployment-Stage $DeploymentTracker
            Return
        }


        $RgName = $DeploymentTracker.ResourceGroupName
        $LocationName = $DeploymentTracker.LocationName
        $VnetName = $DeploymentTracker.ExistingVirtualNetworkName
        $Disk = Get-AzDisk -ResourceGroupName $RgName -DiskName $DeploymentTracker.DiskName

        # Initialize VMConfig
        $VMConfig = New-AzVMConfig -VMName $DeploymentTracker.VirtualMachineName -VMSize $DeploymentTracker.VirtualMachineSize -IdentityType SystemAssigned
        $VMConfig.Location = $DeploymentTracker.LocationName

        # Configure OS disk to be attached
        $VMConfig = Set-AzVMOSDisk -VM $VMConfig -ManagedDiskId $Disk.Id -CreateOption Attach -Linux

        # Enable Boot Diagnostics
        # Handle Change in Cmdlet name - https://docs.microsoft.com/en-us/powershell/azure/release-notes-azureps?view=azps-2.0.0
        Try {
            $AzModule = Get-InstalledModule -Name Az -MinimumVersion 2.0.0
        } Catch {
        }
        if ($AzModule -eq $null) {
            # Az Version is < 2.0.0 - Use "Set-AzVMBootDiagnostics"
            $VMConfig = Set-AzVMBootDiagnostics -VM $VMConfig -Enable -StorageAccountName $DeploymentTracker.StorageAccountName -ResourceGroupName $DeploymentTracker.StorageAccountResourceGroupName
        } else {
            # Az Version is >= 2.0.0 - Use "Set-AzVMBootDiagnostic"
            $VMConfig = Set-AzVMBootDiagnostic -VM $VMConfig -Enable -StorageAccountName $DeploymentTracker.StorageAccountName -ResourceGroupName $DeploymentTracker.StorageAccountResourceGroupName
        }

        # Configure Network settings
        $Vnet = Get-AzVirtualNetwork -Name $VnetName -ResourceGroupName $DeploymentTracker.VirtualNetworkResourceGroupName
        $Subnet = Get-AzVirtualNetworkSubnetConfig -Name $DeploymentTracker.SubnetName -VirtualNetwork $Vnet

        # Create NSG
        $ExistingNSG = Get-AzNetworkSecurityGroup -ResourceGroupName $RgName -Name $DeploymentTracker.NSGName -ErrorAction Ignore
        if ($ExistingNSG -eq $null) {
            $NSG = New-AzNetworkSecurityGroup -ResourceGroupName $RgName -Location $LocationName -Name $DeploymentTracker.NSGName
            Write-Success "Created Network Security Group $($NSG.Name)"
        } else {
            $NSG = $ExistingNSG
            Write-Success "Network Security Group $($ExistingNSG.Name) already exists. Skipping creation."
        }

        # Create PIP
        $ExistingPIP = Get-AzPublicIpAddress -ResourceGroupName $RgName -Name $DeploymentTracker.PIPName -ErrorAction Ignore
        if ($ExistingPIP -eq $null) {
            $PIP = New-AzPublicIpAddress -Name $DeploymentTracker.PIPName -ResourceGroupName $RgName -Location $LocationName -AllocationMethod Dynamic
            Write-Success "Created Public Ip $($PIP.Name)"
        } else {
            $PIP = $ExistingPIP
            Write-Success "Public Ip $($ExistingPIP.Name) already exists. Skipping creation."
        }

        # Create NIC
        $ExistingNIC = Get-AzNetworkInterface -ResourceGroupName $RgName -Name $DeploymentTracker.NICName -ErrorAction Ignore
        if ($ExistingNIC -eq $null) {
            $NIC = New-AzNetworkInterface -Name $DeploymentTracker.NICName -ResourceGroupName $RgName -Location $LocationName `
                    -SubnetId $Subnet.Id -NetworkSecurityGroupId $NSG.Id -PublicIpAddressId $PIP.Id
            Write-Success "Created NIC $($NIC.Name)"
        } else {
            $NIC = $ExistingNIC
            Write-Success "NIC $($ExistingNIC.Name) already exists. Skipping creation."
        }
        $VMConfig = Add-AzVMNetworkInterface -VM $VMConfig -Id $NIC.Id

        Write-Info "Creating VM with the following parameters"
        Write-Text ($VMConfig | Format-Table | Out-String)

        # Tag to identify Storage account from within the Connector
        $SAcct = $DeploymentTracker.StorageAccountName
        $SRg = $DeploymentTracker.StorageAccountResourceGroupName
        $VMTagValue = "$SAcct|$SRg"
        $VMTag = @{"$($DeploymentTracker.VirtualMachineTagKey)" = "$VMTagValue";}

        # Create VM with the Configuration
        $VM = New-AzVM -VM $VMConfig -ResourceGroupName $RgName -Location $LocationName # -Tag $VMTag

        Write-Success "Virtual Machine Creation Complete"
        Transition-To-Next-Deployment-Stage $DeploymentTracker
    } Catch {
        Write-Failure -Message "Error while creating VirtualMachine" -Exception $_.Exception
    }
}

# Configure VM with Identity, NSG, etc..
Function Post-Config
{
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [PSObject]$DeploymentTracker
    )

    Write-Status "Performing Post Config"
    Try {

        Write-Status "Creating Custom Roles for Connector"
        $SubscriptionScope = Create-Connector-Role $DeploymentTracker
        $StorageScope = Create-Connector-Storage-Role $DeploymentTracker
        $VM = Get-AzVM -Name $DeploymentTracker.VirtualMachineName -ResourceGroupName $DeploymentTracker.ResourceGroupName

        # Assign Subscription Scoped role to System Identity
        $ExistingAssignment = Get-AzRoleAssignment -ObjectId $VM.Identity.PrincipalId -RoleDefinitionName $DeploymentTracker.RoleName -ErrorAction Ignore
        if ($ExistingAssignment -eq $null) {
            $Assignment1 = New-AzRoleAssignment -ObjectId $VM.Identity.PrincipalId -RoleDefinitionName $DeploymentTracker.RoleName -Scope $SubscriptionScope
            Write-Success "$($DeploymentTracker.RoleName) successfully assigned to Connector VM's System Identity"
        } else {
            Write-Success "$($DeploymentTracker.RoleName) already assigned to Connector VM's System Identity"
        }

        # Assign Storage Scoped role to System Identity
        $ExistingAssignment = Get-AzRoleAssignment -ObjectId $VM.Identity.PrincipalId -RoleDefinitionName $DeploymentTracker.RoleSAName -ErrorAction Ignore
        if ($ExistingAssignment -eq $null) {
            $Assignment2 = New-AzRoleAssignment -ObjectId $VM.Identity.PrincipalId -RoleDefinitionName $DeploymentTracker.RoleSAName -Scope $StorageScope
            Write-Success "$($DeploymentTracker.RoleSAName) successfully assigned to Connector VM's System Identity"
        } else {
            Write-Success "$($DeploymentTracker.RoleSAName) already assigned to Connector VM's System Identity"
        }

        # Print Connector Details
        $Ni = Get-AzNetworkInterface | where {$_.Id -eq $VM.NetworkProfile.NetworkInterfaces[0].Id}
        $Ip = $Ni.IpConfigurations.PrivateIpAddress
        $VnetName = $DeploymentTracker.ExistingVirtualNetworkName
        $Vnet = Get-AzVirtualNetwork -Name $VnetName -ResourceGroupName $DeploymentTracker.VirtualNetworkResourceGroupName
        $Subnet = Get-AzVirtualNetworkSubnetConfig -Name $DeploymentTracker.SubnetName -VirtualNetwork $Vnet

        Write-Success "Post Config is Complete"
        Write-Text
        Write-Text "*******************************************************************************************"
        Write-Info "Connector VM Name: ""$($VM.Name)"" (Resource Group: ""$($VM.ResourceGroupName)"")"
        Write-Info "Connector Private IP: $Ip"
        Write-Info "Object Id of System Assigned Identity for Connector VM: $($VM.Identity.PrincipalId)"
        if ($Subnet.NetworkSecurityGroup -ne $null) {
            Write-Warning -Message "Subnet $($DeploymentTracker.SubnetName) under Virtual Network $VNetName has a Network Security Group attached!"
            Write-Info -Message "This may affect the reachability and Internet connectivity of the Connector VM."
            Write-Info -Message "Please ensure Connector VM has inbound HTTPS (Port 443) access from within its Subnet and Outbound Internet Connectivity for AWS/Azure access."
        }
        Write-Info "From a VM in VNet: $VnetName, Goto https://$Ip"
        Write-Info "Complete Connector Registration. Use the above Object Id at the last step of Registration."
        Write-Text "*******************************************************************************************"
        Write-Text
        Write-Text

        Transition-To-Next-Deployment-Stage $DeploymentTracker

    } Catch {
        Write-Failure -Message "Error while doing post configuration" -Exception $_.Exception
    }
}

# Complete and Cleanup deployment
Function Complete-Deployment
{
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [PSObject]$DeploymentTracker
    )

    Write-Status "Completing Deployment"
    Delete-DeploymentTracker $DeploymentTracker
    Write-Success "Deployment Complete"
}

####################### Azure Utilities ############################
Function Login
{
    Try {
        if (($SubscriptionId -eq $null) -Or ($SubscriptionId -eq "")) {
            $Login = Connect-AzAccount
        } else {
            $Login = Connect-AzAccount -Subscription $SubscriptionId
        }
        Write-Info "Logged into Account $($Login.Context.Account.Id) under Subscription $($Login.Context.Subscription.Id)"
        Write-Success "Login Successful!"
    } Catch {
        Write-Failure -Message "Could not login to Azure account" -Exception $_.Exception
    }
}

Function Create-Connector-Role
{
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [PSObject]$DeploymentTracker
    )

    $RoleName = $DeploymentTracker.RoleName
    Write-Status "Creating Connector Role $RoleName"

    Try {
        $ExistingRole = Get-AzRoleDefinition -Name $RoleName -ErrorAction Ignore
        if ($ExistingRole -ne $null) {
            Write-Success "Connector Role $RoleName already exists. Skipping Creation."
            Return $ExistingRole.AssignableScopes
        }

        $Role = Get-AzRoleDefinition "Virtual Machine Contributor"
        $Role.Id = $null
        $Role.Name = $RoleName
        $Role.Description = "Perform AWS Server Migration Service related connector tasks on behalf of the customer. This lets Connector create snapshots of Virtual Machines, Read list of virtual machines, etc."
        $Role.Actions.Clear()
        $Role.Actions.Add("Microsoft.Compute/virtualMachines/read")
        $Role.Actions.Add("Microsoft.Network/networkInterfaces/read")
        $Role.Actions.Add("Microsoft.Compute/operations/read")
        $Role.Actions.Add("Microsoft.Compute/disks/read")
        $Role.Actions.Add("Microsoft.Compute/disks/beginGetAccess/action")
        $Role.Actions.Add("Microsoft.Compute/disks/endGetAccess/action")
        $Role.Actions.Add("Microsoft.Compute/snapshots/read")
        $Role.Actions.Add("Microsoft.Compute/snapshots/write")
        $Role.Actions.Add("Microsoft.Compute/snapshots/delete")
        $Role.Actions.Add("Microsoft.Compute/snapshots/beginGetAccess/action")
        $Role.Actions.Add("Microsoft.Compute/snapshots/endGetAccess/action")
        $Role.Actions.Add("Microsoft.Storage/storageAccounts/listkeys/action")
        $Role.Actions.Add("Microsoft.Storage/storageAccounts/read")
        $Role.Actions.Add("Microsoft.Resources/subscriptions/resourcegroups/read")
        $Role.Actions.Add("Microsoft.Authorization/roleDefinitions/read")
        $Role.Actions.Add("Microsoft.Authorization/roleAssignments/read")

        $SubId = $DeploymentTracker.SubscriptionId
        $Scope = "/subscriptions/$SubId"
        $Role.AssignableScopes.Clear()
        $Role.AssignableScopes.Add($Scope)

        Write-Info "Role $RoleName ($($Role.Description)) will be created under scope $($Role.AssignableScopes) and assigned to Connector with the following Actions: "
        Write-Text ($Role.Actions | Format-List | Out-String)

        $RoleDef = New-AzRoleDefinition -Role $Role
        Write-Success "Connector Role $RoleName successfully created!"
        Return $Scope

     } Catch {
        Write-Failure -Message "Could not create Connector role" -Exception $_.Exception
     }
}

Function Create-Connector-Storage-Role
{
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [PSObject]$DeploymentTracker
    )

    $RoleName = $DeploymentTracker.RoleSAName
    Write-Status "Creating Connector Storage Role $RoleName"

    Try {
        $ExistingRole = Get-AzRoleDefinition -Name $RoleName -ErrorAction Ignore
        if ($ExistingRole -ne $null) {
            Write-Success "Connector Storage Role $RoleName already exists. Skipping creation."
            Return $ExistingRole.AssignableScopes
        }

        $Role = Get-AzRoleDefinition "Virtual Machine Contributor"
        $Role.Id = $null
        $Role.Name = $RoleName
        $Role.Description = "Perform AWS Server Migration Service related connector tasks on behalf of the customer. "
        $Role.Description += "This role lets Connector read and write to blobs in its deployed storage account."
        $Role.Actions.Clear()
        $Role.Actions.Add("Microsoft.Storage/storageAccounts/listkeys/action")
        $Role.Actions.Add("Microsoft.Storage/storageAccounts/read")
        $Role.Actions.Add("Microsoft.Storage/storageAccounts/blobServices/read")
        $Role.Actions.Add("Microsoft.Storage/storageAccounts/blobServices/write")
        $Role.Actions.Add("Microsoft.Storage/storageAccounts/blobServices/containers/delete")
        $Role.Actions.Add("Microsoft.Storage/storageAccounts/blobServices/containers/read")
        $Role.Actions.Add("Microsoft.Storage/storageAccounts/blobServices/containers/write")

        $Role.DataActions.Clear()
        $Role.DataActions.Add("Microsoft.Storage/storageAccounts/blobServices/containers/blobs/read")
        $Role.DataActions.Add("Microsoft.Storage/storageAccounts/blobServices/containers/blobs/write")
        $Role.DataActions.Add("Microsoft.Storage/storageAccounts/blobServices/containers/blobs/delete")

        $SubId = $DeploymentTracker.SubscriptionId
        $SAName = $DeploymentTracker.StorageAccountName
        $RgName = $DeploymentTracker.StorageAccountResourceGroupName
        $Scope = "/subscriptions/$SubId/resourceGroups/$RgName/providers/Microsoft.Storage/storageAccounts/$SAName"
        $Role.AssignableScopes.Clear()
        $Role.AssignableScopes.Add($Scope)

        Write-Info "Role $RoleName ($($Role.Description)) will be created under scope $($Role.AssignableScopes) and assigned to Connector with the following Actions: "
        Write-Text ($Role.Actions | Format-List | Out-String)
        Write-Info "And the following DataActions:"
        Write-Text ($Role.DataActions | Format-List | Out-String)

        $RoleDef = New-AzRoleDefinition -Role $Role
        Write-Success "Connector Storage Role $RoleName successfully created!"
        Return $Scope
     } Catch {
        Write-Failure -Message "Could not create Connector Storage role" -Exception $_.Exception
     }
}

Function Get-Location-Based-Disk-Image-BlobUri
{
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [PSObject]$DeploymentTracker
    )

    $DISK_IMAGE_URI = "https://awssmsconnector.blob.core.windows.net/release/AWS-SMS-Connector-for-Azure.vhd"
    Return $DISK_IMAGE_URI
}

Function Get-Location-Based-Version-BlobUri
{
    Return "https://awssmsconnector.blob.core.windows.net/release/AWS-SMS-Connector-for-Azure.version"
}

Function Get-Location-Based-VM-Size
{
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [string]$Location
    )

    Return "Standard_F4s"
}

Function Get-Storage-Context
{
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [string]$SAName,

        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [string]$RgName
    )

    $Keys = (Get-AzStorageAccountKey -ResourceGroupName $RgName -Name $SAName) # | GetNonBlankValue -Field "StorageAccountKey"
    $SAKey = $Keys[0].Value
    $StorageContext = New-AzStorageContext -StorageAccountName $SAName -StorageAccountKey $SAKey

    Return $StorageContext
}


Function Derive-Resource-Name
{
  [CmdletBinding()]
  Param
  (
      [Parameter(Mandatory=$true)] [string]$ResourceType,
      [Parameter(Mandatory=$false)] [string]$Location,
      [Parameter(Mandatory=$false)] [string]$Index
  )

  if (($Location -ne $null) -And ($Location -ne "")) {
    $FormatString = "sms-connector-{0}{1}-{2}" -f "$ResourceType", "$Index", "$Location"
  } else {
    $FormatString = "sms-connector-{0}{1}" -f "$ResourceType", "$Index"
  }

  Return $FormatString
}

#################### DEPLOYMENT TRACKER FUNCTIONS ########################
Function Transition-To-Next-Deployment-Stage
{
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [PSObject]$DeploymentTracker
    )

    # Save Current Stage
    Save-DeploymentTracker $DeploymentTracker

    if ($DeploymentTracker.Stage -ne $Global:DEPLOYMENT_STAGE_COMPLETE) {
        Write-Status "Current Deployment Stage: $($DeploymentTracker.Stage). Transitioning to Next Stage"
    } else {
        Write-Status "Current Deployment Stage: $($DeploymentTracker.Stage)."
    }
    Try {
        # Transition to next stage
        $Stage = $DeploymentTracker.Stage
        Switch ($Stage) {
           $Global:DEPLOYMENT_STAGE_INITIALIZE
           {
                $DeploymentTracker.Stage = $Global:DEPLOYMENT_STAGE_RG_CREATE
                ResourceGroup-Create $DeploymentTracker
           }
           $Global:DEPLOYMENT_STAGE_RG_CREATE
           {
                $DeploymentTracker.Stage = $Global:DEPLOYMENT_STAGE_BLOB_COPY
                Blob-Copy $DeploymentTracker
           }
           $Global:DEPLOYMENT_STAGE_BLOB_COPY
           {
                $DeploymentTracker.Stage = $Global:DEPLOYMENT_STAGE_BLOB_VERIFY
                Blob-Verify $DeploymentTracker
           }
           $Global:DEPLOYMENT_STAGE_BLOB_VERIFY
           {
                $DeploymentTracker.Stage = $Global:DEPLOYMENT_STAGE_DISK_CREATE
                Disk-Create $DeploymentTracker
           }
           $Global:DEPLOYMENT_STAGE_DISK_CREATE
           {
                $DeploymentTracker.Stage = $Global:DEPLOYMENT_STAGE_VM_CREATE
                VirtualMachine-Create $DeploymentTracker
           }
           $Global:DEPLOYMENT_STAGE_VM_CREATE
           {
                $DeploymentTracker.Stage = $Global:DEPLOYMENT_STAGE_POST_CONFIG
                Post-Config $DeploymentTracker
           }
           $Global:DEPLOYMENT_STAGE_POST_CONFIG
           {
                $DeploymentTracker.Stage = $Global:DEPLOYMENT_STAGE_COMPLETE
                Complete-Deployment $DeploymentTracker
           }
           $Global:DEPLOYMENT_STAGE_COMPLETE
           {
                # The only way to come here is when deployment failed at complete stage and we resumed
                Complete-Deployment $DeploymentTracker
           }
           default
           {
                Write-Failure -Message "Invalid Deployment Stage!"
           }
        }
    } Catch {
        Write-Failure -Message "Error Transitioning to next deployment stage" -Exception $_.Exception
    }
}

Function Save-DeploymentTracker
{
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [PSObject]$DeploymentTracker
    )

    Write-Status "Saving Deployment Progress"
    Try {
        $StorageContext = Get-Storage-Context $DeploymentTracker.StorageAccountName $DeploymentTracker.StorageAccountResourceGroupName
        $ContainerName = Derive-Resource-Name "container" $DeploymentTracker.LocationName
        $ContainerObj = Get-AzStorageContainer -Name $ContainerName -Context $StorageContext -ErrorAction Ignore
        if ($ContainerObj -eq $null) {
            $ContainerObj = New-AzStorageContainer -Name $ContainerName -Context $StorageContext
        }
        $BlobName = $Global:DEPLOYMENT_BLOB_NAME
        $TrackerJson = $DeploymentTracker | ConvertTo-Json
        $TrackerJson | Out-File "$BlobName"
        $BlobObj = Set-AzStorageBlobContent -Container $ContainerName -Blob "$BlobName" -BlobType Block -Context $StorageContext -File "$BlobName" -Force
        Write-Success "Saved Deployment Progress"
        $RemoveLocalFile = Remove-Item -Path $BlobName -ErrorAction Ignore
    } Catch {
        Write-Warning -Message "Error saving deployment progress. This deployment cannot be resumed." -Exception $_.Exception
    }
}

Function Resume-Deployment
{
    param(
        [parameter(Mandatory=$true)][String] $StorageAccountName
    )


    Write-Status "Checking if this was a previous deployment"
    Try {
        $SAObj = (Get-AzStorageAccount | Where-Object {$_.StorageAccountName -eq "$StorageAccountName"}) | GetNonBlankValue -Field "StorageAccount"
        $LocationName = $SAObj.Location
        $StorageContext = Get-Storage-Context -SAName $StorageAccountName -RgName $SAObj.ResourceGroupName
        Write-Info "Storage Account $StorageAccountName is under Resource Group: $($SAObj.ResourceGroupName) and Location: $LocationName"

        $ContainerName = Derive-Resource-Name -ResourceType "container" -Location $LocationName
        $ContainerObj = Get-AzStorageContainer -Name $ContainerName -Context $StorageContext -ErrorAction Ignore
        if ($ContainerObj -eq $null) {
            Return $null
        }
        $BlobName = $Global:DEPLOYMENT_BLOB_NAME
        $BlobObj = Get-AzStorageBlobContent -Container $ContainerName -Blob $BlobName -Context $StorageContext -Force -ErrorAction Ignore
        if ($BlobObj -eq $null) {
            Return $null
        }
        $DeploymentTracker = Get-Content "$BlobName" | ConvertFrom-Json
        Write-Success "Found a previous deployment that can be resumed. Last Completed Deployment Stage: $($DeploymentTracker.Stage). Deployment details:"
        Write-Text ($DeploymentTracker | Format-List | Out-String)
        #$Conf = Read-Host "Would you like to resume this deployment (y/n)? "
        $Conf = "y"
        if ($Conf -eq "y") {
            $SubObj = Select-AzSubscription -SubscriptionId $DeploymentTracker.SubscriptionId
            Return $DeploymentTracker
        } else {
            Write-Warning "This will cleanup the existing deployment and start a new one."
            #$Conf = Read-Host "Do you want to proceed (y/n): "
            $Conf ="y"
            if ($Conf -ne "y") {
                Exit-Application
            }

            Delete-DeploymentTracker $DeploymentTracker
            Return $null
        }
    } Catch {
        Write-Warning -Message "Error retrieving previous deployment progress. Previous deployment cannot be resumed." -Exception $_.Exception
        Return $null
    }
}

Function Delete-DeploymentTracker
{
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [PSObject]$DeploymentTracker
    )

    Write-Status "Cleaning up Deployment Tracker"
    Try {
        $StorageContext = Get-Storage-Context -SAName $DeploymentTracker.StorageAccountName -RgName $DeploymentTracker.StorageAccountResourceGroupName
        $ContainerName = Derive-Resource-Name -ResourceType "container" -Location $DeploymentTracker.LocationName
        $ContainerObj = Get-AzStorageContainer -Name $ContainerName -Context $StorageContext
        $BlobName = $Global:DEPLOYMENT_BLOB_NAME
        $RemoveBlob = Remove-AzStorageBlob -Blob $BlobName -Container $ContainerName -Context $StorageContext -Force
        $RemoveLocalFile = Remove-Item -Path $BlobName -ErrorAction Ignore
        Write-Success "Completed Cleaning up Deployment Tracker"
    } Catch {
        Write-Warning -Message "Error cleaning up deployment tracker." -Exception $_.Exception
    }
}


#################### PS UTILITIES ########################
Function Setup
{
    # Check for Powershell version
    Verify-PowerShell-Version $POWERSHELL_MIN_VERSION
    # Check for any installation of Az
    Verify-Install-Az-Version $AZ_MODULE_MIN_VERSION
}

Function Verify-PowerShell-Version
{
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [System.Version]$minimumVersion
    )

    Write-Status "Verifying PowerShell version"
    [System.Version]$psVersion = "1.0"
    if ($PSVersionTable.PSVersion) {
        $psVersion = $PSVersionTable.PSVersion
    }
    $Message = "PowerShell version $minimumVersion or higher is required. Current PowerShell version is $psVersion."
    $Message += " Refer to https://docs.microsoft.com/en-us/powershell/scripting/install/installing-windows-powershell?view=powershell-6"
    If ($psVersion -lt $minimumVersion) {
        Write-Failure -Message $Message
    } else {
        Write-Success $Message
    }
}

Function Verify-Install-Az-Version
{
  [CmdletBinding()]
  Param
  (
      [Parameter(Mandatory=$true)]
      [ValidateNotNullOrEmpty()]
      [System.Version]$MinimumRequiredVersion
  )

  $RecommendedVersion = "2.0.0"
  $BreakingVersion = "3.0.0"

  Write-Status "Verifying Az Module version (Azure Powershell)"
  Try {
    Try {
      $Versions = Get-InstalledModule -Name Az -AllVersions
      Write-Success "Found an installed version of Az Module."
    } Catch {
      Write-Warning "Could not find any installed version of Az Module."
    #   $Conf = Read-Host "Would you like to install this Module (y/n)? "
    #   if ($Conf -ne 'y') {
    #     Exit-Application
    #   }

      Write-Status "Installing Az Module"
      Set-PSRepository -Name PSGallery -InstallationPolicy Trusted
      Install-Module -Name Az -Scope CurrentUser -AllowClobber -MaximumVersion -Force
      #$RecommendedVersion
    }

    Try {
      $CurVersion = Get-InstalledModule -Name Az -MinimumVersion $MinimumRequiredVersion
      Write-Success "Current Az Module version $($CurVersion.Version) satisfies the required minimum version $MinimumRequiredVersion."
      if ([System.Version] $CurVersion.Version -ge [System.Version] $BreakingVersion) {
        Write-Warning "The script works best with Az Module version $RecommendedVersion. Found $($CurVersion.Version) instead."
        Write-Warning "If you run into issues related to cmdlet failures, please re-install Az Module version $RecommendedVersion."
      }
    } Catch {
      Write-Warning "Could not find the minimum required installed version of Az Module - $minimumVersion"
      #$Conf = Read-Host "Would you like to update Az module to the latest version (y/n)? "
      $Conf = "y"
      if ($Conf -ne 'y') {
        Exit-Application
      } else {
        Write-Status "Updating Az Module"
        Update-Module -Name Az -MaximumVersion $RecommendedVersion
      }
    }

    Try {
      # Import Module for use
      Write-Status "Importing Az Module"
      Import-Module -Name Az
    } Catch {
      Write-Warning "Error while Importing Az Module" + $_.Exception + ", Retrying ..."
      Import-Module -Name Az
    }

    Write-Success "Successfully Imported required modules for setup to work"
  } Catch {
    Write-Failure "Error while installing/updating/importing Az Module for Powershell" $_.Exception
  }
}

Function GetNonBlankValue
{
  [CmdletBinding()]
  Param
  (
    [Parameter(Mandatory=$true, ValueFromPipeline=$true)]
    $Value1,

    [Parameter(Mandatory=$false)]
    $Value2,

    [Parameter(Mandatory=$true)]
    [string]$Field
  )

  if (($Value1 -ne $null) -And ($Value1 -ne "")) {
    Return $Value1
  }

  if (($Value2 -ne $null) -And ($Value2 -ne "")) {
    Return $Value2
  }

  Write-Failure "The provided $Field does not exist or is invalid!"
}

Function GetExceptionString
{
    [CmdletBinding()]
    Param(
      [Parameter(Mandatory=$false)]$Exception
    )

    if ($Exception -ne $null) {
      $LineNumber = $Exception.InvocationInfo.ScriptLineNumber
      if (($LineNumber -eq $null) -Or ($LineNumber -eq "")) {
        Try {
            $LineNumber = $Exception.InnerException.InvocationInfo.ScriptLineNumber
        } Catch {
        }
      }
      $Message = $Exception.Message
      $ExceptionStr = "Message: $Message (Line: $LineNumber)"
      $ExceptionStr += "`r`n"
      $ExceptionStr += "ExceptionDetails: " + $Exception.ToString()
      Return $ExceptionStr
    }

    Return ""
}

Function Write-Failure
{
    [CmdletBinding()]
    Param(
      [Parameter(Mandatory=$true)][string]$Message,
      [Parameter(Mandatory=$false)]$Exception
    )

    $Message = "[ERROR] $Message"
    Write-Host $Message -ForegroundColor White -BackgroundColor Red

    $ExceptionStr = GetExceptionString $Exception
    if ($ExceptionStr -ne "") {
      Write-Host $ExceptionStr -ForegroundColor Red
    }

    $TranscriptPath = Join-Path -Path (Get-Location).Path -ChildPath $Global:TRANSCRIPT_FILE_NAME
    $SupportMessage = "For support, please email 'aws-server-migration-azure-support@amazon.com'."
    $SupportMessage += " Attach the log file '$TranscriptPath' with the email."

    Write-Host $SupportMessage  -ForegroundColor White -BackgroundColor Red

    if ($ErrorActionPreference -eq "Stop") {
      Exit-Application
    }
}

Function Write-Warning
{
    [CmdletBinding()]
    Param(
      [Parameter(Mandatory=$true)][string]$Message,
      [Parameter(Mandatory=$false)]$Exception
    )

    $Message = "[Warning] $Message"
    Write-Host $Message -ForegroundColor Yellow -BackgroundColor Black

    $ExceptionStr = GetExceptionString $Exception
    if ($ExceptionStr -ne "") {
      Write-Host $ExceptionStr -ForegroundColor Yellow -BackgroundColor Black
    }
}

Function Write-Success
{
    [CmdletBinding()]
    Param([Parameter(Mandatory=$true)][string]$Message)

    $Message = "[Success] $Message"
    Write-Host $Message -ForegroundColor Green -BackgroundColor Black
}

Function Write-Status
{
  [CmdletBinding()]
  Param([Parameter(Mandatory=$true)][string]$Message)

  $Message = "[Working] $Message ..."
  Write-Host $Message -ForegroundColor White -BackgroundColor Black
}

Function Write-Info
{
  [CmdletBinding()]
  Param([Parameter(Mandatory=$true)][string]$Message)

  $Message = "[INFO] $Message"
  Write-Host $Message -ForegroundColor White -BackgroundColor Black
}

Function Write-Text
{
  [CmdletBinding()]
  Param([Parameter(Mandatory=$false, ValueFromPipeline=$true)][string]$Message)

  Write-Host $Message
}

Function Exit-Application
{
  [CmdletBinding()]
  Param([Parameter(Mandatory=$false)][int]$ExitCode)

  if ($ExitCode -gt 0) {
    Write-Error "Exiting with Error code: $ExitCode"
  }

  Stop-Transcript

  Exit
}

###################### MAIN DRIVER ###########################
Function Driver
{
    # Start Transcript
    Start-Transcript -Path $Global:TRANSCRIPT_FILE_NAME -IncludeInvocationHeader -Force -Append

    # Check Powershell and Az module versions
    Setup

    # Login to Azure account
    Login

    # See if this is a previous deployment
    $DeploymentTracker = Resume-Deployment $StorageAccountName

    if ($DeploymentTracker -ne $null) {
        Write-Status "Resuming Previous Deployment"
        Transition-To-Next-Deployment-Stage $DeploymentTracker
    } else {
        Write-Status "Starting new Deployment"
        Initialize
    }

    Exit-Application
}

Driver


# SIG # Begin signature block
# MIIjfAYJKoZIhvcNAQcCoIIjbTCCI2kCAQExDzANBglghkgBZQMEAgEFADB5Bgor
# BgEEAYI3AgEEoGswaTA0BgorBgEEAYI3AgEeMCYCAwEAAAQQH8w7YFlLCE63JNLG
# KX7zUQIBAAIBAAIBAAIBAAIBADAxMA0GCWCGSAFlAwQCAQUABCCP/5F7YegqW7DK
# z5Kh8RccH880kKd43510EUqCt2TNdKCCEdswggU7MIIDI6ADAgECAgphIE20AAAA
# AAAnMA0GCSqGSIb3DQEBBQUAMH8xCzAJBgNVBAYTAlVTMRMwEQYDVQQIEwpXYXNo
# aW5ndG9uMRAwDgYDVQQHEwdSZWRtb25kMR4wHAYDVQQKExVNaWNyb3NvZnQgQ29y
# cG9yYXRpb24xKTAnBgNVBAMTIE1pY3Jvc29mdCBDb2RlIFZlcmlmaWNhdGlvbiBS
# b290MB4XDTExMDQxNTE5NDUzM1oXDTIxMDQxNTE5NTUzM1owbDELMAkGA1UEBhMC
# VVMxFTATBgNVBAoTDERpZ2lDZXJ0IEluYzEZMBcGA1UECxMQd3d3LmRpZ2ljZXJ0
# LmNvbTErMCkGA1UEAxMiRGlnaUNlcnQgSGlnaCBBc3N1cmFuY2UgRVYgUm9vdCBD
# QTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMbM5XPm+9S75S0tMqbf
# 5YE/yc0lSbZxKsPVlDRnogocsF9ppkCxxLeyj9CYpKlBWTrT3JTWPNt0OKRKzE0l
# gvdKpVMSOO7zSW1xkX5jtqumX8OkhPhPYlG++MXs2ziS4wblCJEMxChBVfvLWokV
# fnHoNb9Ncgk9vjo4UFt3MRuNs8ckRZqnrG0AFFoEt7oT61EKmEFBIk5lYYeBQVCm
# eVyJ3hlKV9Uu5l0cUyx+mM0aBhakaHPQNAQTXKFx01p8VdteZOE3hzBWBOURtCmA
# EvF5OYiiAhF8J2a3iLd48soKqDirCmTCv2ZdlYTBoSUeh10aUAsgEsxBu24LUTi4
# S8sCAwEAAaOByzCByDARBgNVHSAECjAIMAYGBFUdIAAwCwYDVR0PBAQDAgGGMA8G
# A1UdEwEB/wQFMAMBAf8wHQYDVR0OBBYEFLE+w2kD+L9HAdSYJhoIAu9jZCvDMB8G
# A1UdIwQYMBaAFGL7CiFbf0NuEdoJVFBr9dKWcfGeMFUGA1UdHwROMEwwSqBIoEaG
# RGh0dHA6Ly9jcmwubWljcm9zb2Z0LmNvbS9wa2kvY3JsL3Byb2R1Y3RzL01pY3Jv
# c29mdENvZGVWZXJpZlJvb3QuY3JsMA0GCSqGSIb3DQEBBQUAA4ICAQAgjMFZ7W+c
# ay3BSj51HUVMQVAcvYDq2bCSiwYqEz9TFp5WOWqKY7Z4JHn1fbi5R6EKlsL2y72i
# Zp8G4azSeQkO/TzcrAIMcK8/G+x4ftTrSwVgJtlzYZEh7bBoY+CXEqtvoBLt2Z/S
# 2ic8s+RW+dHUgQ9xvUJ8ponczdW9laKr8ZMRfeisMSmoXWZwQZ38dcnVsxo5KtCF
# BVCLrJHKxJPLcaWdpJRvWAz6biDECDG1hZ1+gfnSPcpbGIVsCobsIgkbpXQ0T38o
# vJVKqx22mLBdCaR3dn7vp45dhPYYJMvRbabDoZzCEHWA/50y/ebPQzqC986P4XIq
# m2K3X+2VGjlcL5RtSLcBXzMvu9wtczSJBEIKHIt5+aP6F+/6oRoQ3+CywZXrXAwF
# lzs1PhiITdtsvySJjci92J97OTokoNXf0fNKGpf2pm96H7CQqbOsATmR02G3ZPE+
# VzgDr8560rWQ9a7cOZnVtjyX7abLFsd9aypMkJTmTFT9Hs0g7M5onIdY6WFgvusO
# ydUZfZ/peL0OrCF1B4+pbuCMaiprnOPnZby8LTxt3ATcZ0U2Mq8EgbyoAG5hTJXF
# XNSOjp8vwTJ0vb0RZQMHze+3XgJX2obUGig0r4hJss+l3YJWb2iqFOJZVP7/6u7v
# 6pJwImCB4yUjwJ/MD0myNapYwzrD2RaUEDCCBdgwggTAoAMCAQICEAFXOd/HbGJW
# 5/RjcoKs8WAwDQYJKoZIhvcNAQELBQAwbDELMAkGA1UEBhMCVVMxFTATBgNVBAoT
# DERpZ2lDZXJ0IEluYzEZMBcGA1UECxMQd3d3LmRpZ2ljZXJ0LmNvbTErMCkGA1UE
# AxMiRGlnaUNlcnQgRVYgQ29kZSBTaWduaW5nIENBIChTSEEyKTAeFw0xNzA2MDEw
# MDAwMDBaFw0yMDA2MDQxMjAwMDBaMIIBHTEdMBsGA1UEDwwUUHJpdmF0ZSBPcmdh
# bml6YXRpb24xEzARBgsrBgEEAYI3PAIBAxMCVVMxGTAXBgsrBgEEAYI3PAIBAhMI
# RGVsYXdhcmUxEDAOBgNVBAUTBzQxNTI5NTQxGDAWBgNVBAkTDzQxMCBUZXJyeSBB
# dmUgTjEOMAwGA1UEERMFOTgxMDkxCzAJBgNVBAYTAlVTMRMwEQYDVQQIEwpXYXNo
# aW5ndG9uMRAwDgYDVQQHEwdTZWF0dGxlMSIwIAYDVQQKExlBbWF6b24gV2ViIFNl
# cnZpY2VzLCBJbmMuMRQwEgYDVQQLEwtFQzIgV2luZG93czEiMCAGA1UEAxMZQW1h
# em9uIFdlYiBTZXJ2aWNlcywgSW5jLjCCASIwDQYJKoZIhvcNAQEBBQADggEPADCC
# AQoCggEBAMhxV81JHePkuhSpVQy3FLg0gmr+o4ZAwadPP3mea2ks4bye/jsbNTHM
# WSqcWYPHGNX0tSgx3nJmFVbJ8Qgy/tGxXRGExmon+u601/C3ev/iG2rEOqJQfA1Q
# q2qgc7DRDapLIoVtrAJRub8F/ev/JrkAJsTP9pvMKK+8my9ChG1+XJ8J6bAXZC4r
# xrDlz3wBv3GXT9kmB373roG/GN/ZVUpZ/mk65Ao2jqdxk5T2C2CTy5XyHnIi20Ww
# k/sqk3+GhdkLzwRlfOOLNK4uG613tMTMFjnot/2rSCJn5HyKJhB16eQoEanvm69S
# pslX6GOd2iGApFnwSdZAVW9WbatoBlMCAwEAAaOCAcEwggG9MB8GA1UdIwQYMBaA
# FI/ofvBtMmoABSPHcJdqOpD/a+rUMB0GA1UdDgQWBBSknbTZwaNKHtI1TDCznDpC
# YT0hhTAOBgNVHQ8BAf8EBAMCB4AwEwYDVR0lBAwwCgYIKwYBBQUHAwMwewYDVR0f
# BHQwcjA3oDWgM4YxaHR0cDovL2NybDMuZGlnaWNlcnQuY29tL0VWQ29kZVNpZ25p
# bmdTSEEyLWcxLmNybDA3oDWgM4YxaHR0cDovL2NybDQuZGlnaWNlcnQuY29tL0VW
# Q29kZVNpZ25pbmdTSEEyLWcxLmNybDBLBgNVHSAERDBCMDcGCWCGSAGG/WwDAjAq
# MCgGCCsGAQUFBwIBFhxodHRwczovL3d3dy5kaWdpY2VydC5jb20vQ1BTMAcGBWeB
# DAEDMH4GCCsGAQUFBwEBBHIwcDAkBggrBgEFBQcwAYYYaHR0cDovL29jc3AuZGln
# aWNlcnQuY29tMEgGCCsGAQUFBzAChjxodHRwOi8vY2FjZXJ0cy5kaWdpY2VydC5j
# b20vRGlnaUNlcnRFVkNvZGVTaWduaW5nQ0EtU0hBMi5jcnQwDAYDVR0TAQH/BAIw
# ADANBgkqhkiG9w0BAQsFAAOCAQEABOfgvE16qV5sLyP6B5sGLAZhhtzyYz7k1SmZ
# 7lJ0kdew5Cm2mX2OdkVugRUOW2fF8cQy15ssapfmTPvB6q3kelZq1yO8xNNtmuo4
# xNW+ys2BxKMn9VmhpaqYWy7i4e7AdcIfKoVQDkHFKomsyMstg2HlIT9nyNDYEBid
# mFqF62fPs53YignM2BGpIcz85exmtMqzG/6jyo+pQrCG77kcUdL59de+/mJn0w4t
# wpM9lh5yiNuB1kKT6oJi6HJOFJHcBR2VEAcecnIxyQ1SK0SJE1C3FdnxK7XIEXkE
# TEx1QQUAYIfJerfNSDmD7o67ndViYYwk1MgOHvuSnXRvy6wgHjCCBrwwggWkoAMC
# AQICEAPxtOFfOoLxFJZ4s9fYR1wwDQYJKoZIhvcNAQELBQAwbDELMAkGA1UEBhMC
# VVMxFTATBgNVBAoTDERpZ2lDZXJ0IEluYzEZMBcGA1UECxMQd3d3LmRpZ2ljZXJ0
# LmNvbTErMCkGA1UEAxMiRGlnaUNlcnQgSGlnaCBBc3N1cmFuY2UgRVYgUm9vdCBD
# QTAeFw0xMjA0MTgxMjAwMDBaFw0yNzA0MTgxMjAwMDBaMGwxCzAJBgNVBAYTAlVT
# MRUwEwYDVQQKEwxEaWdpQ2VydCBJbmMxGTAXBgNVBAsTEHd3dy5kaWdpY2VydC5j
# b20xKzApBgNVBAMTIkRpZ2lDZXJ0IEVWIENvZGUgU2lnbmluZyBDQSAoU0hBMikw
# ggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCnU/oPsrUT8WTPhID8roA1
# 0bbXx6MsrBosrPGErDo1EjqSkbpX5MTJ8y+oSDy31m7clyK6UXlhr0MvDbebtEkx
# rkRYPqShlqeHTyN+w2xlJJBVPqHKI3zFQunEemJFm33eY3TLnmMl+ISamq1FT659
# H8gTy3WbyeHhivgLDJj0yj7QRap6HqVYkzY0visuKzFYZrQyEJ+d8FKh7+g+03by
# QFrc+mo9G0utdrCMXO42uoPqMKhM3vELKlhBiK4AiasD0RaCICJ2615UOBJi4dJw
# JNvtH3DSZAmALeK2nc4f8rsh82zb2LMZe4pQn+/sNgpcmrdK0wigOXn93b89Ogkl
# AgMBAAGjggNYMIIDVDASBgNVHRMBAf8ECDAGAQH/AgEAMA4GA1UdDwEB/wQEAwIB
# hjATBgNVHSUEDDAKBggrBgEFBQcDAzB/BggrBgEFBQcBAQRzMHEwJAYIKwYBBQUH
# MAGGGGh0dHA6Ly9vY3NwLmRpZ2ljZXJ0LmNvbTBJBggrBgEFBQcwAoY9aHR0cDov
# L2NhY2VydHMuZGlnaWNlcnQuY29tL0RpZ2lDZXJ0SGlnaEFzc3VyYW5jZUVWUm9v
# dENBLmNydDCBjwYDVR0fBIGHMIGEMECgPqA8hjpodHRwOi8vY3JsMy5kaWdpY2Vy
# dC5jb20vRGlnaUNlcnRIaWdoQXNzdXJhbmNlRVZSb290Q0EuY3JsMECgPqA8hjpo
# dHRwOi8vY3JsNC5kaWdpY2VydC5jb20vRGlnaUNlcnRIaWdoQXNzdXJhbmNlRVZS
# b290Q0EuY3JsMIIBxAYDVR0gBIIBuzCCAbcwggGzBglghkgBhv1sAwIwggGkMDoG
# CCsGAQUFBwIBFi5odHRwOi8vd3d3LmRpZ2ljZXJ0LmNvbS9zc2wtY3BzLXJlcG9z
# aXRvcnkuaHRtMIIBZAYIKwYBBQUHAgIwggFWHoIBUgBBAG4AeQAgAHUAcwBlACAA
# bwBmACAAdABoAGkAcwAgAEMAZQByAHQAaQBmAGkAYwBhAHQAZQAgAGMAbwBuAHMA
# dABpAHQAdQB0AGUAcwAgAGEAYwBjAGUAcAB0AGEAbgBjAGUAIABvAGYAIAB0AGgA
# ZQAgAEQAaQBnAGkAQwBlAHIAdAAgAEMAUAAvAEMAUABTACAAYQBuAGQAIAB0AGgA
# ZQAgAFIAZQBsAHkAaQBuAGcAIABQAGEAcgB0AHkAIABBAGcAcgBlAGUAbQBlAG4A
# dAAgAHcAaABpAGMAaAAgAGwAaQBtAGkAdAAgAGwAaQBhAGIAaQBsAGkAdAB5ACAA
# YQBuAGQAIABhAHIAZQAgAGkAbgBjAG8AcgBwAG8AcgBhAHQAZQBkACAAaABlAHIA
# ZQBpAG4AIABiAHkAIAByAGUAZgBlAHIAZQBuAGMAZQAuMB0GA1UdDgQWBBSP6H7w
# bTJqAAUjx3CXajqQ/2vq1DAfBgNVHSMEGDAWgBSxPsNpA/i/RwHUmCYaCALvY2Qr
# wzANBgkqhkiG9w0BAQsFAAOCAQEAGTNKDIEzN9utNsnkyTq7tRsueqLi9ENCF56/
# TqFN4bHb6YHdnwHy5IjV6f4J/SHB7F2A0vDWwUPC/ncr2/nXkTPObNWyGTvmLtbJ
# k0+IQI7N4fV+8Q/GWVZy6OtqQb0c1UbVfEnKZjgVwb/gkXB3h9zJjTHJDCmiM+2N
# 4ofNiY0/G//V4BqXi3zabfuoxrI6Zmt7AbPN2KY07BIBq5VYpcRTV6hg5ucCEqC5
# I2SiTbt8gSVkIb7P7kIYQ5e7pTcGr03/JqVNYUvsRkG4Zc64eZ4IlguBjIo7j8eZ
# jKMqbphtXmHGlreKuWEtk7jrDgRD1/X+pvBi1JlqpcHB8GSUgDGCEPcwghDzAgEB
# MIGAMGwxCzAJBgNVBAYTAlVTMRUwEwYDVQQKEwxEaWdpQ2VydCBJbmMxGTAXBgNV
# BAsTEHd3dy5kaWdpY2VydC5jb20xKzApBgNVBAMTIkRpZ2lDZXJ0IEVWIENvZGUg
# U2lnbmluZyBDQSAoU0hBMikCEAFXOd/HbGJW5/RjcoKs8WAwDQYJYIZIAWUDBAIB
# BQCgfDAQBgorBgEEAYI3AgEMMQIwADAZBgkqhkiG9w0BCQMxDAYKKwYBBAGCNwIB
# BDAcBgorBgEEAYI3AgELMQ4wDAYKKwYBBAGCNwIBFTAvBgkqhkiG9w0BCQQxIgQg
# /ujXDMOXf5rus5Z5179Ff0DG2AbEh9lz+/vGl5DIcyowDQYJKoZIhvcNAQEBBQAE
# ggEARX41W1qkex7Lt0kzqhp2VBrcE/6UdACAbjq83lWDTrT+ToGF3JsNAvqyoz3i
# EG+KJq37N+ighBmlI+xR6S1UGJ3I8mOGLTt3iHNSlXoXw08wrDjekl8vKjUVQaqV
# Vp1hqJNMaEp8h2AU3t+yAUU0mVvMC/39CiwKfu09HY78wYmpdpHuWZ5OD34VD/FU
# JqrPgyfXPpbzZPsDyZA2KNod6m+N+EJuMckurttZ1TNR+8fmfclonmVm/6Xwya/7
# VUk2Pq0oHR+vwwFmVnktDaSuH0ZgRBBudMxNYWOqfZoo/MvkIPL6MV+BpxHnJZ8G
# IxAE3Fy50xYZFvXqm154GKum1aGCDskwgg7FBgorBgEEAYI3AwMBMYIOtTCCDrEG
# CSqGSIb3DQEHAqCCDqIwgg6eAgEDMQ8wDQYJYIZIAWUDBAIBBQAweAYLKoZIhvcN
# AQkQAQSgaQRnMGUCAQEGCWCGSAGG/WwHATAxMA0GCWCGSAFlAwQCAQUABCBwi+6M
# MUAOgSos6OUQ9KMk5VfQNsqk/Bza4UDNL4qQfwIRALLIQc6YUP2iXxZYgYGskYEY
# DzIwMTkxMTE4MjIyMTM2WqCCC7swggaCMIIFaqADAgECAhAEzT+FaK52xhuw/nFg
# zKdtMA0GCSqGSIb3DQEBCwUAMHIxCzAJBgNVBAYTAlVTMRUwEwYDVQQKEwxEaWdp
# Q2VydCBJbmMxGTAXBgNVBAsTEHd3dy5kaWdpY2VydC5jb20xMTAvBgNVBAMTKERp
# Z2lDZXJ0IFNIQTIgQXNzdXJlZCBJRCBUaW1lc3RhbXBpbmcgQ0EwHhcNMTkxMDAx
# MDAwMDAwWhcNMzAxMDE3MDAwMDAwWjBMMQswCQYDVQQGEwJVUzEXMBUGA1UEChMO
# RGlnaUNlcnQsIEluYy4xJDAiBgNVBAMTG1RJTUVTVEFNUC1TSEEyNTYtMjAxOS0x
# MC0xNTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAOlkNZz6qZhlZBvk
# F9y4KTbMZwlYhU0w4Mn/5Ts8EShQrwcx4l0JGML2iYxpCAQj4HctnRXluOihao7/
# 1K7Sehbv+EG1HTl1wc8vp6xFfpRtrAMBmTxiPn56/UWXMbT6t9lCPqdVm99aT1gC
# qDJpIhO+i4Itxpira5u0yfJlEQx0DbLwCJZ0xOiySKKhFKX4+uGJcEQ7je/7pPTD
# ub0ULOsMKCclgKsQSxYSYAtpIoxOzcbVsmVZIeB8LBKNcA6Pisrg09ezOXdQ0EIs
# LnrOnGd6OHdUQP9PlQQg1OvIzocUCP4dgN3Q5yt46r8fcMbuQhZTNkWbUxlJYp16
# ApuVFKMCAwEAAaOCAzgwggM0MA4GA1UdDwEB/wQEAwIHgDAMBgNVHRMBAf8EAjAA
# MBYGA1UdJQEB/wQMMAoGCCsGAQUFBwMIMIIBvwYDVR0gBIIBtjCCAbIwggGhBglg
# hkgBhv1sBwEwggGSMCgGCCsGAQUFBwIBFhxodHRwczovL3d3dy5kaWdpY2VydC5j
# b20vQ1BTMIIBZAYIKwYBBQUHAgIwggFWHoIBUgBBAG4AeQAgAHUAcwBlACAAbwBm
# ACAAdABoAGkAcwAgAEMAZQByAHQAaQBmAGkAYwBhAHQAZQAgAGMAbwBuAHMAdABp
# AHQAdQB0AGUAcwAgAGEAYwBjAGUAcAB0AGEAbgBjAGUAIABvAGYAIAB0AGgAZQAg
# AEQAaQBnAGkAQwBlAHIAdAAgAEMAUAAvAEMAUABTACAAYQBuAGQAIAB0AGgAZQAg
# AFIAZQBsAHkAaQBuAGcAIABQAGEAcgB0AHkAIABBAGcAcgBlAGUAbQBlAG4AdAAg
# AHcAaABpAGMAaAAgAGwAaQBtAGkAdAAgAGwAaQBhAGIAaQBsAGkAdAB5ACAAYQBu
# AGQAIABhAHIAZQAgAGkAbgBjAG8AcgBwAG8AcgBhAHQAZQBkACAAaABlAHIAZQBp
# AG4AIABiAHkAIAByAGUAZgBlAHIAZQBuAGMAZQAuMAsGCWCGSAGG/WwDFTAfBgNV
# HSMEGDAWgBT0tuEgHf4prtLkYaWyoiWyyBc1bjAdBgNVHQ4EFgQUVlMPwcYHp03X
# 2G5XcoBQTOTsnsEwcQYDVR0fBGowaDAyoDCgLoYsaHR0cDovL2NybDMuZGlnaWNl
# cnQuY29tL3NoYTItYXNzdXJlZC10cy5jcmwwMqAwoC6GLGh0dHA6Ly9jcmw0LmRp
# Z2ljZXJ0LmNvbS9zaGEyLWFzc3VyZWQtdHMuY3JsMIGFBggrBgEFBQcBAQR5MHcw
# JAYIKwYBBQUHMAGGGGh0dHA6Ly9vY3NwLmRpZ2ljZXJ0LmNvbTBPBggrBgEFBQcw
# AoZDaHR0cDovL2NhY2VydHMuZGlnaWNlcnQuY29tL0RpZ2lDZXJ0U0hBMkFzc3Vy
# ZWRJRFRpbWVzdGFtcGluZ0NBLmNydDANBgkqhkiG9w0BAQsFAAOCAQEALoOhRAVK
# BOO5MlL62YHwGrv4CY0juT3YkqHmRhxKL256PGNuNxejGr9YI7JDnJSDTjkJsCzo
# x+HizO3LeWvO3iMBR+2VVIHggHsSsa8Chqk6c2r++J/BjdEhjOQpgsOKC2AAAp0f
# R8SftApoU39aEKb4Iub4U5IxX9iCgy1tE0Kug8EQTqQk9Eec3g8icndcf0/pOZgr
# V5JE1+9uk9lDxwQzY1E3Vp5HBBHDo1hUIdjijlbXST9X/AqfI1579JSN3Z0au996
# KqbSRaZVDI/2TIryls+JRtwxspGQo18zMGBV9fxrMKyh7eRHTjOeZ2ootU3C7VuX
# gvjLqQhsUwm09zCCBTEwggQZoAMCAQICEAqhJdbWMht+QeQF2jaXwhUwDQYJKoZI
# hvcNAQELBQAwZTELMAkGA1UEBhMCVVMxFTATBgNVBAoTDERpZ2lDZXJ0IEluYzEZ
# MBcGA1UECxMQd3d3LmRpZ2ljZXJ0LmNvbTEkMCIGA1UEAxMbRGlnaUNlcnQgQXNz
# dXJlZCBJRCBSb290IENBMB4XDTE2MDEwNzEyMDAwMFoXDTMxMDEwNzEyMDAwMFow
# cjELMAkGA1UEBhMCVVMxFTATBgNVBAoTDERpZ2lDZXJ0IEluYzEZMBcGA1UECxMQ
# d3d3LmRpZ2ljZXJ0LmNvbTExMC8GA1UEAxMoRGlnaUNlcnQgU0hBMiBBc3N1cmVk
# IElEIFRpbWVzdGFtcGluZyBDQTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoC
# ggEBAL3QMu5LzY9/3am6gpnFOVQoV7YjSsQOB0UzURB90Pl9TWh+57ag9I2ziOSX
# v2MhkJi/E7xX08PhfgjWahQAOPcuHjvuzKb2Mln+X2U/4Jvr40ZHBhpVfgsnfsCi
# 9aDg3iI/Dv9+lfvzo7oiPhisEeTwmQNtO4V8CdPuXciaC1TjqAlxa+DPIhAPdc9x
# ck4Krd9AOly3UeGheRTGTSQjMF287DxgaqwvB8z98OpH2YhQXv1mblZhJymJhFHm
# gudGUP2UKiyn5HU+upgPhH+fMRTWrdXyZMt7HgXQhBlyF/EXBu89zdZN7wZC/aJT
# Kk+FHcQdPK/P2qwQ9d2srOlW/5MCAwEAAaOCAc4wggHKMB0GA1UdDgQWBBT0tuEg
# Hf4prtLkYaWyoiWyyBc1bjAfBgNVHSMEGDAWgBRF66Kv9JLLgjEtUYunpyGd823I
# DzASBgNVHRMBAf8ECDAGAQH/AgEAMA4GA1UdDwEB/wQEAwIBhjATBgNVHSUEDDAK
# BggrBgEFBQcDCDB5BggrBgEFBQcBAQRtMGswJAYIKwYBBQUHMAGGGGh0dHA6Ly9v
# Y3NwLmRpZ2ljZXJ0LmNvbTBDBggrBgEFBQcwAoY3aHR0cDovL2NhY2VydHMuZGln
# aWNlcnQuY29tL0RpZ2lDZXJ0QXNzdXJlZElEUm9vdENBLmNydDCBgQYDVR0fBHow
# eDA6oDigNoY0aHR0cDovL2NybDQuZGlnaWNlcnQuY29tL0RpZ2lDZXJ0QXNzdXJl
# ZElEUm9vdENBLmNybDA6oDigNoY0aHR0cDovL2NybDMuZGlnaWNlcnQuY29tL0Rp
# Z2lDZXJ0QXNzdXJlZElEUm9vdENBLmNybDBQBgNVHSAESTBHMDgGCmCGSAGG/WwA
# AgQwKjAoBggrBgEFBQcCARYcaHR0cHM6Ly93d3cuZGlnaWNlcnQuY29tL0NQUzAL
# BglghkgBhv1sBwEwDQYJKoZIhvcNAQELBQADggEBAHGVEulRh1Zpze/d2nyqY3qz
# eM8GN0CE70uEv8rPAwL9xafDDiBCLK938ysfDCFaKrcFNB1qrpn4J6JmvwmqYN92
# pDqTD/iy0dh8GWLoXoIlHsS6HHssIeLWWywUNUMEaLLbdQLgcseY1jxk5R9IEBhf
# iThhTWJGJIdjjJFSLK8pieV4H9YLFKWA1xJHcLN11ZOFk362kmf7U2GJqPVrlsD0
# WGkNfMgBsbkodbeZY4UijGHKeZR+WfyMD+NvtQEmtmyl7odRIeRYYJu6DC0rbaLE
# frvEJStHAgh8Sa4TtuF8QkIoxhhWz0E0tmZdtnR79VYzIi8iNrJLokqV2PWmjlIx
# ggJNMIICSQIBATCBhjByMQswCQYDVQQGEwJVUzEVMBMGA1UEChMMRGlnaUNlcnQg
# SW5jMRkwFwYDVQQLExB3d3cuZGlnaWNlcnQuY29tMTEwLwYDVQQDEyhEaWdpQ2Vy
# dCBTSEEyIEFzc3VyZWQgSUQgVGltZXN0YW1waW5nIENBAhAEzT+FaK52xhuw/nFg
# zKdtMA0GCWCGSAFlAwQCAQUAoIGYMBoGCSqGSIb3DQEJAzENBgsqhkiG9w0BCRAB
# BDAcBgkqhkiG9w0BCQUxDxcNMTkxMTE4MjIyMTM2WjArBgsqhkiG9w0BCRACDDEc
# MBowGDAWBBQDJb1QXtqWMC3CL0+gHkwovig0xTAvBgkqhkiG9w0BCQQxIgQgVtkj
# 6LsVJR2ap4faxwKq3lj6eb5hcK61rY0fSX/WGW4wDQYJKoZIhvcNAQEBBQAEggEA
# 1/8K4KI6ScO0PvzJQqj27DxWHf9+/6fl34OoyNWRkZOJCw+QZHDq0BxkV/M07nrT
# s1PcTRwUeQLg76S244XLPRyIBg+38ZCBD1nrcU8HsRVtw2WaoAViEeri5RQtC7V+
# bjlh8G9o6fXJhY7HBOQduoUc6TQkyN7d5SKMoS3gUEqo+REnRV5286YFDvbWNY3v
# Wiqo5+zc/zWIbVJvR65fN1h4MrjDh92Od6R6Vy5OX8piY4FVPWC1JoCCPOkV8r0I
# ufdw1i3zOwK+dz1Tm/li4GzO5yr3XCaZHVTph1jtXK+Z8ff4GLFAkn1POR6sXhkj
# OYwh8d9ipl+CbSLrzR5zcA==
# SIG # End signature block
