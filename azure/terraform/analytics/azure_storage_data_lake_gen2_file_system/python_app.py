import pandas as pd
import sys

def read_blob_to_dataframe(blob_url):
    try:
        df = pd.read_json(blob_url)
        print("DataFrame loaded successfully!")
        print(df.head())  # Display the first few rows of the DataFrame
    except Exception as e:
        print(f"An error occurred: {e}")

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python script.py <blob_url>")
        sys.exit(1)

    blob_url = sys.argv[1]
    read_blob_to_dataframe(blob_url)
