locals {
  container_names = ["bob", "kevin", "stewart"]
}

# To create an resource group
resource "azurerm_resource_group" "example" {
  name     = "${var.prefix}-${var.project}-${var.env}-rg"
  location = var.az_location
}

# To create a storage account
resource "azurerm_storage_account" "example" {
  name                          = "${var.prefix}${var.project}${var.env}storacc"
  resource_group_name           = azurerm_resource_group.example.name
  location                      = var.az_location
  account_tier                  = "Standard"
  account_replication_type      = "LRS"
  account_kind                  = "StorageV2"
  is_hns_enabled                = "true"
  public_network_access_enabled = true
}

# To create a data lake gen2 file system
resource "azurerm_storage_data_lake_gen2_filesystem" "example" {
  name               = "${var.prefix}${var.project}${var.env}sadlgen2fs"
  storage_account_id = azurerm_storage_account.example.id

  properties = {
    hello = "aGVsbG8="
  }
}

# To create a container in the storage account
resource "azurerm_storage_container" "example" {
  for_each              = toset(local.container_names)
  name                  = "${var.prefix}-${var.project}-${var.env}-${each.value}"
  storage_account_name  = azurerm_storage_account.example.name
  container_access_type = "private"
}

# To create a blob in the container (Upload a file)
resource "azurerm_storage_blob" "example1" {
  for_each = toset(local.container_names)

  name                   = var.blob_name_1
  storage_account_name   = azurerm_storage_account.example.name
  storage_container_name = azurerm_storage_container.example[each.key].name
  type                   = "Block"
  source                 = var.blob_name_1_path
}

# To create a blob in the container (Upload a file)
resource "azurerm_storage_blob" "example2" {
  for_each = toset(local.container_names)

  name                   = var.blob_name_2
  storage_account_name   = azurerm_storage_account.example.name
  storage_container_name = azurerm_storage_container.example[each.key].name
  type                   = "Block"
  source                 = var.blob_name_2_path
}

# To create a blob in the container (Upload a file)
resource "azurerm_storage_blob" "example3" {
  for_each = toset(local.container_names)

  name                   = var.blob_name_3
  storage_account_name   = azurerm_storage_account.example.name
  storage_container_name = azurerm_storage_container.example[each.key].name
  type                   = "Block"
  source                 = var.blob_name_3_path
}