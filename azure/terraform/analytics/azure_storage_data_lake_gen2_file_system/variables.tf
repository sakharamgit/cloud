##################################################################################
# VARIABLES
##################################################################################


variable "azure_key_path" {}

variable "prefix" {
  type    = string
  default = "ss"
}

variable "project" {
  type    = string
  default = "sakharam_xor"
}

variable "env" {
  type    = string
  default = "dev"
}

variable "az_location" {
  type    = string
  default = "Central India"
}

variable "az_stor_acc_name" {
  type    = string
  default = "storage_account"
}

variable "az_stg_con_name" {
  type    = string
  default = "az_container"
}

resource "random_string" "random" {
  length  = 5
  upper   = false
  lower   = true
  numeric = true
  special = false
}

variable "blob_name_1" {
  type    = string
  default = "blob1"
}

variable "blob_name_1_path" {
  type = string
}

variable "blob_name_2" {
  type    = string
  default = "blob1"
}

variable "blob_name_2_path" {
  type = string
}

variable "blob_name_3" {
  type    = string
  default = "blob1"
}

variable "blob_name_3_path" {
  type = string
}

