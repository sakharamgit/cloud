##################################################################################
# VARIABLES
##################################################################################

# Azure Variables
variable "private_key_path" {
  type = string
}
variable "sample_txt_file_path" {
  type = string
}

variable "az_location" {
  type    = string
  default = "East US"
}

variable "az_prefix" {
  type    = string
  default = "ss"
}

variable "az_rg_name" {
  type    = string
  default = "sakharam_xor"
}

variable "az_environment" {
  type    = string
  default = "dev"
}

variable "az_vm_name" {
  type    = string
  default = "virtualmachine"
}

variable "public_key_path" {
  type = string
}
