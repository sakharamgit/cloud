output "storage_account" {
    value = {
        name     = module.azurerm_storage_account.name
        location = module.azurerm_storage_account.location
    } 
}