
locals {
  config = jsondecode(file("terraform.tfvars.json"))
}

module "resource_group" {

  for_each = local.config.resource_groups

  source = "../../../../../modules/terraform/azure/general/resource_group"

  resource_group_name = each.value.resource_group_name
  location            = each.value.location
  tags = merge(
    lookup(each.value, "tags", {}),
    local.config.common_tags
  )
}

module "storage_account" {

  for_each = local.config.storage_accounts

  source = "../../../../../modules/terraform/azure/storage/storage_account"

  storage_account_name            = each.value.storage_account_name
  resource_group_name             = each.value.resource_group_name
  location                        = each.value.location
  account_tier                    = each.value.account_tier
  account_replication_type        = each.value.account_replication_type
  delete_retention_policy_in_days = each.value.delete_retention_policy_in_days
  identity_type                   = each.value.identity_type
  tags = merge(
    lookup(each.value, "tags", {}),
    local.config.common_tags
  )
  depends_on = [ module.resource_group ]
}

# module "storage_account" {
#   source  = "SakharamS/storage-account/azure"
#   version = "~> 1.0"

#   resource_group_name      = local.config.resource_group_name
#   location                 = local.config.location
#   storage_account_name     = local.config.storage_account_name
#   account_tier             = local.config.account_tier
#   account_replication_type = local.config.account_replication_type
#   delete_retention_policy_in_days = local.config.delete_retention_policy_in_days
#   tags                     = local.config.tags
# }