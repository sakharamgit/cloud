#############################
# Variables
#############################
$subscriptionid = "71e10ac9-a8a2-48aa-8db6-496b91fa7449"
#$subscriptionid = "53df3d12-f68b-4aa6-aed7-cef4b7e7ac08"

$azStorageAccountsAll = @()
$resourcegroupname = "Default-Storage-EastUS"
$storageaccountname = "vpc3uploadedfiles"
$containername = "thumbnails-test"

#############################
# Script
#############################
# To set the current subscription 
$currsubscriptionid = (Get-AzContext).Subscription.id
if($currsubscriptionid -ne $subscriptionid)
{
    Clear-AzContext -Scope CurrentUser -Force
    Connect-AzAccount 
    Set-AzContext -Subscription $subscriptionid
    $currsubscriptionid = (Get-AzContext).Subscription.Name
    Write-Host "Subscription set to $currsubscriptionid ..."
}
else
{
    Write-Host "Subscription is already set to $currsubscriptionid ..."
}

# To get the list of Storage Accounts
If($storageaccountname -ne $null)
{
    $azStorageAccounts = Get-AzStorageAccount -StorageAccountName $storageaccountname -ResourceGroupName $resourcegroupname
}
else {
    $azStorageAccounts = Get-AzStorageAccount    
}


# To create an object with the Storage Account Details
foreach($azStorageAccount in $azStorageAccounts)
{
    # To get the account key
    $azStorageAccountKey = (Get-AzStorageAccountKey -ResourceGroupName $azStorageAccount.ResourceGroupName -Name $azStorageAccount.StorageAccountName | Select -First 1).value
    
    # Custom Object for Azure Storage Accounts
    $azStorageAccProperties = [ordered] @{
        accountRg        = $azStorageAccount.ResourceGroupName
        accountName      = $azStorageAccount.StorageAccountName
        accountKey       = $azStorageAccountKey
    }
    
    $azStorageAccounts = New-Object -Type PSObject -Property $azStorageAccProperties

    $azStorageAccountsAll += $azStorageAccounts
}

# $azStorageAccountsAll | Format-Table 

foreach($azStorageAcc in $azStorageAccountsAll)
{
    $storageAcc = Get-AzStorageAccount -ResourceGroupName $azStorageAcc.accountRg -Name $azStorageAcc.accountName          
    $ctx = $storageAcc.Context    
    If($containername -ne $null)
    {
        $containers = $containername
    }
    else {
        $containers = (Get-AzStorageContainer  -Context $ctx).Name    
    }

    $accountName = "vpc3uploadedfilestest"
    $rcloneRemoteCmd = "rclone config create $($azStorageAcc.accountName) azureblob account $($azStorageAcc.accountName) key $($azStorageAcc.accountKey)"
    
    $existingRemotes = rclone listremotes
    If($existingRemotes -contains "$($azStorageAcc.accountName):")
    {
        Write-Host "The remote $azStorageAcc.accountName already exists." -Foreground Yellow
    }
    else 
    {
        Write-Host "$rcloneRemotecmd"
        Invoke-Expression $rcloneRemotecmd
    }

    # Add rclone remote for AWS based on each container in Azure Storage

    foreach($container in $containers)
    {
        Write-Host "Start Time: (Get-Date)"
        $rcloneCopyCmd = "rclone copy $($azStorageAcc.accountName):$($container) awsremote:$accountName/$($container) --log-level ERROR --progress"
        Invoke-Expression $rcloneCopyCmd
        Write-Host "End Time: (Get-Date)"
    }
}


# rclone config create vpc3uploadedfiles azureblob account vpc3uploadedfiles key ht+BPWgs//BpJGkffwij0kqF36bh0nC3I4ZckUwMF/Xo29EePdaWN+TBV1CkgJQE+wGBoViNdMnUt3AaAfYekA==
# rclone config create awsremote s3 provider AWS region ap-southeast-1 location_constraint ap-southeast-1 env_auth false access_key_id AKIAXG7JMBB2T57EQLJA secret_access_key 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
# rclone copy azremote:ss-blobtos3-container1 awsremote:ss-blobtos3-dev-s3-bucket --log-level ERROR --progress --dry-run
# Start Time: 7.20 PM IST