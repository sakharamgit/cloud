[CmdletBinding()]
Param(
    [Parameter(Mandatory=$false)]
    $destroy
    )

$ErrorActionPreference = "Stop"

#############################################################################
# Script to clone a virtual machine 
#############################################################################

# Variables
$subscriptionid        = $env:ARM_SUBSCRIPTION_ID
$existingRgName        = "ss-winvm-dev-rg"
$existingVmName        = "ss-winvm-dev-vm"
$existingVnetName      = "ss-winvm-dev-vnet"
$prefix                = "ss"
$project               = "clonevm"
$env                   = "dev"
#$vnetCidrRange         = ""
#$subnetCidrRange       = ""
$storageType           = 'Standard_LRS'
$azure_region          = 'East US'
$scriptLogName         = "clonescriptoutput.log"
$ostype                = "Windows"
$clonedVMSize          = 'Standard_D4s_v3'

#Script
$currentDirectory = (Get-Location).Path
$scriptLogPath = $currentDirectory + "\" + "$scriptLogName"
"Starting script exection" | Out-File -FilePath $scriptLogPath -Force

# Cloned Virtual Machine variables
$pipName        = "$prefix-$project-$env-pip"
$nsgName        = "$prefix-$project-$env-nsg"
$nicName        = "$prefix-$project-$env-nic"
$clonedVMname   = "$prefix-$project-$env-vm"
$storageAccName = "bootstoracc"

# Destroy
If($destroy -eq 'True')
{
    "Destroying the resource group $newRgName and resources within it" | Out-File -FilePath clonescriptoutput.log -Append 
    If(Get-AzResourceGroup -Name $newRgName -Location $azure_region -ErrorAction SilentlyContinue)
    {
        try {
            Remove-AzResourceGroup -Name $newRgName -Force -Verbose
        }
        catch {
            "Error while removing the resource group $newRgName / resources within the group. Error: $_" | Out-File -FilePath clonescriptoutput.log -Append 
        }
    }
    else {
        "The resource group $newRgName does not exist." | Out-File -FilePath clonescriptoutput.log -Append
    }

    # Removing the snapshots
    $vmSnapshotOSDiskName      = "$prefix-$project-$env-$($existingVM.Name)_os_snapshot_$(Get-Date -Format ddMMyy)"
    $existingVM = Get-AzVM -ResourceGroupName $existingRgName -Name $existingVmName
    try {
        Remove-AzSnapshot -ResourceGroupName $existingRgName -SnapshotName $vmSnapshotOSDiskName -Force | Out-Null
    }
    catch {
        "Error while removing the snapshot $vmSnapshotOSDiskName from $existingRgName. Error: $_" | Out-File -FilePath clonescriptoutput.log -Append 
    }

    $existingVMDataDisks         = ($existingVM.StorageProfile.DataDisks).Name
    foreach($disk in $existingVMDataDisks)
    {
        $vmSnapshotDataDiskName  = "$prefix-$project-$env-$disk-_disk_snapshot_$(Get-Date -Format ddMMyy)"
        try {
            Remove-AzSnapshot -ResourceGroupName $existingRgName -SnapshotName $vmSnapshotDataDiskName -Force | Out-Null
        }
        catch {
            "Error while removing the snapshot $vmSnapshotDataDiskName from $existingRgName. Error: $_" | Out-File -FilePath clonescriptoutput.log -Append 
        }
    }

    Exit
}

# To set the current subscription 
$currsubscriptionid = (Get-AzContext).Subscription.id
If($currsubscriptionid -ne $subscriptionid)
{
    Clear-AzContext -Scope CurrentUser -Force
    Connect-AzAccount 
    Set-AzContext -Subscription $subscriptionid | Out-Null
    $currsubscriptionid = (Get-AzContext).Subscription.Name
    "Subscription set to $currsubscriptionid ..." | Out-File -FilePath $scriptLogPath -Append
}
else
{
    "Subscription is already set to $currsubscriptionid ..." | Out-File -FilePath clonescriptoutput.log -Append
}

# To create a snapshot of OS & Data disk from the existing virtual machine
try {
    $existingVM = Get-AzVM -ResourceGroupName $existingRgName -Name $existingVmName
    "Existing Virtual Machine: $($existingVM.Name)" | Out-File -FilePath clonescriptoutput.log -Append  
}
catch {
    "Error while retrieving the existing virtual machine Error: $_" | Out-File -FilePath clonescriptoutput.log -Append 
}

#OS Disk Snapshot
$vmSnapshotOSDiskName      = "$prefix-$project-$env-$($existingVM.Name)_os_snapshot_$(Get-Date -Format ddMMyy)"
"Starting Virtual Machine $($existingVM.Name) OS Disk Snapshot" | Out-File -FilePath clonescriptoutput.log -Append 
If(Get-AzSnapshot -SnapshotName $vmSnapshotOSDiskName) {
    "OS Disk Snapshot $vmSnapshotOSDiskName for the existing virtual machine is already created." | Out-File -FilePath clonescriptoutput.log -Append 
}
else {
    $OSDiskSnapshotConfig      = New-AzSnapshotConfig -SourceUri $existingVM.StorageProfile.OsDisk.ManagedDisk.id -CreateOption Copy -Location $azure_region -OsType $ostype

    # OS Disk Snapshot
    try {
        New-AzSnapshot -ResourceGroupName $existingRgName -SnapshotName $vmSnapshotOSDiskName -Snapshot $OSDiskSnapshotConfig | Out-Null
        "OS Disk Snaphshot for existing virtual machine $($existingVM.Name) completed successfully: $vmSnapshotOSDiskName " | Out-File -FilePath clonescriptoutput.log -Append 
    } catch {
        "Creating snapshot of the OS Disk for existing virtual machine failed. Error: $_" | Out-File -FilePath clonescriptoutput.log -Append 
    }

    "Completed Virtual Machine $($existingVM.Name) OS Disk Snapshot" | Out-File -FilePath clonescriptoutput.log -Append
}

# Data Disk Snapshots 
"Starting Virtual Machine $($existingVM.Name) Data Disks Snapshot" | Out-File -FilePath clonescriptoutput.log -Append 
$existingVMDataDisks         = ($existingVM.StorageProfile.DataDisks).Name
foreach($disk in $existingVMDataDisks)
{
    $vmSnapshotDataDiskName  = "$prefix-$project-$env-$disk-_disk_snapshot_$(Get-Date -Format ddMMyy)"
    "Starting Data Disk Snapshot for $disk" | Out-File -FilePath clonescriptoutput.log -Append 
    If(Get-AzSnapshot -SnapshotName $vmSnapshotDataDiskName) {
        "Data disk snapshot $vmSnapshotDataDiskName for the existing virtual machine is already created." | Out-File -FilePath clonescriptoutput.log -Append 
    }
    else {
        $dataDisk = Get-AzDisk -ResourceGroupName $existingVM.ResourceGroupName -DiskName $disk
        $DataDiskSnapshotConfig = New-AzSnapshotConfig -SourceUri $dataDisk.Id -CreateOption Copy -Location $azure_region
        try {
            New-AzSnapshot -ResourceGroupName $existingRgName -SnapshotName $vmSnapshotDataDiskName -Snapshot $DataDiskSnapshotConfig | Out-Null
            "Data Disk Snaphshot for disk $disk of existing virtual machine completed successfully: $vmSnapshotDataDiskName " | Out-File -FilePath clonescriptoutput.log -Append 
        }
        catch {
            "Creating snapshot of the data disk for existing virtual machine failed. Error: $_" | Out-File -FilePath clonescriptoutput.log -Append
        }     
        "Completed Virtual Machine $($existingVM.Name) Data Disk Snapshot" | Out-File -FilePath clonescriptoutput.log -Append
    }
}

######################################## 
# To create a cloned Virtual Machine
######################################## 

# To create a resource group
$newRgName = "$prefix-$project-$env-rg"
If(Get-AzResourceGroup -Name $newRgName -Location $azure_region -ErrorAction SilentlyContinue)
{
    "Resource group $newRgName is already present in the Azure subscripion" | Out-File -FilePath clonescriptoutput.log -Append
}
else {   
    try{
        New-AzResourceGroup -Name $newRgName -Location $azure_region | Out-Null
        "New resource group $newRgName created successfully" | Out-File -FilePath clonescriptoutput.log -Append      
    }
    catch{
        "Creating new resource group $newRgName failed. Error: $_" | Out-File -FilePath clonescriptoutput.log -Append    
    }
}

# Retrieve the values of snapshot for the OS Snapshot and create a managed disk
$newVMOSDiskName       = "$prefix-$project-$env-cloned_vm_os_disk_$(Get-Date -Format ddMMyy)"
try {
    $exitingOSSnapshotName = Get-AzSnapshot -ResourceGroupName $existingRgName -SnapshotName $vmSnapshotOSDiskName     
}
catch {
    "Retrieving the snapshot $vmSnapshotOSDiskName failed. Error: $_" | Out-File -FilePath clonescriptoutput.log -Append  
}
try {
    $osDiskConfig          = New-AzDiskConfig -AccountType $storageType -Location $azure_region -CreateOption Copy -SourceResourceId $exitingOSSnapshotName.Id
    $osDisk                = New-AzDisk -Disk $osDiskConfig -ResourceGroupName $existingRgName -DiskName $newVMOSDiskName 
    "Managed disk $newVMOSDiskName for the cloned VM created successfully" | Out-File -FilePath clonescriptoutput.log -Append 
}
catch {
    "Creating Managed disk $newVMOSDiskName for the cloned VM created failed. Error: $_" | Out-File -FilePath clonescriptoutput.log -Append 
}

# Create a public IP and assign static IP address
If(Get-AzPublicIpAddress -Name $pipName -ResourceGroupName $newRgName)
{
    "The public ip resource $pipName is already present." | Out-File -FilePath clonescriptoutput.log -Append 
}
else {
    try {
        $pip = New-AzPublicIpAddress -Name $pipName -ResourceGroupName $newRgName -Location $azure_region -AllocationMethod Static    
    }
    catch {
        "Creating a public ip address resource failed. Error: $_" | Out-File -FilePath clonescriptoutput.log -Append 
    }
}

# To create a new storage account
If(Get-AzStorageAccount -Name $storageAccName -ResourceGroupName $newRgName -ErrorAction SilentlyContinue)
{
    "The storage account $storageAccName is already present." | Out-File -FilePath clonescriptoutput.log -Append
}
else {
    try {
        New-AzStorageAccount -ResourceGroupName $newRgName -Name $storageAccName -Location $azure_region -SkuName Standard_RAGRS -Kind StorageV2 | Out-Null
    }
    catch {
        "Creating the storage account $storageAccName failed. Error: $_" | Out-File -FilePath clonescriptoutput.log -Append 
    }    
}



# Create an inbound network security group rule for port 3389
$nsgRuleRDP = New-AzNetworkSecurityRuleConfig -Name AllowRDPIn  -Protocol Tcp -Direction Inbound -Priority 1000 -SourceAddressPrefix * -SourcePortRange * -DestinationAddressPrefix * -DestinationPortRange 3389 -Access Allow
If(Get-AzNetworkSecurityGroup -Name $nsgName)
{
    "The network security group $nsgName is already present" | Out-File -FilePath clonescriptoutput.log -Append
}
else {
    try {
        $nsg  = New-AzNetworkSecurityGroup -ResourceGroupName $newRgName -Location $azure_region -Name $nsgName -SecurityRules $nsgRuleRDP    
    }
    catch {
        "Creating the network security group $nsg failed. Error: $_" | Out-File -FilePath clonescriptoutput.log -Append 
    }
}


# Retrieve existing NIC and create a new NIC for cloned VM
try {
    $vnet = Get-AzVirtualNetwork -Name $existingVnetName -ResourceGroupName $existingRgName
}
catch {
    "Retrieving the existing vnet $existingVnetName failed. Error: $_" | Out-File -FilePath clonescriptoutput.log -Append 
}
If(Get-AzNetworkInterface -Name $nicName -ResourceGroupName $newRgName -ErrorAction SilentlyContinue)
{
    "The NIC $nicName is already created." | Out-File -FilePath clonescriptoutput.log -Append 
}
else {
    try {
        $nic = New-AzNetworkInterface -Name $nicName -ResourceGroupName $newRgName -Location $azure_region -SubnetId $vnet.Subnets[0].Id -PublicIpAddressId $pip.Id -NetworkSecurityGroupId $nsg.Id
    }
    catch {
        "Creating a new NIC for the cloned VM failed. Error: $_" | Out-File -FilePath clonescriptoutput.log -Append 
    }
}



# To assign a VM identity and VM size
$vmObject = New-AzVMConfig -VMName $clonedVMname -VMSize $clonedVMSize
$vmObject = Set-AzVMOSDisk -VM $vmObject -ManagedDiskId $osdisk.Id -CreateOption Attach -Windows
$vmObject = Set-AzVMBootDiagnostic -VM $vmObject -Enable -StorageAccountName $storageAccName -ResourceGroupName $newRgName
#$vmObject = Add-AzVMDataDisk -VM $vmObject -Name $dataDiskName -ManagedDiskId $datadisk.id -Lun "0" -CreateOption "Attach"
$vmObject = Add-AzVMNetworkInterface -VM $vmObject -Id $nic.Id
If(Get-AzVM -Name $clonedVMname -ErrorAction SilentlyContinue)
{
    "The cloned virtual machine $clonedVMname is already present." | Out-File -FilePath clonescriptoutput.log -Append 
}
else {
    try {
        New-AzVM -VM $vmObject -ResourceGroupName $newRgName -Location $azure_region | Out-Null
    }
    catch {
        "Creating cloned virtual machine $clonedVMname failed. Error: $_" | Out-File -FilePath clonescriptoutput.log -Append 
    }
}

<#
# Retrieve the values of snapshot for the Data Snapshot
$DataSnapshot = Get-AzSnapshot -ResourceGroupName $RGName -SnapshotName $DatasnapshotName 

# Create a configurable data disk object from the details of storage type Geo Location and snapshot ID
$DatadiskConfig = New-AzDiskConfig -AccountType $StorageType -Location $geolocation -CreateOption Copy -SourceResourceId $DataSnapshot.Id

# Create a Managed Data Disk from the data disk Configuration
$Datadisk = New-AzDisk -Disk $DatadiskConfig -ResourceGroupName $RGName -DiskName $DataDiskName

#>
