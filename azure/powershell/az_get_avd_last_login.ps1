#Variables
#-------------------------

$resourcegroupname = "avd-demo"


# Script
#-----------------------------------

$current_az_context = (az account show --output json | ConvertFrom-Json).name

If($current_az_context)
{
    Write-Host "Current Subscription is : $current_az_context" -ForegroundColor Yellow
}
else 
{
    az login
}


$applicationGroups = (Get-AzWvdApplicationGroup).Name
$hostPools = (Get-AzWvdHostPool).Name

foreach($hostpool in $hostPools)
{
    Write-Host "Processing Host Pool : $hostpool" -ForegroundColor Yellow
    $sessionHostlists = (Get-AzWvdSessionHost -HostPoolName $hostpool -ResourceGroupName $resourcegroupname).name
    foreach($sessionHostlist in $sessionHostlists)
    {
        $sessionHost = ($sessionHostlist -split "/")[1]
        Write-Host "Processing Host: $sessionHost" -ForegroundColor Yellow
        #(az vm run-command invoke  --command-id RunPowerShellScript --name $sessionHost -g $resourcegroupname  --scripts 'Get-LocalUser | Select Name, Lastlogon' --output json | ConvertFrom-Json).value.message
        #(az vm run-command invoke  --command-id RunPowerShellScript --name $sessionHost -g $resourcegroupname  --scripts '(Get-EventLog -InstanceId 4624 -LogName Security -Newest 1).TimeGenerated' --output json | ConvertFrom-Json).value.message
        #(az vm run-command invoke  --command-id RunPowerShellScript --name $sessionHost -g $resourcegroupname  --scripts 'Get-WinEvent -logname "Microsoft-Windows-TerminalServices-LocalSessionManager/Operational"  | Sort-Object TimeCreated -Descending | Select-Object -First 1' --output json | ConvertFrom-Json).value.message
        Invoke-AzVmRunCommand -ResourceGroupName $resourcegroupname -VMName $sessionHost -CommandId "RunPowerShellScript" -ScriptPath "Get-WinEvent -logname 'Microsoft-Windows-TerminalServices-LocalSessionManager/Operational'  | Sort-Object TimeCreated -Descending | Select-Object -First 1"
    }
}