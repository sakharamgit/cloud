################################################
# Name: Get the list of AKS subnets with CIDR
################################################

#############################
# Variables
#############################
$subscriptionid = "2b13aa22-d8da-4137-8bea-7bc68bf2ca61"


######################
# Script
######################
# To set the current subscription 
$currsubscriptionid = (Get-AzContext).Subscription.id
if($currsubscriptionid -ne $subscriptionid)
{
    Clear-AzContext -Scope CurrentUser -Force
    Connect-AzAccount 
    Set-AzContext -Subscription $subscriptionid
    $currsubscriptionid = (Get-AzContext).Subscription.Name
    Write-Host "Subscription set to $currsubscriptionid ..."
}
else
{
    Write-Host "Subscription is already set to $currsubscriptionid ..."
}

$aks = Get-AzAksCluster
$aks