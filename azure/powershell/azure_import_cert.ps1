################################################
# Name: Get Expiring Certs from Azure Key Vault
################################################

#############################
# Variables
#############################
# Staging
$subscriptionid = "e521ad64-ab0c-4ec7-a2d8-2c6d4a1fd00e"

# Lab
#$subscriptionid = "115f738e-a8ff-4ed7-9188-8a36d4bb3e7d"
$newCertName = "auto-wildcard"
$expCertName = "auto2-wildcard"
$keyVault = "aeu2-00e-s-ws-kv04"

######################
# Script
######################
# To set the current subscription 
$currsubscriptionid = (Get-AzContext).Subscription.id
if($currsubscriptionid -ne $subscriptionid)
{
    Clear-AzContext -Scope CurrentUser -Force
    Connect-AzAccount 
    Set-AzContext -Subscription $subscriptionid
    $currsubscriptionid = (Get-AzContext).Subscription.Name
    Write-Host "Subscription set to $currsubscriptionid ..."
}
else
{
    Write-Host "Subscription is already set to $currsubscriptionid ..."
}

Write-Host "Expiring Cert: $expCertName" -ForegroundColor Yellow
$expCert = Get-AzKeyVaultCertificate -VaultName $keyVault -Name $expCertName
$expCertOldVersion = $expCert.Version
$expCertDomain = $expCert.Certificate.Subject.Replace("CN=", "")

Write-Host "New Cert: $newCertName" -ForegroundColor Yellow
$newCert = Get-AzKeyVaultCertificate -VaultName $keyVault -Name $newCertName

#Export Azure Key Vault certificate to .cer file
$secret = Get-AzKeyVaultSecret -VaultName $keyVault -Name $expCert.Name
$secretValueText = '';
$ssPtr = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($secret.SecretValue)
try {
    $secretValueText = [System.Runtime.InteropServices.Marshal]::PtrToStringBSTR($ssPtr)
} finally {
    [System.Runtime.InteropServices.Marshal]::ZeroFreeBSTR($ssPtr)
}
$secretByte = [Convert]::FromBase64String($secretValueText)
$x509Cert = New-Object System.Security.Cryptography.X509Certificates.X509Certificate2
$x509Cert.Import($secretByte, "", "Exportable,PersistKeySet")
$type = [System.Security.Cryptography.X509Certificates.X509ContentType]::Pfx
$password = "secret"
$secure = ConvertTo-SecureString -String $password -AsPlainText -Force
$pfxFileByte = $x509Cert.Export($type, $password)

# Write to a file
[System.IO.File]::WriteAllBytes(".\$expCertName.pfx", $pfxFileByte)
Write-Host "Exported certificate to file - $.\$expCertName.pfx"

Write-Host "Import Cert" -ForegroundColor Yellow
Import-AzKeyVaultCertificate -VaultName $keyVault -Name "$expCertName" -FilePath ".\$expCertName.pfx" -Password $secure

# Disable older certificate version
Update-AzKeyVaultCertificate -VaultName $KeyVault -Name $expCertName -Version $expCertOldVersion -Enable $false
