################################################
# Name: Get Expiring Certs from Azure Key Vault
################################################

#############################
# Variables
#############################
$subscriptionid = "e521ad64-ab0c-4ec7-a2d8-2c6d4a1fd00e"
$expiringCertsList = @()
$listofCerts = @()
$daystoexpire = 90

######################
# Script
######################
# To set the current subscription 
$currsubscriptionid = (Get-AzContext).Subscription.id
if($currsubscriptionid -ne $subscriptionid)
{
    Clear-AzContext -Scope CurrentUser -Force
    Connect-AzAccount 
    Set-AzContext -Subscription $subscriptionid
    $currsubscriptionid = (Get-AzContext).Subscription.Name
    Write-Host "Subscription set to $currsubscriptionid ..."
}
else
{
    Write-Host "Subscription is already set to $currsubscriptionid ..."
}


$KeyVaults = (Get-AzKeyVault).VaultName

# To get the Azure key vaults
foreach($keyVault in $KeyVaults)
{
    Write-Host "Processing Key Vault: $keyVault" -ForegroundColor Yellow
    $listofCerts += Get-AzKeyVaultCertificate -VaultName $KeyVault | Where-Object {$_.Name -eq "le-wildcard"}
}

# To add the expiring certs to the variable 
foreach($cert in $listofCerts)
{
    $expiringCert = $null

    $KeyVaultCertExpiryDate = $cert.expires
    
    if($KeyVaultCertExpiryDate -lt ((Get-Date).AddDays($daystoexpire)))
    {
        $certProperties = [ordered] @{
            KeyVaultName             = $cert.VaultName
            KeyVaultCertName         = $cert.Name
            KeyVaultCertEnabled      = $cert.Enabled
            KeyVaultCertExpiryDate   = $cert.expires
        }
    
        $expiringCert = New-Object -Type PSObject -Property $certProperties
    
        $expiringCertsList += $expiringCert
    }
}

$expiringCertsList | Format-Table -AutoSize
