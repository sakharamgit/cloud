
// IO object is global available in the browser due to socket.io client library
// Create a socket connect to the local http server using websocket protocol
const socket = io('ws://localhost:8080');

// To listen to the socket event emitted by the browser
socket.on('message', text => {
    
    // Using Imperative browser api to append the message to the unordered list 
    const el = document.createElement('li');
    el.innerHTML = text;
    document.querySelector('ul').appendChild(el)

});

// Function to send message
function sendMessage() {
    const text = document.querySelector('input').value;
    socket.emit('message', text);
    document.querySelector('input').value = ''; // Clear the input after sending
}

// To provide ability to the user to send message  using onclick event
document.querySelector('button').onclick = sendMessage;

// Capture Enter key in the input field
document.querySelector('input').addEventListener('keydown', (event) => {
    if (event.key === 'Enter') {
        sendMessage();
    }
});

// Regular Websockets

// const socket = new WebSocket('ws://localhost:8080');

// // Listen for messages
// socket.onmessage = ({ data }) => {
//     console.log('Message from server ', data);
// };

// document.querySelector('button').onclick = () => {
//     socket.send('hello');
// }