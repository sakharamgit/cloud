
// To create an http server
const http = require('http').createServer();

// Import socket.io and pass the http server object
const io = require('socket.io')(http, {
    // Set CORS origin to anything to disable CORS
    cors: { origin: "*" }
});
// To log all the messages to a file
const fs = require('fs'); 


// Function to append logs to a file
function logToFile(message) {
    fs.appendFile('server.log', message + '\n', (err) => {
        if (err) throw err;
    });
}

// To listen for a connection from client to the server
io.on('connection', (socket) => {
    // console.log('a user connected'); // Log messages to the console
    logToFile('a user connected'); // Log messages to a file
    // To listen for a custom event from client - Here it 'message'
    socket.on('message', (message) =>     {
        //console.log(message); // Log messages to the console
        logToFile(message); // Log messages to a file
        // To broadcast the event to all the clients listening to the server
        io.emit('message', `${socket.id.substr(0,2)} said ${message}` );   
    });
});

// Instruct the server to listen on port 8080
http.listen(8080, () => console.log('listening on http://localhost:8080') );


// Regular Websockets

// const WebSocket = require('ws')
// const server = new WebSocket.Server({ port: '8080' })

// server.on('connection', socket => { 

//   socket.on('message', message => {

//     socket.send(`Roger that! ${message}`);

//   });

// });


 
