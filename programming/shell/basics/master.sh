
#################################################################################
# Variable Declaration
#################################################################################

# Shell variables don't require explicit type declaration.

# String
name="John Doe"

# Integer
count=10

# Arrays
fruits=("apple" "banana" "cherry")


#################################################################################
# Function Definition
#################################################################################

# Without Parameters:
Copy code
greet() {
    echo "Hello, World!"
}
greet

# With Parameters:

Copy code
greet() {
    echo "Hello, $1!"
}

greet "Alice"

# With Return Value:

Copy code
add() {
    local sum=$(( $1 + $2 ))
    echo $sum
}

result=$(add 5 3)
echo $result  # Outputs: 8

#################################################################################
# Built-in Commands
#################################################################################

# String Concatenation:

a="Hello"
b="World"
c="$a $b"
echo $c  # Outputs: "Hello World"

# String Splitting:

text="Hello World"
IFS=' ' read -r -a words <<< "$text"
echo "${words[0]}"  # Outputs: "Hello"
echo "${words[1]}"  # Outputs: "World"

# Length of a String:

length=${#text}
echo $length  # Outputs: 11


#################################################################################
# Loops
#################################################################################

# For Loop:

for i in {1..5}
do
    echo $i
done

# For Each Loop (using an array):

fruits=("apple" "banana" "cherry")
for fruit in "${fruits[@]}"
do
    echo $fruit
done

# While Loop:

count=0
while [ $count -lt 5 ]
do
    echo $count
    count=$((count + 1))
done

# Do-While Loop:

count=0
while :
do
    echo $count
    count=$((count + 1))
    [ $count -ge 5 ] && break
done

#################################################################################
# Shell Script File Structure
#################################################################################

# A basic shell script project might look like this:

project/
│
├── main.sh         # Main script file
├── functions.sh    # Additional functions
└── scripts/
    └── helper.sh   # Helper script