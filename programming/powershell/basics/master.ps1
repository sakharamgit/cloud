#################################################################################
# Variable Declaration
#################################################################################

# PowerShell variables are declared using the $ symbol, and types are inferred automatically.

# String
$name = "John Doe"

# Integer
$count = 10

# Array
$fruits = @("apple", "banana", "cherry")

# Hash Table (Dictionary)
$person = @{ "Name" = "John"; "Age" = 30 }


#################################################################################
# Function Definition
#################################################################################

# Without Parameters:
function Greet {
    Write-Output "Hello, World!"
}

Greet

# With Parameters:
function Greet($name) {
    Write-Output "Hello, $name!"
}

Greet "Alice"

# With Return Value:
function Add($a, $b) {
    return $a + $b
}

$result = Add 5 3
Write-Output $result  # Outputs: 8

#################################################################################
# Built-in Commands
#################################################################################

# String Concatenation:

$a = "Hello"
$b = "World"
$c = "$a $b"
Write-Output $c  # Outputs: "Hello World"

# String Splitting:

$text = "Hello World"
$words = $text -split " "
$words[0]  # Outputs: "Hello"
$words[1]  # Outputs: "World"

# Length of a String:

$length = $text.Length
Write-Output $length  # Outputs: 11

#################################################################################
# Loops
#################################################################################

# For Loop:

for ($i = 0; $i -lt 5; $i++) {
    Write-Output $i
}

# ForEach Loop (using an array):

$fruits = @("apple", "banana", "cherry")
foreach ($fruit in $fruits) {
    Write-Output $fruit
}

# While Loop:

$count = 0
while ($count -lt 5) {
    Write-Output $count
    $count++
}

# Do-While Loop:

$count = 0
do {
    Write-Output $count
    $count++
} while ($count -lt 5)


#################################################################################
# PowerShell Script File Structure
#################################################################################

# A basic PowerShell project might look like this:

project/
│
├── main.ps1         # Main script file
├── functions.ps1    # Additional functions
└── scripts/
    └── helper.ps1   # Helper script