import requests

# API endpoint for JSONPlaceholder
api_url = "https://jsonplaceholder.typicode.com/posts"

# Make a request to the API
response = requests.get(api_url)
user_id = 1

# Check if the request was successful
if response.status_code == 200:
    data = response.json()
    
    # Filter: Extract posts by userId 
    user_posts = [post for post in data if post["userId"] == user_id]
    
    # Print the filtered posts
    print(f"Posts by User {user_id}:")
    for post in user_posts:
        print(f"Post ID: {post['id']}, Title: {post['title']}")
        print(f"Body: {post['body']}\n")

else:
    print("Failed to retrieve data from JSONPlaceholder API")
