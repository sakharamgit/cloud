import requests
import os

# API endpoint for OpenWeatherMap (replace 'YOUR_API_KEY' with your actual API key)
api_url = "http://api.openweathermap.org/data/2.5/weather"
city = "Mumbai"
api_key = os.getenv("OPENWEATHERAPI")  # Sign up at OpenWeatherMap to get an API key

# Check if the API key is set
if not api_key:
    print("API key not found. Please set the API_KEY environment variable.")
    exit()

# Make a request to the API
response = requests.get(api_url, params={"q": city, "appid": api_key, "units": "metric"})

# Check if the request was successful
if response.status_code == 200:
    data = response.json()
    
    # Extract specific information
    city_name = data["name"]
    temperature = data["main"]["temp"]
    weather_description = data["weather"][0]["description"]
    
    # Print the extracted information
    print(f"City: {city_name}")
    print(f"Temperature: {temperature}°C")
    print(f"Weather: {weather_description}")
    
    # Filtering: Only print if temperature is above 20°C
    if temperature > 20:
        print("It's warm in", city_name)
    else:
        print("It's cool in", city_name)

else:
    print(f"Failed to retrieve data from OpenWeatherMap API. Status Code: {response.status_code}")
    print(f"Error Message: {response.text}")
