#################################################################################
# Variable Declaration
#################################################################################

# Integer
x = 10

# Float
y = 3.14

# String
name = "John Doe"

# Boolean
is_active = True

# List
fruits = ["apple", "banana", "cherry"]

# Tuple (immutable list)
coordinates = (10.0, 20.0)

# Dictionary (key-value pairs)
person = {"name": "John", "age": 30}

# Set (unique elements)
unique_numbers = {1, 2, 3, 4}

#################################################################################
# Function Declaration
#################################################################################

# Without Parameters
def greet():
    print("Hello, World!")

greet()

# With Parameters
def greet(name):
    print(f"Hello, {name}!")

greet("Alice")

# With Return Value

def add(a, b):
    return a + b

result = add(5, 3)
print(result)  # Outputs: 8


#################################################################################
# Built-in Functions
#################################################################################

# String Concatenation:
a = "Hello"
b = "World"
c = a + " " + b  # Outputs: "Hello World"

# String Splitting
text = "Hello World"
words = text.split()  # Outputs: ['Hello', 'World']

# Length of a String, List, etc.:
length = len("Hello")  # Outputs: 5

# Type Casting
num_str = "123"
num = int(num_str)  # Converts string to integer


#################################################################################
# Loops
#################################################################################

# For Loop 
for i in range(5):
    print(i)

# For Loop using a list
fruits = ["apple", "banana", "cherry"]
for fruit in fruits:
    print(fruit)

# While Loop
count = 0
while count < 5:
    print(count)
    count += 1

# Do While Loop
count = 0
while True:
    print(count)
    count += 1
    if count >= 5:
        break


#################################################################################
# Python File Structure
#################################################################################

# project/
# │
# ├── main.py         # Entry point of your application
# ├── module.py       # Additional module
# └── utils/
#     ├── __init__.py # Marks this directory as a package
#     └── helper.py   # Utility functions
