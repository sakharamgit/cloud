# Project Title

This project uses a `Taskfile.yml` to manage tasks related to deploying and testing a hello-world Kubernetes application along with nginx ingress controller.

## Prerequisites

- [Task](https://taskfile.dev/#/installation)
- [Minikube](https://minikube.sigs.k8s.io/docs/start/)
- [kubectl](https://kubernetes.io/docs/tasks/tools/)
- [Git](https://git-scm.com/)

## Getting Started:

1. Navigate to the directory and run the below command:

```
task one-click-deploy
```

- The one-click-deploy task in the Taskfile.yml orchestrates the process of starting a Minikube cluster, enabling certain Minikube addons, deploying two "Hello World" applications, exposing these applications, deploying an Ingress controller, and testing the applications and Ingress controller, all in a single command.

2. Use the different tasks availble in the taskfile.yaml to test the application. For example:

```
task browse-app-ingress
````

- This task can be used to browse the app on the defined URL to confirm if nginx controller is working as expected.