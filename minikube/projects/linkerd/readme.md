# Prerequisities
1. Install Linkerd CLI 
    ```
    curl --proto '=https' --tlsv1.2 -sSfL https://run.linkerd.io/install-edge | sh
    export PATH=$HOME/.linkerd2/bin:$PATH
    linkerd version
    ```
2. Linkerd proxy-init container needs to run as the root user when your Kubernetes nodes are using the Docker container runtime.

